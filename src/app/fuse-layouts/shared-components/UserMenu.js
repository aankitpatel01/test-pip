import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';
import * as authActions from 'app/auth/store/actions';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

function UserMenu(props) {
	const dispatch = useDispatch();
	const user = useSelector(({ auth }) => auth.user);

	const [userMenu, setUserMenu] = useState(null);

	const userMenuClick = event => {
		setUserMenu(event.currentTarget);
	};

	const userMenuClose = () => {
		setUserMenu(null);
	};

	return (
		<>
			<Button className="h-64" onClick={userMenuClick}>
				{user.data.photoURL ? (
					<Avatar className="" alt="user photo" src={user.data.photoURL} />
				) : (
					<Avatar className="">{user.data.displayName[0]}</Avatar>
				)}

				<div className="hidden md:flex flex-col mx-12 items-start">
					<Typography component="span" className="normal-case font-600 flex">
						{user.data.displayName}
					</Typography>
					<Typography className="text-11 capitalize text-black" color="textSecondary">
						{user.role}
					</Typography>
				</div>

				<Icon className="text-16 hidden sm:flex" variant="action">
					keyboard_arrow_down
				</Icon>
			</Button>

			<Popover
				open={Boolean(userMenu)}
				anchorEl={userMenu}
				onClose={userMenuClose}
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'center'
				}}
				transformOrigin={{
					vertical: 'top',
					horizontal: 'center'
				}}
				classes={{
					paper: 'py-8'
				}}
			>
				{user.role === 'Interpreter' && (
					<>
						<MenuItem component={Link} to="/dashboard" onClick={userMenuClose} role="button">
							<ListItemIcon className="min-w-40">
								<Icon>account_circle</Icon>
							</ListItemIcon>
							<ListItemText primary="Dashboard" />
						</MenuItem>
						<MenuItem component={Link} to="/interpreter-settings" onClick={userMenuClose} role="button">
							<ListItemIcon className="min-w-40">
								<Icon>settings</Icon>
							</ListItemIcon>
							<ListItemText primary="settings" />
						</MenuItem>
						<MenuItem
							onClick={() => {
								dispatch(authActions.logoutUser());
								userMenuClose();
							}}
						>
							<ListItemIcon className="min-w-40">
								<Icon>exit_to_app</Icon>
							</ListItemIcon>
							<ListItemText primary="Logout" />
						</MenuItem>
					</>
				)}

				{user.role === 'Admin' && (
					<>
						<MenuItem component={Link} to="/dashboard" onClick={userMenuClose} role="button">
							<ListItemIcon className="min-w-40">
								<Icon>account_circle</Icon>
							</ListItemIcon>
							<ListItemText primary="Dashboard" />
						</MenuItem>
						<MenuItem component={Link} to="/company-list" onClick={userMenuClose} role="button">
							<ListItemIcon className="min-w-40">
								<Icon>mail</Icon>
							</ListItemIcon>
							<ListItemText primary="Company List" />
						</MenuItem>
						<MenuItem component={Link} to="/users-manager" onClick={userMenuClose} role="button">
							<ListItemIcon className="min-w-40">
								<Icon>people_outline</Icon>
							</ListItemIcon>
							<ListItemText primary="Users" />
						</MenuItem>
						<MenuItem component={Link} to="/settings" onClick={userMenuClose} role="button">
							<ListItemIcon className="min-w-40">
								<Icon>settings</Icon>
							</ListItemIcon>
							<ListItemText primary="settings" />
						</MenuItem>
						<MenuItem
							onClick={() => {
								dispatch(authActions.logoutUser());
								userMenuClose();
							}}
						>
							<ListItemIcon className="min-w-40">
								<Icon>exit_to_app</Icon>
							</ListItemIcon>
							<ListItemText primary="Logout" />
						</MenuItem>
					</>
				)}

				{(user.role === 'Company' || user.role === 'Organizer') && (
					<>
						<MenuItem component={Link} to="/dashboard" onClick={userMenuClose} role="button">
							<ListItemIcon className="min-w-40">
								<Icon>account_circle</Icon>
							</ListItemIcon>
							<ListItemText primary="Dashboard" />
						</MenuItem>
						<MenuItem component={Link} to="/users-manager" onClick={userMenuClose} role="button">
							<ListItemIcon className="min-w-40">
								<Icon>people_outline</Icon>
							</ListItemIcon>
							<ListItemText primary="Users" />
						</MenuItem>
						<MenuItem component={Link} to="/settings" onClick={userMenuClose} role="button">
							<ListItemIcon className="min-w-40">
								<Icon>settings</Icon>
							</ListItemIcon>
							<ListItemText primary="settings" />
						</MenuItem>
						<MenuItem
							onClick={() => {
								dispatch(authActions.logoutUser());
								userMenuClose();
							}}
						>
							<ListItemIcon className="min-w-40">
								<Icon>exit_to_app</Icon>
							</ListItemIcon>
							<ListItemText primary="Logout" />
						</MenuItem>
					</>
				)}
			</Popover>
		</>
	);
}

export default UserMenu;
