import jwtService from 'app/services/jwtService';
import * as Actions from 'app/store/actions';

export const RESET_ERROR = 'RESET PASSWORD CHANGE ERROR';
export const RESET_SUCCESS = 'RESET PASSWORD CHANGE SUCCESS';
export const RESET_PASSWORD_LINK = '[USER] RESET PASSWORD LINK SEND';
export const RESET_PASSWORD_ERROR = '[USER] RESET PASSWORD LINK ERROR';

/*
 * Reset Password link
 */

export function resetPasswordUser(email) {
	console.log('resetPasswordUser', email);
	return dispatch =>
		jwtService
			.resetPasswordUser(email)
			.then(data => {
				console.log(data);
				dispatch(Actions.showMessage({ message: data.message }));
				return dispatch({ type: RESET_PASSWORD_LINK, payload: data.message });
			})
			.catch(err => {
				console.log(err);
				dispatch(Actions.showMessage({ message: err.message }));
				return dispatch({ type: RESET_PASSWORD_ERROR, payload: err });
			});
}

/*
 * Reset Password changed
 */

export function resetPasswordUpdate(data) {
	console.log('resetPasswordUser', data);
	return dispatch =>
		jwtService
			.resetPasswordUpdate(data)
			.then(res => {
				console.log(res);
				dispatch(Actions.showMessage({ message: res.message }));
				return dispatch({ type: RESET_SUCCESS, payload: res.message });
			})
			.catch(err => {
				console.log(err);
				dispatch(Actions.showMessage({ message: err.message }));
				return dispatch({ type: RESET_ERROR, payload: err.message });
			});
}
