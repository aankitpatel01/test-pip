import * as Actions from '../actions';

const initialState = {
	linksuccess: false,
	success: false,
	error: {
		username: null,
		password: null
	},
	message: null
};

const reset = (state = initialState, action) => {
	switch (action.type) {
		case Actions.RESET_PASSWORD_LINK: {
			return {
				...initialState,
				linksuccess: true
			};
		}
		case Actions.RESET_PASSWORD_ERROR: {
			return {
				linksuccess: false,
				error: action.payload
			};
		}
		case Actions.RESET_SUCCESS: {
			return {
				...initialState,
				success: true,
				message: action.payload
			};
		}
		case Actions.RESET_ERROR: {
			return {
				...initialState,
				success: false,
				message: action.payload
			};
		}
		default: {
			return state;
		}
	}
};

export default reset;
