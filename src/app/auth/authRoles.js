/**
 * Authorization Roles
 */
const authRoles = {
	admin: ['Admin'],
	staff: ['Admin', 'Organizer', 'Company'],
	interpreter: ['Interpreter'],
	onlyGuest: []
};

export default authRoles;
