import FuseSplashScreen from '@fuse/core/FuseSplashScreen';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router';

const Callback = props => {
	const loginValid = useSelector(({ auth }) => auth.user.role);

	useEffect(() => {
		console.log(loginValid);
	}, [loginValid]);

	if (loginValid === 'Admin' || loginValid === 'Company' || loginValid === 'Organizer') {
		return <Redirect to="/dashboard" />;
	}
	if (loginValid === 'Interpreter') {
		return <Redirect to="/interpreter" />;
	}

	return <FuseSplashScreen />;
};

export default Callback;
