import { TextFieldFormsy } from '@fuse/core/formsy';
import FuseAnimate from '@fuse/core/FuseAnimate';
import { useForm } from '@fuse/hooks';
import clsx from 'clsx';
// import { darken } from '@material-ui/core/styles/colorManipulator';
import * as authActions from 'app/auth/store/actions';
import { Link } from 'react-router-dom';
import Formsy from 'formsy-react';
import React, { useRef, useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
	makeStyles,
	Card,
	CardContent,
	FormControl,
	FormControlLabel,
	Checkbox,
	Button,
	Typography
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
	root: {
		background: theme.palette.grey[900],
		color: theme.palette.primary.contrastText
	},
	heightCenter: {
		minHeight: '100vh'
	}
}));

function Login() {
	const dispatch = useDispatch();
	const login = useSelector(({ auth }) => auth.login);

	const [isFormValid, setIsFormValid] = useState(false);
	// const [showPassword, setShowPassword] = useState(false);

	const formRef = useRef(null);

	const classes = useStyles();

	const { form, handleChange, resetForm } = useForm({
		email: '',
		password: '',
		remember: true
	});

	useEffect(() => {
		if (login.error && (login.error.email || login.error.password)) {
			formRef.current.updateInputsWithError({
				...login.error
			});
			disableButton();
		}
	}, [login.error]);

	function disableButton() {
		setIsFormValid(false);
	}

	function enableButton() {
		setIsFormValid(true);
	}

	function handleSubmit(model) {
		console.log(model);

		dispatch(authActions.submitLogin(model));
		resetForm();
	}

	// function isFormValid() {
	// 	return form.email.length > 0 && form.password.length > 0;
	// }

	// function handleSubmit(ev) {
	// 	ev.preventDefault();
	// 	resetForm();
	// }

	return (
		<div className={clsx(classes.root, 'flex flex-col flex-auto flex-shrink-0 p-24 md:flex-row md:p-0')}>
			<div className="flex flex-col flex-grow-0 items-center text-white p-16 text-center md:p-128 md:items-start md:flex-shrink-0 md:flex-1 md:text-left">
				<FuseAnimate animation="transition.expandIn">
					<img className="w-128 mb-32" src="assets/images/logos/logo-square.png" alt="logo" />
				</FuseAnimate>

				<FuseAnimate animation="transition.slideUpIn" delay={300}>
					<Typography variant="h3" color="inherit" className="font-light">
						Welcome to the DAASTA!
					</Typography>
				</FuseAnimate>

				<FuseAnimate delay={400}>
					<Typography variant="subtitle1" color="inherit" className="max-w-512 mt-16">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ullamcorper nisl erat, vel
						convallis elit fermentum pellentesque. Sed mollis velit facilisis facilisis.
					</Typography>
				</FuseAnimate>
			</div>

			<FuseAnimate animation={{ translateX: [0, '100%'] }}>
				<Card className="w-full max-w-400 mx-auto m-16 md:m-0" square>
					<CardContent
						className={clsx(
							classes.heightCenter,
							'flex flex-col items-center justify-center p-32 md:p-48 '
						)}
					>
						<Typography variant="h6" className="md:w-full mb-32">
							LOGIN TO YOUR ACCOUNT
						</Typography>

						<Formsy
							onValidSubmit={handleSubmit}
							onValid={enableButton}
							onInvalid={disableButton}
							ref={formRef}
							className="flex flex-col justify-center w-full"
						>
							<TextFieldFormsy
								className="mb-16"
								type="text"
								name="email"
								label="Email"
								value=""
								validations={{
									isEmail: true,
									minLength: 0
								}}
								validationErrors={{
									isEmail: 'Enter a Vaild Email',
									minLength: 'Enter the Email'
								}}
								variant="outlined"
								required
							/>

							<TextFieldFormsy
								className="mb-16"
								type="password"
								name="password"
								label="Password"
								value=""
								validations={{
									minLength: 0
								}}
								validationErrors={{
									minLength: 'Enter the Password'
								}}
								variant="outlined"
								required
							/>
							<div className="flex items-center justify-between">
								<FormControl>
									<FormControlLabel
										control={
											<Checkbox name="remember" checked={form.remember} onChange={handleChange} />
										}
										label="Remember Me"
									/>
								</FormControl>

								<Link className="font-medium" to="/reset-password">
									Forgot Password?
								</Link>
							</div>

							<Button
								type="submit"
								variant="contained"
								color="primary"
								className="w-full mx-auto mt-16 normal-case"
								aria-label="LOG IN"
								disabled={!isFormValid}
								value="legacy"
							>
								Login
							</Button>
						</Formsy>
					</CardContent>
				</Card>
			</FuseAnimate>
		</div>
	);
}

export default Login;
