import React, { useEffect, useState } from 'react';
import {
	IconButton,
	Icon,
	Button,
	DialogTitle,
	DialogContent,
	Typography,
	DialogActions,
	Checkbox,
	FormControlLabel
} from '@material-ui/core';
import { useDispatch } from 'react-redux';
import * as FuseActions from 'app/store/actions';
import { useForm } from '@fuse/hooks';
import * as Actions from '../store/actions';

const InterpretersItems = ({ interpreter, session }) => {
	const dispatch = useDispatch();

	const defaultFormState = {
		id: null,
		eventId: null,
		roomId: null,
		user: '',
		status: 'pending',
		srcLang: '',
		srcTitle: '',
		dstLang: '',
		dstTitle: '',
		dstStreamId: null,
		srcStreamId: null,
		bidirectional: true,
		session: []
	};

	const roomSessionState = [];

	const { form, setInForm, setForm } = useForm(defaultFormState);
	const [sessionAssgin, setSessionAssgin] = useState(roomSessionState);

	sessionAssgin.map(c => (c.isChecked = form.session.indexOf(c.id) >= 0));

	useEffect(() => {
		if (interpreter !== []) {
			setForm({ ...defaultFormState, ...interpreter });
			setSessionAssgin([...roomSessionState, ...session]);
		}

		// eslint-disable-next-line
	}, [interpreter, setForm, setSessionAssgin]);

	const handleChangedSession = (e, checked) => {
		sessionAssgin.forEach(sess => {
			if (checked === true) {
				setInForm('session', [...form.session, Number(e.target.value)]);
				sess.isChecked = form.session.indexOf(sess.id) >= 0;
			} else {
				const newArray = form.session.filter(sid => sid !== Number(e.target.value));
				setInForm('session', newArray);
				sess.isChecked = form.session.indexOf(sess.id) >= 0;
			}
		});
	};

	const handleUpdate = () => {
		console.log('Session Update !');
		const data = { eventId: form.eventId, id: form.id, session: form.session };
		console.log('currentSessions', data);
		dispatch(Actions.updateInterpreter(data));
	};

	return (
		<>
			<div className="flex flex-col sm:flex-row">
				<div className="w-full sm:w-1/2 flex flex-row">
					<div className="flex flex-col w-1/2">
						<Typography variant="caption">Caption</Typography>
						<Typography variant="subtitle1">{form.user}</Typography>
					</div>
					<div className="flex flex-col w-1/2">
						<Typography variant="caption">From</Typography>
						<Typography variant="subtitle1">{form.srcTitle}</Typography>
					</div>
				</div>
				<div className="w-full sm:w-1/2 flex flex-row">
					<div className="flex flex-col w-1/2">
						<Typography variant="caption">To</Typography>
						<Typography variant="subtitle1">{form.dstTitle}</Typography>
					</div>
					<div className="flex flex-col w-1/2">
						<Typography variant="caption">Sessions</Typography>
						<Typography
							className="p-4 text-red-700"
							variant="subtitle1"
							// onClick={() => {
							// 	dispatch(
							// 		FuseActions.openDialog({
							// 			children: (
							// 				<>
							// 					<DialogTitle id="update-session-assign">Update Session ?</DialogTitle>
							// 					<DialogContent dividers className="p-24 flex flex-row">
							// 						<div className="flex flex-row flex-grow justify-start">
							// 							{sessionAssgin.map(sls => (
							// 								<FormControlLabel
							// 									key={sls.id}
							// 									className="m-8 truncate"
							// 									control={
							// 										<Checkbox
							// 											value={sls.id}
							// 											checked={sls.isChecked}
							// 											onChange={handleChangedSession}
							// 											name="session"
							// 											color="primary"
							// 										/>
							// 									}
							// 									label={`${sls.title} + ${sls.id}`}
							// 								/>
							// 							))}
							// 						</div>
							// 					</DialogContent>
							// 					<DialogActions>
							// 						<Button
							// 							color="primary"
							// 							onClick={() => {
							// 								dispatch(FuseActions.closeDialog());
							// 							}}
							// 						>
							// 							Cancel
							// 						</Button>
							// 						<Button
							// 							color="primary"
							// 							onClick={() => {
							// 								console.log(form.id);
							// 								dispatch(
							// 									Actions.updateInterpreter(
							// 										form.eventId,
							// 										form.id,
							// 										form.session
							// 									)
							// 								);
							// 							}}
							// 							autoFocus
							// 						>
							// 							OK
							// 						</Button>
							// 					</DialogActions>
							// 				</>
							// 			)
							// 		})
							// 	);
							// }}
						>
							Assigned to {form.session.length} session
						</Typography>
					</div>
				</div>

				<div className="flex justify-end">
					<IconButton
						onClick={() => {
							dispatch(
								FuseActions.openDialog({
									children: (
										<>
											<DialogTitle id="delete-room-item">Delete User ?</DialogTitle>
											<DialogContent dividers className="p-24 flex flex-row">
												<Typography className="flex flex-row text-center" variant="subtitle1">
													Are You want to Delete{' '}
													<p className="px-4 font-bold text-red-700">{form.user}</p> ?
												</Typography>
											</DialogContent>
											<DialogActions>
												<Button
													color="primary"
													onClick={() => {
														dispatch(FuseActions.closeDialog());
													}}
												>
													Cancel
												</Button>
												<Button
													color="primary"
													onClick={() => {
														console.log(form.id);
														dispatch(Actions.deleteInterpreter(form.id, form.eventId));
													}}
													autoFocus
												>
													OK
												</Button>
											</DialogActions>
										</>
									)
								})
							);
						}}
					>
						<Icon className="text-red-700">delete</Icon>
					</IconButton>
				</div>
			</div>
			<div className="w-full  flex flex-row">
				<div className="flex flex-wrap flex-row flex-warp flex-grow justify-start">
					{sessionAssgin.map(sls => (
						<FormControlLabel
							key={sls.id}
							className="m-8 truncate"
							control={
								<Checkbox
									value={sls.id}
									checked={sls.isChecked}
									onChange={handleChangedSession}
									onBlur={handleUpdate}
									name="session"
									color="primary"
								/>
							}
							label={`${sls.title}`}
						/>
					))}
				</div>
			</div>
		</>
	);
};

export default React.memo(InterpretersItems);
