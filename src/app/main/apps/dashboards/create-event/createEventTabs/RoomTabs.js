import React, { useState, useEffect } from 'react';
import { Button, CircularProgress } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { useSelector, useDispatch } from 'react-redux';
import * as Actions from '../store/actions';
import RoomTextItem from './RoomTextItem';

const RoomTabs = ({ handleNextTabChange, handlePreviousTabChange, eventId }) => {
	const dispatch = useDispatch();
	const roomTabData = useSelector(({ createEvent }) => createEvent.CreateEvent.roomsTab);
	const [roomset, setRoomset] = useState([]);
	const [loading, setLoading] = useState(false);

	useEffect(() => {
		if (roomTabData !== []) {
			setRoomset([...roomTabData]);
		}
	}, [roomTabData, setRoomset]);

	const handleAddRoom = () => {
		setLoading(true);
		console.log('Add More Click', roomTabData);
		const data = {
			eventId,
			room: [{ lang: '', title: `Room ${(roomset.length + 1).toString()}`, description: '' }]
		};

		dispatch(Actions.addNewRoom(data)).then(() => setLoading(false));
	};

	if (!roomTabData) {
		return null;
	}

	return (
		<div className="w-full">
			{roomTabData.map((allrooms, index) => (
				<RoomTextItem key={index} rooms={allrooms.rooms} eventId={allrooms.eventId} />
			))}
			<div className="w-full p-16 my-8 flex justify-center relative">
				{loading ? (
					<CircularProgress size={24} />
				) : (
					<Button
						className="w-full p-16"
						startIcon={<AddIcon />}
						disableElevation
						variant="outlined"
						onClick={handleAddRoom}
					>
						Add Room
					</Button>
				)}
			</div>

			<div className="w-full flex justify-between mt-8">
				<Button variant="contained" color="secondary" onClick={handlePreviousTabChange}>
					Previous Step
				</Button>
				<Button type="submit" variant="contained" color="secondary" onClick={handleNextTabChange}>
					Next Step
				</Button>
			</div>
		</div>
	);
};

export default React.memo(RoomTabs);
