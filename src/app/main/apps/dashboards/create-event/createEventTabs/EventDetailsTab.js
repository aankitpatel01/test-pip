import React, { useState, useEffect } from 'react';
import {
	Icon,
	FormControlLabel,
	Checkbox,
	TextField,
	Button,
	Zoom,
	Tooltip,
	Typography,
	makeStyles,
	CircularProgress
} from '@material-ui/core';
import { DatePicker, TimePicker } from '@material-ui/pickers';
import moment from 'moment';
import { useForm } from '@fuse/hooks';
// import _ from '@lodash';
// import FuseUtils from '@fuse/utils';
import clsx from 'clsx';
import DeleteIcon from '@material-ui/icons/Delete';
import { useDispatch, useSelector } from 'react-redux';
import { green } from '@material-ui/core/colors';
import History from '@history';
import FuseLoading from '@fuse/core/FuseLoading';
import GoogleMaps from './TabsElements/GoogleMaps';
import EventAttachment from './TabsElements/EventAttachment';
import * as Actions from '../store/actions';
import SearchPlace from './SearchPlace';

const useStyles = makeStyles(theme => ({
	productImageFeaturedStar: {
		position: 'absolute',
		top: 0,
		right: 0,
		opacity: 1
	},
	productImageUpload: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut
	},
	productImageItem: {
		transitionProperty: 'box-shadow',
		transitionDuration: theme.transitions.duration.short,
		transitionTimingFunction: theme.transitions.easing.easeInOut,
		'&:hover': {
			'& $productImageFeaturedStar': {
				opacity: 0.8
			}
		},
		'&.featured': {
			pointerEvents: 'none',
			boxShadow: theme.shadows[3],
			'& $productImageFeaturedStar': {
				opacity: 1
			},
			'&:hover $productImageFeaturedStar': {
				opacity: 1
			}
		}
	},
	radioGreen: {
		color: green[500],
		'&:checked': {
			color: green[100]
		}
	}
}));

const defaultFormState = {
	eventId: null,
	eventName: '',
	eventAddress: '',
	location: '',
	testEvent: false,
	repeatWeekly: false,
	date: null,
	startTime: null,
	endTime: null,
	coverImage: null,
	logo: null,
	theme: '',
	updatedAt: null,
	description: '',
	eventMaterial: null,
	participantsMaterial: null,
	status: 'new'
};

const EventDetailsTab = ({ handleNextTabChange }) => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const eventDetails = useSelector(({ createEvent }) => createEvent.CreateEvent.eventDetails);
	const [isFormValid, setIsFormValid] = useState(false);
	const { form, handleChange, setForm, setInForm } = useForm(defaultFormState);
	// loader for Image Uploads
	const [coverImageLoading, setCoverImageLoading] = useState(false);
	const [logoLoading, setLogoLoading] = useState(false);
	const [eventMaterialLoading, setEventMaterialLoading] = useState(false);

	// const startdate = moment(form.startdate, 'DD/MM/YYYY');
	// const endtime = moment(form.entime, 'DD/MM/YYYY').add(1, 'hours').format();

	useEffect(() => {
		enableButton();
	});

	useEffect(() => {
		console.log('Event Form Data', eventDetails);

		// eslint-disable-next-line
		setForm({ ...defaultFormState, ...eventDetails });

		// eslint-disable-next-line
	}, [eventDetails, setForm]);

	// Validate Event Name and country
	const enableButton = () => {
		if (form.eventName !== '' && form.location !== '') {
			setIsFormValid(true);
		} else {
			setIsFormValid(false);
		}
	};
	// Upload Image
	const handleUploadCover = (e, type) => {
		const file = e.target.files[0];
		// const fileURL = URL.createObjectURL(e.target.files[0]);
		// console.log('fileURL', fileURL);
		// setInForm('coverImageURL', fileURL);
		console.log('File data=>', type);
		if (!file) {
			return;
		}

		if (type === 'coverImage') {
			console.log('setCoverImageLoading =>', coverImageLoading);
			setCoverImageLoading(true);
		}
		if (type === 'logo') {
			console.log('setLogoLoading => ', logoLoading);
			setLogoLoading(true);
		}
		if (type === 'eventMaterial') {
			console.log('seteventMaterialLoading =>', eventMaterialLoading);
			setEventMaterialLoading(true);
		}

		const data = new FormData();
		data.append(type, file, file.name);
		data.append('eventId', form.eventId);
		data.append('eventName', form.eventName);

		Promise.all([dispatch(Actions.updateEventDetails(data))]).then(() => {
			setCoverImageLoading(false);
			setLogoLoading(false);
			setEventMaterialLoading(false);
		});
	};

	function deleteCover() {
		setInForm('coverImage', null);
		const data = { ...form, coverImage: null };
		dispatch(Actions.updateEventDetails(data));
		// handleSubmit();
	}
	function deleteLogo() {
		setInForm('logo', null);
		const data = { ...form, logo: null };
		dispatch(Actions.updateEventDetails(data));

		// handleSubmit();
	}

	function handleSubmit(e) {
		e.preventDefault();

		dispatch(Actions.updateEventDetails(form));

		// console.log('submit', form);
		handleNextTabChange();
		// resetForm();
	}

	const deleteEvent = () => {
		dispatch(Actions.deleteEvent(form.eventId)).then(() => History.push('/'));
	};

	const setLocationData = data => {
		console.log('setLocationData =>', data?.name);
		setInForm('location', data?.name);
	};

	// const addSessionsinEventDetails = () => {
	// 	const data = {
	// 		eventId: form.eventId,
	// 		// roomId: roomobj.roomId,
	// 		startTime: form.startTime,
	// 		endTime: form.endTime,
	// 		room: [{ lang: '', title: `Session 1`, description: '' }]
	// 	};
	// 	console.log('Session DataEvent TIme=>  ', data);
	// 	dispatch(Actions.addNewSession(data));
	// };

	// console.log('Event Detail Tab Component >', locData);

	return (
		<>
			<form className="w-full flex flex-wrap" onSubmit={handleSubmit}>
				<div className="w-full flex flex-col sm:flex-row">
					<div className="flex sm:w-1/2 md:w-1/2 flex-col">
						<TextField
							className="m-8"
							name="eventName"
							label="Event Name"
							variant="outlined"
							type="text"
							error={form.eventName === ''}
							helperText={form.eventName === '' ? 'Required' : ''}
							value={form.eventName || ''}
							onChange={handleChange}
							onBlur={() => dispatch(Actions.updateEventDetails(form))}
							required
						/>

						<FormControlLabel
							className="mx-4"
							control={
								<Checkbox
									color="secondary"
									checked={form.testEvent}
									onChange={handleChange}
									name="testEvent"
								/>
							}
							label="Test Event"
							labelPlacement="end"
						/>
					</div>

					<div className="flex sm:w-1/2 md:w-1/2 flex-col">
						{/* <TextField
							className="m-8"
							name="location"
							label="Country"
							variant="outlined"
							type="text"
							error={form.location === ''}
							helperText={form.location === '' ? 'Required' : ''}
							value={form.location}
							onChange={handleChange}
							onBlur={() => dispatch(Actions.updateEventDetails(form))}
							required
						/> */}
						<SearchPlace setLocationData={setLocationData} location={form.location} />
						<FormControlLabel
							className="mx-4"
							control={
								<Checkbox
									color="secondary"
									checked={form.repeatWeekly}
									onChange={handleChange}
									name="repeatWeekly"
								/>
							}
							label="Repeat Weekly"
							labelPlacement="end"
						/>
					</div>
				</div>

				<div className="w-full">
					<div className="flex">
						<DatePicker
							label="Start Date"
							inputVariant="outlined"
							name="startdate"
							value={form.date}
							onChange={date => setInForm('date', moment(date).toISOString())}
							className="mt-8 mb-16 w-full mx-4"
							minDate={form.date}
							format="DD-MMM-YYYY"
							fullWidth
						/>

						<TimePicker
							label="Start Time"
							inputVariant="outlined"
							name="starttime"
							value={form.startTime}
							onChange={time => setInForm('startTime', moment(time).toISOString())}
							className="mt-8 mb-16 w-full mx-4"
							fullWidth
						/>
						<TimePicker
							autoOk
							label="End Time"
							inputVariant="outlined"
							name="endtime"
							value={form.endTime}
							className="mt-8 mb-16 w-full mx-4"
							onChange={time => setInForm('endTime', moment(time).toISOString())}
						/>
					</div>

					<TextField
						className="mb-16"
						name="description"
						onChange={handleChange}
						onBlur={() => dispatch(Actions.updateEventDetails(form))}
						value={form.description}
						label="Description"
						type="text"
						multiline
						rows={5}
						variant="outlined"
						fullWidth
					/>
				</div>
				<div className="w-full flex flex-row">
					<div className="w-1/3 flex flex-col">
						<div className="w-full flex flex-row px-8">
							<Typography color="inherit">Cover Image </Typography>
							<Tooltip
								TransitionComponent={Zoom}
								placement="top-end"
								fontSize="small"
								title={
									<>
										<li>We recommend a picture without text on it</li>
										<li>Max size 1MB</li>
									</>
								}
							>
								<Icon>info</Icon>
							</Tooltip>
						</div>

						{}

						{form.coverImage === null ? (
							<label
								htmlFor="button-file"
								className="flex items-center justify-center relative h-200 rounded-4 mx-8 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5"
							>
								<input
									name="coverImage"
									accept="image/*"
									className="hidden"
									id="button-file"
									type="file"
									onChange={e => handleUploadCover(e, 'coverImage')}
								/>
								{coverImageLoading ? (
									<CircularProgress />
								) : (
									<Icon fontSize="large" color="action">
										cloud_upload
									</Icon>
								)}
							</label>
						) : (
							<>
								<div
									// onClick={() => setFeaturedImage(form.coverimage.id)}
									role="button"
									tabIndex={0}
									className={clsx(
										classes.productImageItem,
										'flex items-center justify-center relative h-200 rounded-4 mx-8 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5'
									)}
									key={form.coverImage?.id}
								>
									<DeleteIcon
										className={clsx(classes.productImageFeaturedStar, 'text-red-700')}
										onClick={deleteCover}
									/>
									<img
										className="max-w-none w-auto h-full object-cover"
										src={form.coverImage}
										alt="coverImages"
									/>
								</div>
							</>
						)}
					</div>

					<div className=" w-1/3 flex flex-col">
						<div className="w-full flex flex-row px-8">
							<Typography color="inherit">Logo </Typography>
							<Tooltip
								TransitionComponent={Zoom}
								placement="top-end"
								fontSize="small"
								title={
									<>
										<li>PNG file only</li>
										<li>Max size 1MB</li>
										<li>Transparent background</li>
										<li>Should contain only one color, preferably white</li>
										<li>At-least 300 pixels for width and height</li>
									</>
								}
							>
								<Icon>info</Icon>
							</Tooltip>
						</div>

						{form.logo === null ? (
							<label
								htmlFor="button-file2"
								className="flex items-center justify-center relative h-200 rounded-4 mx-8 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5"
							>
								<input
									name="logo"
									accept="image/*"
									className="hidden"
									id="button-file2"
									type="file"
									onChange={e => handleUploadCover(e, 'logo')}
								/>
								{logoLoading ? (
									<CircularProgress />
								) : (
									<Icon fontSize="large" color="action">
										cloud_upload
									</Icon>
								)}
							</label>
						) : (
							<div
								// onClick={() => setFeaturedImage(form.coverimage.id)}
								role="button"
								tabIndex={0}
								className={clsx(
									classes.productImageItem,
									'flex items-center justify-center relative h-200 rounded-4 mx-8 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5'
								)}
								key={form.logo.id}
							>
								<DeleteIcon
									className={clsx(classes.productImageFeaturedStar, 'text-red-700')}
									onClick={deleteLogo}
								/>
								<img className="max-w-none w-auto h-full object-cover" src={form.logo} alt="logo" />
							</div>
						)}
					</div>

					<div className="w-2/3 mx-4">
						<Typography color="inherit">Location </Typography>

						<GoogleMaps />
						{/* Theme Section  */}
						{/* <div className="m-4">
							<FormControl className="flex flex-1 flex-row">
								<Typography className="flex items-center pr-8" color="inherit">
									Theme{' '}
								</Typography>
								<RadioGroup
									className="flex flex-row flex-wrap"
									aria-label="Theme"p
									name="eventTheme"
									value={form.theme}
									onChange={handleChange}
								>
									<FormControlLabel
										className="text-red"
										value="red"
										control={<Radio color="primary" />}
									/>
									<FormControlLabel
										className="text-blue"
										value="blue"
										control={<Radio className={classes.radioGreen} />}
									/>
									<FormControlLabel className="text-green" value="green" control={<Radio />} />
									<FormControlLabel className="text-yellow-700" value="yellow" control={<Radio />} />
									<FormControlLabel className="text-purple" value="purple" control={<Radio />} />
									<FormControlLabel className="text-grey-700" value="grey" control={<Radio />} />
								</RadioGroup>
							</FormControl>
						</div> */}
					</div>
				</div>

				<div className="w-full">
					<div className="w-full flex flex-row px-8">
						<Typography color="inherit">Event Material</Typography>
						<Tooltip
							TransitionComponent={Zoom}
							placement="top-end"
							fontSize="small"
							title="These files will only be accessible to interpreters assigned to this event."
						>
							<Icon>info</Icon>
						</Tooltip>
					</div>

					{eventMaterialLoading ? (
						<div className="flex items-center justify-center relative  h-160 rounded-4 mx-8 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5">
							<FuseLoading />
						</div>
					) : (
						<label
							htmlFor="button-file3"
							className="flex items-center justify-center relative  h-160 rounded-4 mx-8 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5"
						>
							<input
								name="eventMaterial"
								accept="image/*"
								className="hidden"
								id="button-file3"
								type="file"
								onChange={e => handleUploadCover(e, 'eventMaterial')}
							/>
							<Icon fontSize="large" color="action">
								cloud_upload
							</Icon>
						</label>
					)}
				</div>

				<div className="pt-8 w-full">
					{form.eventMaterial !== null &&
						form.eventMaterial.map((material, index) => (
							<EventAttachment
								key={index}
								eventId={material.eventID}
								id={material._id}
								fileName={material.fileName}
								size="12 kb"
								url={material.url}
							/>
						))}
				</div>

				{/* <div className="w-full">
					<div className="w-full flex flex-row px-8">
						<Typography color="inherit">Participants Material</Typography>
						<Tooltip
							TransitionComponent={Zoom}
							placement="top-end"
							fontSize="small"
							title="These files will only be accessible to participants who are allowed to download files."
						>
							<Icon>info</Icon>
						</Tooltip>
					</div>
					<label
						htmlFor="button-file"
						className="flex items-center justify-center relative  h-160 rounded-4 mx-8 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5"
					>
						<input accept="image/*" className="hidden" id="button-file" type="file" />
						<Icon fontSize="large" color="action">
							cloud_upload
						</Icon>
					</label>
				</div>

				<div className="pt-8 w-full">
					<EventAttachment fileName="attachment-2.doc" size="12 kb" />
					<EventAttachment fileName="attachment-1.jpg" size="350 kb" />
				</div> */}

				<div className="w-full flex justify-between mt-8">
					<Button variant="contained" color="secondary" onClick={deleteEvent}>
						Delete Event
					</Button>
					<Button variant="contained" color="secondary" disabled={!isFormValid} onClick={handleSubmit}>
						Next Step
					</Button>
				</div>
			</form>
		</>
	);
};

export default React.memo(EventDetailsTab);
