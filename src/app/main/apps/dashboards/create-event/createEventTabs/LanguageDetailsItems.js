import React from 'react';
import { Icon, Button, Typography, Divider } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../store/actions';
import LanguageListItem from './LanguageListItem';

const LanguageDetailsItems = ({ room }) => {
	const dispatch = useDispatch();
	const languageTab = useSelector(({ createEvent }) => createEvent.CreateEvent.languageTab);
	const roomobj = room[0];

	// roomobj.filter(c => c.roomId === c.id);
	const roomLanguage = languageTab.filter(lang => lang.roomId === roomobj.roomId);

	return (
		<>
			<Typography variant="h5" className="m-16">
				{roomobj.title}
			</Typography>

			<Divider className="mb-16" />
			<div className="flex flex-col">
				{!roomLanguage ? (
					<Typography variant="subtitle2" className="m-8 flex justify-center">
						Language is not available
					</Typography>
				) : (
					roomLanguage.map(lang => <LanguageListItem key={lang.id} lang={lang} />)
				)}
			</div>

			<Button
				className="w-full p-16 my-8"
				startIcon={<Icon>add</Icon>}
				disableElevation
				variant="outlined"
				onClick={() => dispatch(Actions.openEventsDialog({ roomId: roomobj.roomId }))}
			>
				Add Language/Audio
			</Button>
		</>
	);
};

export default React.memo(LanguageDetailsItems);
