import React from 'react';
import { Typography, Button, Icon, Divider } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import * as Actions from '../store/actions';

const ParticipantsTabItem = ({ room }) => {
	const { roomid, roomtitle } = room;
	const dispatch = useDispatch();
	return (
		<>
			<Typography variant="h5" className="m-16">
				{roomtitle}
			</Typography>
			<Divider className="mb-16" />
			<div className="flex flex-col sm:flex-row">
				<div className="w-full sm:w-1/2 flex flex-row">
					<Button
						className="w-full p-16 m-16"
						startIcon={<Icon>add</Icon>}
						disableElevation
						variant="outlined"
						onClick={() => dispatch(Actions.openEventsDialog())}
					>
						INVITE PARTICIPANTS INDIVIDUALLY
					</Button>
				</div>
				<div className="w-full sm:w-1/2 flex flex-row">
					<Button
						className="w-full p-16 m-16"
						startIcon={<Icon>add</Icon>}
						disableElevation
						variant="outlined"
						onClick={() => dispatch(Actions.openShareableDialog())}
					>
						ADD SHAREABLE LINK
					</Button>
				</div>
			</div>
		</>
	);
};

export default ParticipantsTabItem;
