import React, { useState, useEffect } from 'react';
import {
	TextField,
	IconButton,
	Icon,
	Button,
	DialogTitle,
	DialogContent,
	Typography,
	DialogActions
} from '@material-ui/core';
import { TimePicker, DatePicker } from '@material-ui/pickers';
import moment from 'moment';
import { useForm } from '@fuse/hooks';
import { useDispatch } from 'react-redux';
import * as FuseActions from 'app/store/actions';
import * as Actions from '../store/actions';

const SessionDetailsItem = ({ agenda }) => {
	const dispatch = useDispatch();

	const defaultFormState = {
		eventId: null,
		id: null,
		title: '',
		description: '',
		startTime: null,
		endTime: null,
		recEndTime: null,
		recstartTime: null,
		roomId: null
	};

	const { form, handleChange, setInForm, setForm } = useForm(defaultFormState);

	useEffect(() => {
		if (agenda !== []) {
			setForm({ ...form, ...agenda });
		}
		// eslint-disable-next-line
	}, [agenda, setForm]);

	const handleUpdate = () => {
		console.log('Session Update !');
		const data = {
			eventId: form.eventId,
			roomId: form.roomId,
			id: form.id,
			startTime: form.startTime,
			EndTime: form.endTime,
			room: [{ lang: '', title: form.title, description: form.description }]
		};
		dispatch(Actions.updateAgenda(data));
	};

	const [descBox, setDescBox] = useState(false);
	return (
		<>
			<div className="flex flex-col sm:flex-row" onFocus={() => setDescBox(true)}>
				<div className="w-full sm:w-1/2 flex flex-row p-4">
					<TextField
						className="flex w-1/2 flex-col text-center m-4"
						label="Session Name"
						name="title"
						value={form.title}
						error={form.title === ''}
						// helperText={form.title === '' ? 'Required' : ''}
						variant="outlined"
						type="text"
						onChange={handleChange}
						onBlur={handleUpdate}
						required
					/>

					<DatePicker
						className="flex w-1/2 justify-center m-4"
						label="Date"
						inputVariant="outlined"
						name="date"
						value={new Date()}
						onChange={date => console.log('start', moment(date).toISOString())}
						onBlur={handleUpdate}
						format="DD-MMM-YYYY"
						fullWidth
					/>
				</div>
				<div className="w-full sm:w-1/2 flex flex-row p-4">
					<TimePicker
						className="flex w-2/5 justify-center m-4"
						label="Start"
						inputVariant="outlined"
						name="startTime"
						value={form.recstartTime}
						onChange={time => setInForm('startTime', moment(time).toISOString())}
						onBlur={handleUpdate}
						// minDate=""
						fullWidth
					/>

					<TimePicker
						className="flex w-2/5 justify-center m-4"
						label="End"
						inputVariant="outlined"
						name="endTime"
						value={form.recEndTime}
						onChange={time => setInForm('endTime', moment(time).toISOString())}
						onBlur={handleUpdate}
						// minDate=""
						fullWidth
					/>

					<IconButton
						onClick={() => {
							dispatch(
								FuseActions.openDialog({
									children: (
										<>
											<DialogTitle id="delete-room-item">Delete User ?</DialogTitle>
											<DialogContent dividers className="p-24 flex flex-row">
												<Typography className="flex flex-row text-center" variant="subtitle1">
													Are You want to Delete{' '}
													<p className="px-4 font-bold text-red-700">Session</p> ?
												</Typography>
											</DialogContent>
											<DialogActions>
												<Button
													color="primary"
													onClick={() => {
														dispatch(FuseActions.closeDialog());
													}}
												>
													Cancel
												</Button>
												<Button
													color="primary"
													onClick={() => {
														console.log('delete this Session ');
														dispatch(Actions.deleteAgenda(form.id, form.eventId));
													}}
													autoFocus
												>
													OK
												</Button>
											</DialogActions>
										</>
									)
								})
							);
						}}
					>
						<Icon className="text-red-700">delete</Icon>
					</IconButton>
				</div>
			</div>

			{descBox === true && (
				<TextField
					onBlur={() => {
						setDescBox(false);
						handleUpdate();
					}}
					className="my-16"
					name="description"
					onChange={handleChange}
					value={form.description}
					label="Description"
					type="text"
					multiline
					rows={5}
					variant="outlined"
					fullWidth
				/>
			)}
		</>
	);
};

export default React.memo(SessionDetailsItem);
