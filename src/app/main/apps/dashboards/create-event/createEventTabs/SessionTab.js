import React from 'react';
import { Button } from '@material-ui/core';
import { useSelector } from 'react-redux';
import SessionListItems from './SessionListItems';

const SessionTab = ({ handleNextTabChange, handlePreviousTabChange, eventId }) => {
	const roomTabData = useSelector(({ createEvent }) => createEvent.CreateEvent.roomsTab);
	// console.log(roomTabData);

	return (
		<div className="flex flex-col">
			{roomTabData.map(room => (
				<SessionListItems key={room.id} room={room.rooms} eventId={eventId} />
			))}

			<div className="w-full flex justify-between mt-8">
				<Button variant="contained" color="secondary" onClick={handlePreviousTabChange}>
					Previous Step
				</Button>
				<Button type="submit" variant="contained" color="secondary" onClick={handleNextTabChange}>
					Next Step
				</Button>
			</div>
		</div>
	);
};

export default SessionTab;
