import React from 'react';
import { Button } from '@material-ui/core';
import { useSelector } from 'react-redux';
import LanguageDetailsItems from './LanguageDetailsItems';

const LanguageDetailsTabs = ({ handleNextTabChange, handlePreviousTabChange }) => {
	const roomTabData = useSelector(({ createEvent }) => createEvent.CreateEvent.roomsTab);

	console.log('roomTabData', roomTabData);

	if (!roomTabData) {
		return null;
	}

	return (
		<div className="flex flex-col">
			{roomTabData.map(room => (
				<LanguageDetailsItems key={room.id} room={room.rooms} />
			))}

			<div className="w-full flex justify-between mt-8">
				<Button variant="contained" color="secondary" onClick={handlePreviousTabChange}>
					Previous Step
				</Button>
				<Button type="submit" variant="contained" color="secondary" onClick={handleNextTabChange}>
					Next Step
				</Button>
			</div>
		</div>
	);
};

export default LanguageDetailsTabs;
