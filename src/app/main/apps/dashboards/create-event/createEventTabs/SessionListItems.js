import React, { useState } from 'react';
import { Icon, Button, Typography, Divider, CircularProgress } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import * as Actions from '../store/actions';
import SessionDetailsItem from './SessionDetailsItem';

const SessionListItems = ({ room, eventId }) => {
	const dispatch = useDispatch();
	const agendaData = useSelector(({ createEvent }) => createEvent.CreateEvent.agendaTab);
	const roomobj = room[0];
	const [loading, setLoading] = useState(false);

	const roomAgenda = agendaData.filter(session => session.roomId === roomobj.roomId);

	const startTime = new Date().toISOString();
	const endTime = moment(startTime).add(1, 'hours').toISOString();

	console.log('startTime AGENDA', startTime);
	console.log('endTime AGENDA', endTime);

	const handleAddSession = () => {
		setLoading(true);
		const data = {
			eventId,
			roomId: roomobj.roomId,
			startTime,
			endTime,
			room: [{ lang: '', title: `Session ${(agendaData.length + 1).toString()}`, description: '' }]
		};
		console.log(data);
		dispatch(Actions.addNewSession(data)).then(() => setLoading(false));
	};

	return (
		<>
			<Typography variant="h5" className="m-16">
				{roomobj.title}
			</Typography>
			<Divider className="mb-16" />

			{!roomAgenda ? (
				<Typography variant="subtitle2" className="m-8 flex justify-center">
					Session is not available
				</Typography>
			) : (
				roomAgenda.map(agenda => <SessionDetailsItem key={agenda.id} agenda={agenda} />)
			)}

			{/* <Button
				className="w-full p-16 my-8"
				startIcon={<Icon>add</Icon>}
				variant="outlined"
				onClick={handleAddSession}
			>
				Add Session
			</Button> */}
			<div className="w-full p-16 my-8 flex justify-center relative">
				{loading ? (
					<CircularProgress size={24} />
				) : (
					<Button
						className="w-full p-16 "
						startIcon={<Icon>add</Icon>}
						variant="outlined"
						disabled={loading}
						onClick={handleAddSession}
					>
						Add Session
					</Button>
				)}
			</div>
		</>
	);
};

export default React.memo(SessionListItems);
