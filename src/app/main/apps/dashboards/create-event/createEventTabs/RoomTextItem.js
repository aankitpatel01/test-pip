import React from 'react';

import RoomTextField from './RoomTextField';

const RoomTextItem = ({ rooms, eventId }) => {
	console.log('Room RoomTextItem', rooms);
	return (
		<div className="p-8 w-full flex flex-row">
			{rooms.map((room, index) => (
				<RoomTextField key={index} room={room} eventId={eventId} />
			))}
		</div>
	);
};

export default React.memo(RoomTextItem);
