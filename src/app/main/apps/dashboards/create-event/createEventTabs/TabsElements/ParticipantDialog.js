import { useForm } from '@fuse/hooks';
// import FuseUtils from '@fuse/utils';
// import _ from '@lodash';
// import moment from 'moment/moment';
import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
	DialogTitle,
	DialogContent,
	Button,
	Dialog,
	MenuItem,
	DialogActions,
	TextField,
	Select,
	InputLabel,
	FormControl,
	FormControlLabel,
	Checkbox,
	Typography
} from '@material-ui/core';
import * as Actions from '../../store/actions';

const defaultFormState = {
	id: '',
	email: '',
	strmlangauge: '',
	strmvideo: '',
	allowchat: true,
	allowpolls: true,
	allowupload: true,
	allowdownload: true,
	session: true
	// startDate: new Date(),
	// dueDate: new Date()
};

function ParticipantDialog(props) {
	const dispatch = useDispatch();
	const eventDialog = useSelector(({ createEvent }) => createEvent.CreateEvent.eventDialog);

	const [isFormValid, setIsFormValid] = useState(false);
	const { form, handleChange, setForm } = useForm(defaultFormState);

	const initDialog = useCallback(() => {
		/**
		 * Dialog type: 'new'
		 */
		if (eventDialog.type === 'new') {
			setForm({
				...defaultFormState,
				...eventDialog.data
			});
		}
	}, [eventDialog.data, eventDialog.type, setForm]);

	useEffect(() => {
		/**
		 * After Dialog Open
		 */
		if (eventDialog.props.open) {
			initDialog();
		}
	}, [eventDialog.props.open, initDialog]);

	useEffect(() => {
		// Validation or Dialog submit button for different Dialog
		if (eventDialog.type !== 'share') {
			enableButton();
		}

		console.log('Form', form);
		console.log('eventDialog', eventDialog);
	});

	function closeParticipantDialog() {
		return dispatch(Actions.closeEventDialog());
	}

	const enableButton = () => {
		if (form.email !== '') {
			setIsFormValid(true);
		} else {
			setIsFormValid(false);
		}
	};

	const handleSubmit = e => {
		e.preventDefault();
		console.log(form);

		// dispatch(Actions.addUser(data));
	};

	return (
		<Dialog {...eventDialog.props} onClose={closeParticipantDialog} fullWidth disableBackdropClick maxWidth="sm">
			<DialogTitle>Assignment Details</DialogTitle>
			<DialogContent dividers classes={{ root: 'p-0' }}>
				<form
					noValidate
					className="flex flex-col"
					onSubmit={handleSubmit}

					// ref={formRef}
				>
					{eventDialog.type === 'new' && (
						<TextField
							className="m-8"
							name="email"
							onChange={handleChange}
							value={form.emails}
							label="Email"
							variant="outlined"
							type="text"
							required
						/>
					)}
					<div className="flex flex-row">
						<FormControl variant="outlined" className="m-8 w-1/2 sm:w-1/2">
							<InputLabel id="srclanguage">Stream Language</InputLabel>
							<Select
								name="strmlangauge"
								onChange={handleChange}
								value={form.strmlangauge}
								label="Stream Language"
								labelId="srclanguage"
								required
							>
								<MenuItem value="English">English</MenuItem>
								<MenuItem value="Hindi">Hindi</MenuItem>
								<MenuItem value="German">German</MenuItem>
							</Select>
						</FormControl>

						<FormControl variant="outlined" className="m-8 w-1/2 sm:w-1/2">
							<InputLabel id="languagelabel">Stream Video</InputLabel>
							<Select
								name="strmvideo"
								onChange={handleChange}
								value={form.strmvideo}
								label="Stream Video"
								labelId="languagelabel"
								required
							>
								<MenuItem value="English">English</MenuItem>
								<MenuItem value="Hindi">Hindi</MenuItem>
								<MenuItem value="German">German</MenuItem>
							</Select>
						</FormControl>
					</div>

					<Typography variant="h6" className="mx-16">
						Settings
					</Typography>
					<div className="flex flex-row flex-wrap">
						<FormControlLabel
							className="m-4 "
							control={
								<Checkbox
									name="allowchat"
									checked={form.allowchat}
									onChange={handleChange}
									color="primary"
								/>
							}
							label="Allow Chat"
						/>
						<FormControlLabel
							className="m-4 "
							control={
								<Checkbox
									name="allowpolls"
									checked={form.allowpolls}
									onChange={handleChange}
									color="primary"
								/>
							}
							label="Allow Polls"
						/>
						<FormControlLabel
							className="m-4 "
							control={
								<Checkbox
									name="allowupload"
									checked={form.allowupload}
									onChange={handleChange}
									color="primary"
								/>
							}
							label="Allow Upload Files"
						/>
						<FormControlLabel
							className="m-4 "
							control={
								<Checkbox
									name="allowdownload"
									checked={form.allowdownload}
									onChange={handleChange}
									color="primary"
								/>
							}
							label="Allow Download Files"
						/>
					</div>

					<Typography variant="h6" className="mx-16">
						Sessions
					</Typography>
					<div className="flex flex-row flex-wrap">
						<FormControlLabel
							className="m-4 truncate"
							control={
								<Checkbox
									name="session"
									checked={form.session}
									onChange={handleChange}
									color="primary"
								/>
							}
							label="Session 1"
						/>
						<FormControlLabel
							className="m-4"
							control={
								<Checkbox
									checked={form.session}
									onChange={handleChange}
									name="session"
									color="primary"
								/>
							}
							label="Session"
						/>
						<FormControlLabel
							className="m-4"
							control={
								<Checkbox
									checked={form.session}
									onChange={handleChange}
									name="session"
									color="primary"
								/>
							}
							label="Session new test"
						/>
						<FormControlLabel
							className="m-4"
							control={
								<Checkbox
									checked={form.session}
									onChange={handleChange}
									name="session"
									color="primary"
								/>
							}
							label="Session"
						/>
					</div>
				</form>
			</DialogContent>
			{eventDialog.type === 'new' ? (
				<DialogActions className="p-8 w-full flex justify-end">
					<Button className="mx-8" onClick={closeParticipantDialog} color="primary">
						Cancel
					</Button>
					<Button
						className="mx-8"
						type="submit"
						variant="contained"
						color="secondary"
						onClick={handleSubmit}
						disabled={!isFormValid}
					>
						OK
					</Button>
				</DialogActions>
			) : (
				<DialogActions className="p-8 w-full flex justify-end">
					<Button className="mx-8" onClick={closeParticipantDialog} color="primary">
						Cancel
					</Button>
					<Button className="mx-8" type="submit" variant="contained" color="secondary" onClick={handleSubmit}>
						OK
					</Button>
				</DialogActions>
			)}
		</Dialog>
	);
}

export default ParticipantDialog;
