import Card from '@material-ui/core/Card';
import Icon from '@material-ui/core/Icon';
import Tooltip from '@material-ui/core/Tooltip';
import GoogleMap from 'google-map-react';
import React from 'react';

function Marker(props) {
	return (
		<Tooltip title={props.text} placement="top">
			<Icon className="text-red">place</Icon>
		</Tooltip>
	);
}

function GoogleMaps(props) {
	return (
		<Card className="w-full h-200 rounded-8 shadow-none border-1">
			<GoogleMap
				bootstrapURLKeys={{
					key: process.env.REACT_APP_MAP_KEY
				}}
				defaultZoom={12}
				defaultCenter={[-34.397, 150.64]}
			>
				<Marker text="Marker Text" lat="-34.397" lng="150.644" />
			</GoogleMap>
		</Card>
	);
}

export default GoogleMaps;
