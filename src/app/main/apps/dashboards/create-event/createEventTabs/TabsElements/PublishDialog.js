import { useForm } from '@fuse/hooks';
import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
	DialogTitle,
	DialogContent,
	Button,
	Dialog,
	MenuItem,
	DialogActions,
	TextField,
	Select,
	InputLabel,
	FormControl
} from '@material-ui/core';
import * as Actions from '../../store/actions';

const defaultFormState = {
	eventId: null,
	lat: '12.89233',
	lag: '42.28939',
	wifiName: '',
	externalIp: '',
	accesslevel: '',
	code: ''
};

const PublishDialog = () => {
	const dispatch = useDispatch();
	const eventDialog = useSelector(({ createEvent }) => createEvent.CreateEvent.eventDialog);

	const { form, handleChange, setForm } = useForm(defaultFormState);

	const initDialog = useCallback(() => {
		/**
		 * Dialog type: 'new'
		 */
		if (eventDialog.type === 'new') {
			setForm({
				...defaultFormState,
				...eventDialog.data
			});
		}
	}, [eventDialog.data, eventDialog.type, setForm]);

	useEffect(() => {
		/**
		 * After Dialog Open
		 */
		if (eventDialog.props.open) {
			initDialog();
		}
	}, [eventDialog.props.open, initDialog]);

	function closeLanguageDialog() {
		console.log('closeLanguageDialog');
		return dispatch(Actions.closeEventDialog());
	}

	const handleSubmit = e => {
		e.preventDefault();
		console.log(form);

		const data = {
			eventId: form.eventId,
			lat: form.lat,
			lag: form.lag,
			wifiName: form.wifiName,
			externalIp: form.externalIp,
			accesslevel: form.accesslevel,
			code: form.code
		};

		dispatch(Actions.updateEventDetails(data));
	};

	return (
		<Dialog {...eventDialog.props} onClose={closeLanguageDialog} fullWidth disableBackdropClick maxWidth="sm">
			<DialogTitle>Event Access Setting</DialogTitle>
			<DialogContent dividers>
				<form noValidate className="flex flex-col" onSubmit={handleSubmit}>
					<div className="flex flex-row">
						<TextField
							className="m-8 w-1/2"
							name="lat"
							value={form.lat}
							disabled
							label="Lat"
							variant="outlined"
							type="text"
							required
						/>

						<TextField
							className="m-8 w-1/2"
							name="lag"
							value={form.lag}
							disabled
							label="Lag"
							variant="outlined"
							type="text"
							required
						/>
					</div>
					<div className="flex flex-row">
						<TextField
							className="m-8 w-1/2"
							name="wifiName"
							onChange={handleChange}
							value={form.wifiName}
							label="Wi-Fi"
							variant="outlined"
							type="text"
							required
						/>

						<TextField
							className="m-8 w-1/2"
							name="externalIp"
							onChange={handleChange}
							value={form.externalIp}
							label="IP Address"
							variant="outlined"
							type="text"
							required
						/>
					</div>
					<div className="flex flex-row">
						<FormControl variant="outlined" className="m-8 w-1/2">
							<InputLabel id="accesslabel">Access Level</InputLabel>
							<Select
								name="accesslevel"
								onChange={handleChange}
								value={form.accesslevel}
								label="Acess Level"
								required
							>
								<MenuItem value="public">Public</MenuItem>
								<MenuItem value="shared">Shared</MenuItem>
								<MenuItem value="individual">Individual</MenuItem>
							</Select>
						</FormControl>

						<TextField
							className="m-8 w-1/2"
							name="code"
							onChange={handleChange}
							value={form.code}
							label="Access Code"
							variant="outlined"
							type="text"
							required
						/>
					</div>
				</form>
			</DialogContent>

			<DialogActions className="p-8 w-full flex justify-end">
				<Button className="mx-8" onClick={() => dispatch(Actions.closeEventDialog())} color="primary">
					Cancel
				</Button>
				<Button
					className="mx-8"
					type="submit"
					variant="contained"
					color="secondary"
					onClick={handleSubmit}
					// disabled={!isFormValidVideo}
				>
					OK
				</Button>
			</DialogActions>
		</Dialog>
	);
};

export default PublishDialog;
