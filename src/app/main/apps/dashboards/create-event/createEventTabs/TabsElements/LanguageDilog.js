import { useForm } from '@fuse/hooks';
// import _ from '@lodash';
// import moment from 'moment/moment';
import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
	DialogTitle,
	DialogContent,
	Button,
	Dialog,
	MenuItem,
	DialogActions,
	TextField,
	Select,
	InputLabel,
	FormControl,
	FormControlLabel,
	Checkbox
} from '@material-ui/core';
import * as Actions from '../../store/actions';

const defaultFormState = {
	roomId: null,
	mode: 'audio',
	language: '',
	title: '',
	lang: '',
	record: false,
	public: false

	// startDate: new Date(),
	// dueDate: new Date()
};

function LanguageDilog({ eventId }) {
	const dispatch = useDispatch();
	const eventDialog = useSelector(({ createEvent }) => createEvent.CreateEvent.eventDialog);
	const languageList = useSelector(({ createEvent }) => createEvent.CreateEvent.languageList);
	const [isFormValid, setIsFormValid] = useState(false);
	const [isFormValidVideo, setIsFormValidVideo] = useState(false);
	const { form, handleChange, setForm, setInForm } = useForm(defaultFormState);

	const initDialog = useCallback(() => {
		/**
		 * Dialog type: 'new'
		 */
		if (eventDialog.type === 'new') {
			setForm({
				...defaultFormState,
				...eventDialog.data
			});
		}
	}, [eventDialog.data, eventDialog.type, setForm]);

	useEffect(() => {
		/**
		 * After Dialog Open
		 */
		if (eventDialog.props.open) {
			initDialog();
		}
	}, [eventDialog.props.open, initDialog]);

	useEffect(() => {
		// Validation or langauge and title
		enableButton();
		// console.log('Form', form);
		// console.log('eventDialog', eventDialog);
	});

	function closeLanguageDialog() {
		console.log('closeLanguageDialog');
		return dispatch(Actions.closeEventDialog());
	}

	const enableButton = () => {
		if (form.language !== '' && form.title !== '') {
			setIsFormValid(true);
		} else {
			setIsFormValid(false);
		}
		if (form.title !== '') {
			setIsFormValidVideo(true);
		} else {
			setIsFormValidVideo(false);
		}
	};

	const handleChangeMode = e => {
		setInForm('mode', e.target.value);
	};

	const handleChangelang = (e, child) => {
		setInForm('language', e.target.value);
		setInForm('title', e.target.value);
		setInForm('lang', child.props.dataiso);
	};

	const handleSubmit = e => {
		e.preventDefault();
		console.log(form);

		if (form.mode === 'audio') {
			const data = {
				eventId,
				roomId: form.roomId,
				type: [form.mode],
				lang: form.lang,
				title: form.title,
				record: form.record,
				public: form.public
			};

			console.log('audiosubmit', data);
			dispatch(Actions.addNewLanguage(data));
		} else {
			const data = {
				id: form.id,
				mode: form.mode,
				title: form.title
			};
			console.log('Videosubmit', data);
		}

		// dispatch(Actions.addUser(data));
		// setRoles('');
		// setUseremail('');
	};

	return (
		<Dialog {...eventDialog.props} onClose={closeLanguageDialog} fullWidth disableBackdropClick maxWidth="sm">
			<DialogTitle>Add language/video source</DialogTitle>
			<DialogContent dividers classes={{ root: 'p-0' }}>
				<FormControl variant="outlined" className="flex flex-col m-8">
					<InputLabel id="languagelabel">Mode</InputLabel>
					<Select
						className="w-full"
						name="mode"
						onChange={handleChangeMode}
						value={form.mode}
						label="Mode"
						labelId="languagelabel"
						required
					>
						<MenuItem value="audio">Audio</MenuItem>
						<MenuItem value="video">Video</MenuItem>
					</Select>
				</FormControl>
				{form.mode === 'audio' ? (
					<form
						noValidate
						className="flex flex-col"
						onSubmit={handleSubmit}

						// ref={formRef}
					>
						<div className="flex flex-row">
							<FormControl variant="outlined" className="m-8 w-1/2">
								<InputLabel id="languagelabel">Language</InputLabel>
								<Select
									name="language"
									onChange={handleChangelang}
									value={form.language}
									label="Language"
									required
								>
									{languageList.map(language => (
										<MenuItem key={language.id} dataiso={language.iso} value={language.englishName}>
											{`${language.englishName} (${language.name})`}
										</MenuItem>
									))}
								</Select>
							</FormControl>

							<TextField
								className="m-8 w-1/2"
								name="title"
								onChange={handleChange}
								value={form.title}
								label="Title"
								variant="outlined"
								type="text"
								required
							/>
						</div>
						<div className="flex flex-row justify-center">
							<FormControlLabel
								className="m-8 w-1/2"
								control={
									<Checkbox
										checked={form.record}
										onChange={handleChange}
										name="record"
										color="primary"
									/>
								}
								label="Record"
							/>
							<FormControlLabel
								className="m-8 w-1/2"
								control={
									<Checkbox
										checked={form.public}
										onChange={handleChange}
										name="public"
										color="primary"
									/>
								}
								label="Public"
							/>
						</div>
					</form>
				) : (
					<form
						className="flex flex-col"
						onSubmit={handleSubmit}

						// ref={formRef}
					>
						<TextField
							className="m-8"
							name="title"
							onChange={handleChange}
							value={form.title}
							label="Title"
							variant="outlined"
							type="text"
							required
						/>
					</form>
				)}
			</DialogContent>
			{form.mode === 'audio' ? (
				<DialogActions className="p-8 w-full flex justify-end">
					<Button className="mx-8" onClick={closeLanguageDialog} color="primary">
						Cancel
					</Button>
					<Button
						className="mx-8"
						type="submit"
						variant="contained"
						color="secondary"
						onClick={handleSubmit}
						disabled={!isFormValid}
					>
						OK
					</Button>
				</DialogActions>
			) : (
				<DialogActions className="p-8 w-full flex justify-end">
					<Button className="mx-8" onClick={() => dispatch(Actions.closeEventDialog())} color="primary">
						Cancel
					</Button>
					<Button
						className="mx-8"
						type="submit"
						variant="contained"
						color="secondary"
						onClick={handleSubmit}
						disabled={!isFormValidVideo}
					>
						OK
					</Button>
				</DialogActions>
			)}
		</Dialog>
	);
}

export default LanguageDilog;
