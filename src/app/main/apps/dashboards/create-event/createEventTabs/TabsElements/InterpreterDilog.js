import { useForm } from '@fuse/hooks';
// import FuseUtils from '@fuse/utils';
// import _ from '@lodash';
// import moment from 'moment/moment';
import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
	DialogTitle,
	DialogContent,
	Button,
	Dialog,
	MenuItem,
	DialogActions,
	TextField,
	Select,
	InputLabel,
	FormControl,
	FormControlLabel,
	Checkbox,
	Typography
} from '@material-ui/core';
import * as Actions from '../../store/actions';

const defaultFormState = {
	eventId: null,
	roomId: null,
	user: '',
	status: 'pending',
	srcLang: '',
	srcTitle: '',
	dstLang: '',
	dstTitle: '',
	dstStreamId: null,
	srcStreamId: null,
	bidirectional: true,
	session: []
};

const avalibleLanguageState = [];
const roomSessionState = [];

function InterpeterDialog({ eventId }) {
	const dispatch = useDispatch();
	const eventDialog = useSelector(({ createEvent }) => createEvent.CreateEvent.eventDialog);

	const [sessionlang, setSessionlang] = useState(avalibleLanguageState);
	const [sessionAssgin, setSessionAssgin] = useState(roomSessionState);
	const [isFormValid, setIsFormValid] = useState(false);
	const { form, handleChange, setInForm, resetForm } = useForm(defaultFormState);

	const initDialog = useCallback(() => {
		/**
		 * Dialog type: 'new'
		 */
		if (eventDialog.type === 'new') {
			setInForm('roomId', eventDialog.data.roomId);
			setInForm('eventId', eventId);
			setSessionAssgin([...roomSessionState, ...eventDialog.data.session]);
			setSessionlang([...avalibleLanguageState, ...eventDialog.data.langarr]);
		}
	}, [eventDialog.data, eventDialog.type, setSessionlang, setInForm, eventId]);

	useEffect(() => {
		/**
		 * After Dialog Open
		 */
		if (eventDialog.props.open) {
			initDialog();
		}
	}, [eventDialog.props.open, initDialog]);

	useEffect(() => {
		// Validation or langauge and title
		enableButton();
	});

	const enableButton = () => {
		if (form.user !== '' && form.srclanguage !== '' && form.dstlanguage !== '') {
			setIsFormValid(true);
		} else {
			setIsFormValid(false);
		}
	};

	const handleChangesrcLang = (e, child) => {
		setInForm('srcLang', e.target.value);
		setInForm('srcTitle', child.props.langtitile);
		setInForm('srcStreamId', child.props.streamid);
	};

	const handleChangedstLang = (e, child) => {
		setInForm('dstLang', e.target.value);
		setInForm('dstTitle', child.props.langtitile);
		setInForm('dstStreamId', child.props.streamid);
	};

	const handleChangedSession = (e, checked) => {
		sessionAssgin.forEach(session => {
			if (checked === true) {
				setInForm('session', [...form.session, e.target.value]);
			} else {
				const newArray = form.session.filter(sid => sid !== e.target.value);
				setInForm('session', newArray);
			}
		});
	};

	const closeInterpeterDialog = () => {
		resetForm();
		return dispatch(Actions.closeEventDialog());
	};

	const handleSubmit = e => {
		e.preventDefault();
		console.log(form);

		dispatch(Actions.addInterpreter(form));
		// empty Form after Submit
		// resetForm();
		setInForm('user', '');
	};

	return (
		<Dialog {...eventDialog.props} onClose={closeInterpeterDialog} fullWidth disableBackdropClick maxWidth="sm">
			<DialogTitle>Assignment Details</DialogTitle>
			<DialogContent dividers classes={{ root: 'p-0' }}>
				<form
					noValidate
					className="flex flex-col"
					onSubmit={handleSubmit}

					// ref={formRef}
				>
					<div className="flex flex-row">
						<TextField
							className="m-8 w-1/3"
							name="user"
							onChange={handleChange}
							value={form.user}
							label="Email"
							variant="outlined"
							type="text"
							required
						/>

						<FormControl variant="outlined" className="m-8 w-1/3">
							<InputLabel id="srclanguage">From</InputLabel>
							<Select
								name="srcLang"
								onChange={handleChangesrcLang}
								value={form.srcLang}
								label="From"
								labelId="srclanguage"
								required
							>
								{sessionlang.map(language => (
									<MenuItem
										key={language.id}
										streamid={language.id}
										langtitile={language.title}
										value={language.lang}
									>
										{language.title}
									</MenuItem>
								))}
							</Select>
						</FormControl>

						<FormControl variant="outlined" className="m-8 w-1/3">
							<InputLabel id="dstlanguage">To</InputLabel>
							<Select
								name="dstLang"
								onChange={handleChangedstLang}
								value={form.dstLang}
								label="To"
								labelId="dstlanguage"
								required
							>
								{sessionlang.map(language => (
									<MenuItem
										key={language.id}
										streamid={language.id}
										langtitile={language.title}
										value={language.lang}
									>
										{language.title}
									</MenuItem>
								))}
							</Select>
						</FormControl>
					</div>

					<div className="w-full">
						<FormControlLabel
							className="m-8"
							control={
								<Checkbox
									checked={form.bidirectional}
									onChange={handleChange}
									name="bidirectional"
									color="primary"
								/>
							}
							label="Allow Two-Way"
						/>
					</div>

					<Typography variant="h6" className="mx-16">
						Sessions
					</Typography>
					<div className="flex flex-wrap flex-row flex-grow justify-start">
						{sessionAssgin.map(session => (
							<FormControlLabel
								key={session.id}
								className="m-4 truncate"
								control={
									<Checkbox
										value={session.id}
										onChange={handleChangedSession}
										name="session"
										color="primary"
									/>
								}
								label={session.title}
							/>
						))}
					</div>
				</form>
			</DialogContent>

			<DialogActions className="p-8 w-full flex justify-end">
				<Button className="mx-8" onClick={closeInterpeterDialog} color="primary">
					Cancel
				</Button>
				<Button
					className="mx-8"
					type="submit"
					variant="contained"
					color="secondary"
					onClick={handleSubmit}
					disabled={!isFormValid}
				>
					OK
				</Button>
			</DialogActions>
		</Dialog>
	);
}

export default React.memo(InterpeterDialog);
