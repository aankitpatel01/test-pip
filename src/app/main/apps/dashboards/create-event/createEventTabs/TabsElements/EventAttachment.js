import { Link } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import clsx from 'clsx';
import React from 'react';
import { useDispatch } from 'react-redux';
import * as Actions from '../../store/actions';

const useStyles = makeStyles({
	root: {
		fontSize: 13,
		backgroundColor: 'rgba(0, 0, 0, 0.08)',
		border: '1px solid rgba(0, 0, 0, 0.16)',
		paddingLeft: 16,
		marginBottom: 8,
		borderRadius: 2,
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	filename: {
		fontWeight: 600
	},
	size: {
		marginLeft: 8,
		fontWeight: 300
	}
});

function EventAttachment(props) {
	const dispatch = useDispatch();
	const classes = useStyles();

	return (
		<div className={clsx(classes.root, props.className)}>
			<div className="flex">
				<Typography variant="caption" className={classes.filename}>
					{props.fileName || ''}
				</Typography>
				{/* <Typography variant="caption" className={classes.size}>
					({props.size})
				</Typography> */}
			</div>
			<div>
				<Link href={`${props?.url}`} download={props?.fileName} target="_blank">
					Download
				</Link>
				<IconButton onClick={() => dispatch(Actions.deleteEventMaterial(props.id, props.eventId))}>
					<Icon className="text-16">delete</Icon>
				</IconButton>
			</div>
		</div>
	);
}

export default EventAttachment;
