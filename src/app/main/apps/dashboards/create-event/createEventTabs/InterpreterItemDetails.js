import React from 'react';
import { Icon, Button, Typography, Divider } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../store/actions';
import InterpretersItems from './InterpretersItems';

const InterpreterItemDetails = ({ room }) => {
	const dispatch = useDispatch();
	const languageTab = useSelector(({ createEvent }) => createEvent.CreateEvent.languageTab);
	const agendaData = useSelector(({ createEvent }) => createEvent.CreateEvent.agendaTab);
	const interpreterTab = useSelector(({ createEvent }) => createEvent.CreateEvent.interpreters);
	const roomobj = room[0];

	const roomLanguage = languageTab.filter(lang => lang.roomId === roomobj.roomId);
	const roomAgenda = agendaData.filter(session => session.roomId === roomobj.roomId);
	const roomInterpreter = interpreterTab.filter(assign => assign.roomId === roomobj.roomId);
	console.log('roomInterpreter', roomInterpreter);

	return (
		<>
			<Typography variant="h5" className="m-16">
				{roomobj.title}
			</Typography>

			<Divider className="mb-16" />
			<div className="flex flex-col">
				{!roomInterpreter ? (
					<Typography variant="subtitle2" className="m-8 flex justify-center">
						interpreters is not available
					</Typography>
				) : (
					roomInterpreter.map(interpreter => (
						<InterpretersItems key={interpreter.id} interpreter={interpreter} session={roomAgenda} />
					))
				)}
			</div>

			<Button
				onClick={() =>
					dispatch(
						Actions.openEventsDialog({ langarr: roomLanguage, session: roomAgenda, roomId: roomobj.roomId })
					)
				}
				className="w-full p-16 my-8"
				startIcon={<Icon>add</Icon>}
				disableElevation
				variant="outlined"
			>
				Add Assignment
			</Button>
		</>
	);
};

export default React.memo(InterpreterItemDetails);
