import React, { useEffect } from 'react';
import {
	TextField,
	IconButton,
	Icon,
	Button,
	DialogTitle,
	DialogContent,
	Typography,
	DialogActions,
	FormControlLabel,
	Checkbox
} from '@material-ui/core';
import { useDispatch } from 'react-redux';
import * as FuseActions from 'app/store/actions';
import { useForm } from '@fuse/hooks';
import * as Actions from '../store/actions';

const LanguageListItem = ({ lang }) => {
	const dispatch = useDispatch();

	const defaultFormState = {
		eventId: '',
		roomId: '',
		id: '',
		type: null,
		public: false,
		record: false,
		lang: '',
		title: '',
		purpose: ''
	};

	const { form, handleChange, setForm } = useForm(defaultFormState);

	useEffect(() => {
		if (lang !== []) {
			setForm({ ...defaultFormState, ...lang });
		}

		// eslint-disable-next-line
	}, [lang, setForm]);

	const handleUpdate = () => {
		console.log('Language Update !');
		const data = {
			id: form.id,
			title: form.title,
			public: form.public,
			record: form.record
		};
		dispatch(Actions.updateStream(data, form.eventId));
	};

	return (
		<div className="flex flex-col sm:flex-row mb-16">
			<div className="w-full sm:w-1/2 flex flex-row">
				<div className="flex w-1/3 sm:w-1/3 flex-col text-center">
					<Typography className="p-4" variant="body1">
						Type
					</Typography>
					<Typography className="p-4" variant="subtitle1">
						{form.type}
					</Typography>
				</div>
				<div className="flex w-2/3 sm:w-2/3 justify-center pr-4">
					<TextField
						className="w-full p-4"
						label="Language"
						name="title"
						value={form.title}
						onChange={handleChange}
						onBlur={handleUpdate}
						variant="outlined"
						type="text"
						required
					/>
				</div>
			</div>

			<div className="w-full sm:w-1/2 flex flex-row justify-evenly">
				<FormControlLabel
					className="p-4"
					control={
						<Checkbox
							color="secondary"
							checked={form.record}
							onBlur={handleUpdate}
							onChange={handleChange}
						/>
					}
					label="Record"
					name="record"
					labelPlacement="end"
				/>
				<FormControlLabel
					className="p-4"
					control={
						<Checkbox
							color="secondary"
							checked={form.public}
							onBlur={handleUpdate}
							onChange={handleChange}
						/>
					}
					label="Public"
					name="public"
					labelPlacement="end"
				/>
				<IconButton
					onClick={() => {
						dispatch(
							FuseActions.openDialog({
								children: (
									<>
										<DialogTitle id="delete-room-item">Delete User ?</DialogTitle>
										<DialogContent dividers className="p-24 flex flex-row">
											<Typography className="flex flex-row text-center" variant="subtitle1">
												Are You want to Delete{' '}
												<p className="px-4 font-bold text-red-700">{form.title}</p> ?
											</Typography>
										</DialogContent>
										<DialogActions>
											<Button
												color="primary"
												onClick={() => {
													dispatch(FuseActions.closeDialog());
												}}
											>
												Cancel
											</Button>
											<Button
												color="primary"
												onClick={() => {
													console.log(form.id);
													dispatch(Actions.deleteLang(form.id, form.eventId));
												}}
												autoFocus
											>
												OK
											</Button>
										</DialogActions>
									</>
								)
							})
						);
					}}
				>
					<Icon className="text-red-700">delete</Icon>
				</IconButton>
			</div>
		</div>
	);
};

export default React.memo(LanguageListItem);
