import React from 'react';
import { Button, makeStyles, CircularProgress } from '@material-ui/core';
import { useSelector } from 'react-redux';
// import * as Actions from '../store/actions';

import ParticipantsTabItem from './ParticipantsTabItem';

const ParticipantsTab = ({ handlePreviousTabChange }) => {
	// const dispatch = useDispatch();
	const roomTabData = useSelector(({ createEvent }) => createEvent.CreateEvent.roomsTab);
	// const eventid = useSelector(({ createEvent }) => createEvent.CreateEvent.eventDetails.eventid);
	return (
		<div className="flex flex-col">
			{roomTabData.map(room => (
				<ParticipantsTabItem key={room.roomid} room={room} />
			))}

			<div className="w-full flex justify-between mt-8">
				<Button variant="contained" color="secondary" onClick={handlePreviousTabChange}>
					Previous Step
				</Button>
			</div>
		</div>
	);
};

export default ParticipantsTab;
