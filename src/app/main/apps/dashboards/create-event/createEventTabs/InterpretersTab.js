import React from 'react';
import { Button, makeStyles, CircularProgress } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { green } from '@material-ui/core/colors';
import History from '@history';
import InterpreterItemDetails from './InterpreterItemDetails';
import * as Actions from '../store/actions';

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		alignItems: 'center'
	},

	buttonProgress: {
		color: green[500],
		position: 'absolute',
		top: '50%',
		left: '50%',
		marginTop: -12,
		marginLeft: -12
	}
}));

const InterpretersTab = ({ handlePreviousTabChange, eventId }) => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const roomTabData = useSelector(({ createEvent }) => createEvent.CreateEvent.roomsTab);

	const [loading, setLoading] = React.useState(false);

	const timer = React.useRef();

	React.useEffect(() => {
		return () => {
			clearTimeout(timer.current);
		};
	}, []);

	const handleButtonClick = () => {
		if (!loading) {
			setLoading(true);
			const data = { eventId, status: 'publish' };
			dispatch(Actions.updateEventDetails(data)).then(() => History.push(`/publishevent/${eventId}`));
			timer.current = setTimeout(() => {
				setLoading(false);
				History.push(`/publishevent/${eventId}`);
			}, 500);
		}
	};

	return (
		<div className="flex flex-col">
			{roomTabData.map(room => (
				<InterpreterItemDetails key={room.id} room={room.rooms} />
			))}

			<div className="w-full flex justify-between mt-8">
				<Button variant="contained" color="secondary" onClick={handlePreviousTabChange}>
					Previous Step
				</Button>
				<div className="relative">
					<Button variant="contained" color="secondary" disabled={loading} onClick={handleButtonClick}>
						Publish Event
					</Button>
					{loading && <CircularProgress size={24} className={classes.buttonProgress} />}
				</div>
			</div>
		</div>
	);
};

export default React.memo(InterpretersTab);
