import React, { useEffect } from 'react';
import {
	TextField,
	IconButton,
	Icon,
	Button,
	DialogTitle,
	DialogContent,
	Typography,
	DialogActions
} from '@material-ui/core';
import { useDispatch } from 'react-redux';
import * as FuseActions from 'app/store/actions';
import { useForm } from '@fuse/hooks';
import * as Actions from '../store/actions';

const RoomTextField = ({ room, eventId }) => {
	const dispatch = useDispatch();

	const defaultFormState = {
		roomId: null,
		title: '',
		lang: 'en'
	};

	const { form, handleChange, setForm } = useForm(defaultFormState);

	useEffect(() => {
		console.log('Room SingleValue', form, room);
		if (room !== null) {
			setForm({ ...form, ...room });
		}
		// eslint-disable-next-line
	}, [room, setForm]);

	return (
		<>
			<TextField
				className="w-full"
				name="title"
				value={form.title}
				variant="outlined"
				type="text"
				onChange={handleChange}
				required
			/>

			<IconButton
				onClick={() => {
					dispatch(
						FuseActions.openDialog({
							children: (
								<>
									<DialogTitle id="delete-room-item">Delete User ?</DialogTitle>
									<DialogContent dividers className="p-24 flex flex-row">
										<Typography className="flex flex-row text-center" variant="subtitle1">
											Are You want to Delete{' '}
											<p className="px-4 font-bold text-red-700">{form.title}</p> ?
										</Typography>
									</DialogContent>
									<DialogActions>
										<Button
											color="primary"
											onClick={() => {
												dispatch(FuseActions.closeDialog());
											}}
										>
											Cancel
										</Button>
										<Button
											color="primary"
											onClick={() => dispatch(Actions.deleteRoom(form.roomId, eventId))}
											autoFocus
										>
											OK
										</Button>
									</DialogActions>
								</>
							)
						})
					);
				}}
			>
				<Icon className="text-red-700">delete</Icon>
			</IconButton>
		</>
	);
};

export default React.memo(RoomTextField);
