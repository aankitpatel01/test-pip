import FusePageCarded from '@fuse/core/FusePageCarded';
import { makeStyles } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import React, { useState, useEffect } from 'react';
import withReducer from 'app/store/withReducer';
import { useSelector, useDispatch } from 'react-redux';
import FuseLoading from '@fuse/core/FuseLoading';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import { Redirect } from 'react-router';
import reducer from './store/reducers';
import EventDetailsTab from './createEventTabs/EventDetailsTab';
import RoomTabs from './createEventTabs/RoomTabs';
import LanguageTabs from './createEventTabs/LanguageTabs';
import SessionTab from './createEventTabs/SessionTab';
import InterpretersTab from './createEventTabs/InterpretersTab';
import LanguageDilog from './createEventTabs/TabsElements/LanguageDilog';
import InterpreterDialog from './createEventTabs/TabsElements/InterpreterDilog';
import * as Actions from './store/actions';

const useStyles = makeStyles(theme => ({
	layoutRoot: {},
	layoutHeader: {
		height: 70,
		minHeight: 70,
		[theme.breakpoints.down('md')]: {
			height: 60,
			minHeight: 60
		}
	}
}));

function CreateEventApp(props) {
	const classes = useStyles();
	const dispatch = useDispatch();
	const allEventData = useSelector(({ createEvent }) => createEvent.CreateEvent.data);

	const [selectedTab, setSelectedTab] = useState(0);
	const [isTabValid, setisTabValid] = useState(false);

	const { match } = props;
	const { eventId } = match.params;

	useEffect(() => {
		dispatch(Actions.getAvEvents(eventId));
		dispatch(Actions.getEvent(eventId));
		dispatch(Actions.getRooms(eventId));
		dispatch(Actions.getStream(eventId));
		dispatch(Actions.getAgenda(eventId));
		dispatch(Actions.getInterpreter(eventId));
		dispatch(Actions.getLanguageList());
		return () => {
			console.log('Clean UP');
			dispatch(Actions.cleanUp());
		};
	}, [dispatch, eventId]);

	const handleTabChange = (event, value) => {
		if (isTabValid) {
			setSelectedTab(value);
		}
	};

	const handleNextTabChange = (event, value) => {
		setSelectedTab(selectedTab + 1);
		setisTabValid(true);
	};

	const handlePreviousTabChange = (event, value) => {
		setSelectedTab(selectedTab - 1);
	};

	if (allEventData === null) {
		return <FuseLoading />;
	}

	if (allEventData.status === 'publish' || allEventData.status === 'archived') {
		return <Redirect to={`/publishevent/${eventId}`} />;
	}

	return (
		<FuseScrollbars scrollToTopOnChildChange>
			<FusePageCarded
				classes={{
					root: classes.layoutRoot,
					header: classes.layoutHeader,
					toolbar: 'p-0',
					contentCard: 'rounded'
				}}
				contentToolbar={
					<Tabs
						value={selectedTab}
						onChange={handleTabChange}
						indicatorColor="primary"
						textColor="primary"
						variant="fullWidth"
						scrollButtons="auto"
						className="w-full h-64"
						disabled
					>
						<Tab className="h-64" label="EVENT DETAILS" />
						<Tab className="h-64" label="ROOMs" />
						<Tab className="h-64" label="LANGUAGES/VIDEO" />
						<Tab className="h-64" label="AGENDA" />
						<Tab className="h-64" label="INTERPRETERS" />
						{/* <Tab className="h-64" label="PARTICIPANTS" /> */}
					</Tabs>
				}
				content={
					<div className="p-24">
						{selectedTab === 0 && (
							<div>
								<h3 className="mb-16">Event Details</h3>
								<EventDetailsTab handleNextTabChange={handleNextTabChange} />
							</div>
						)}
						{selectedTab === 1 && (
							<div>
								<h3 className="mb-16">Rooms Details</h3>
								<RoomTabs
									eventId={eventId}
									handleNextTabChange={handleNextTabChange}
									handlePreviousTabChange={handlePreviousTabChange}
								/>
							</div>
						)}
						{selectedTab === 2 && (
							<div>
								<h3 className="mb-16">Language Details</h3>
								<LanguageTabs
									handlePreviousTabChange={handlePreviousTabChange}
									handleNextTabChange={handleNextTabChange}
								/>
								<LanguageDilog eventId={eventId} />
							</div>
						)}
						{selectedTab === 3 && (
							<div>
								<h3 className="mb-16">Session Details</h3>
								<SessionTab
									eventId={match.params.eventId}
									handlePreviousTabChange={handlePreviousTabChange}
									handleNextTabChange={handleNextTabChange}
								/>
							</div>
						)}
						{selectedTab === 4 && (
							<div>
								<h3 className="mb-16">Interpreters Details</h3>
								<InterpretersTab
									eventId={eventId}
									handlePreviousTabChange={handlePreviousTabChange}
									handleNextTabChange={handleNextTabChange}
								/>
								<InterpreterDialog eventId={eventId} />
							</div>
						)}
						{/* {selectedTab === 5 && (
							<div>
								<h3 className="mb-16">Participants Details</h3>
								<ParticipantsTab handlePreviousTabChange={handlePreviousTabChange} />
								<ParticipantDialog />
							</div>
						)} */}
					</div>
				}
			/>
		</FuseScrollbars>
	);
}

export default withReducer('createEvent', reducer)(React.memo(CreateEventApp));
