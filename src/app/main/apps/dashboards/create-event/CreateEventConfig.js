import React from 'react';
import { authRoles } from 'app/auth';

const CreateEventConfig = {
	settings: {
		layout: {
			config: {
				scroll: 'body',
				footer: {
					display: false,
					style: 'fixed'
				}
			}
		}
	},
	auth: authRoles.staff,
	routes: [
		{
			path: '/create-event/:eventId',
			component: React.lazy(() => import('./CreateEventApp'))
		}
	]
};

export default CreateEventConfig;
