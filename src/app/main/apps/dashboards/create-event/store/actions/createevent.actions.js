import * as FuseActions from 'app/store/actions';
import axios from 'axios';

export const GET_ALL_EVENT_DATA = '[CREATE EVENT] GET ALL EVENT DATA';
export const GET_EVENT_DETAILS = '[CREATE EVENT] GET EVENT_DETAILS';
export const UPDATE_EVENT_DETAILS = '[CREATE EVENT] uPDATE EVENT DETAILS';
export const UPDATE_STREAM = '[CREATE EVENT] uPDATE STREAM';
export const UPDATE_AGENDA = '[CREATE EVENT] uPDATE AGENDA';
export const UPDATE_INTERPRETER = '[CREATE EVENT] UPDATE_INTERPRETER';
export const GET_ROOMS = '[CREATE EVENT] GET ROOM';
export const GET_STREAM = '[CREATE EVENT] GET STREAM';
export const GET_LANGUAGE_LIST = '[CREATE EVENT]GET LANGUAGE LIST';
export const GET_EVENT_STATS = '[CREATE EVENT] GET EVENT STATS';
export const GET_AGENDA = '[CREATE EVENT] GET AGENDA';
export const GET_INTERPRETER_LIST = '[CREATE EVENT] GET ALL INTERPRETER';
export const ADD_ROOMS = '[CREATE EVENT] ADD ROOM';
export const ADD_SESSION = '[CREATE EVENT] ADD SESSION';
export const DELETE_EVENT = '[CREATE EVENT] DELETE EVENT';
export const DELETE_EVENT_MATERIAL = '[CREATE EVENT] DELETE_EVENT_MATERIAL';
export const DELETE_ROOM = '[CREATE EVENT] DELETE ROOM';
export const DELETE_SESSION = '[CREATE EVENT] DELETE ROOM';
export const DELETE_INTERPRETER = '[CREATE EVENT] DELETE INTERPRETER';
export const ADD_LANGUAGE = '[CREATE EVENT] ADD LANGUAGE';
export const ADD_INTERPRETER = '[CREATE EVENT] ADD INTERPRETER';
export const OPEN_EVENT_DIALOG = '[CREATE EVENT] OPEN LANGUAGE/AUDIO DIALOG';
export const OPEN_INTERPETER_DIALOG = '[CREATE EVENT] OPEN INTERPETER DIALOG';
export const OPEN_PARTICIPANTS_DIALOG = '[CREATE EVENT] OPEN PARTICIPANTS DIALOG';
export const OPEN_SHAREABLE_DIALOG = '[CREATE EVENT] OPEN SHAREABLE DIALOG';
export const CLOSE_EVENT_DIALOG = '[CREATE EVENT] CLOSE ALL DIALOG';
export const CLEAN_UP = '[CREATE EVENT] CLEAN UP';

export function cleanUp() {
	return {
		type: CLEAN_UP
	};
}

export function openEventsDialog(data) {
	return {
		type: OPEN_EVENT_DIALOG,
		payload: data
	};
}

export function openShareableDialog() {
	return {
		type: OPEN_SHAREABLE_DIALOG
	};
}

export function closeEventDialog() {
	return {
		type: CLOSE_EVENT_DIALOG
	};
}

export function getAvEvents(eventId) {
	const request = axios.get(`/av-console/getAvEvents/${eventId}`);

	return dispatch =>
		request
			.then(response =>
				dispatch({
					type: GET_ALL_EVENT_DATA,
					payload: response.data.event
				})
			)
			.catch(error => console.log(error));
}

export function getEvent(eventId) {
	const request = axios.get(`/event/getEvent/${eventId}`);

	return dispatch =>
		request
			.then(response =>
				dispatch({
					type: GET_EVENT_DETAILS,
					payload: response.data.event
				})
			)
			.catch(error => console.log(error));
}

export function getRooms(eventId) {
	const request = axios.get(`/event/getRoom/${eventId}`);

	return dispatch =>
		request
			.then(response =>
				dispatch({
					type: GET_ROOMS,
					payload: response.data.event
				})
			)
			.catch(error => console.log(error));
}

export function getStream(eventId) {
	const request = axios.get(`/event/getStream/${eventId}`);

	return dispatch =>
		request
			.then(response =>
				dispatch({
					type: GET_STREAM,
					payload: response.data.Stream
				})
			)
			.catch(error => console.log(error));
}

export function getLanguageList() {
	const request = axios.get('/event/getLanguage');

	return dispatch =>
		request
			.then(response =>
				dispatch({
					type: GET_LANGUAGE_LIST,
					payload: response.data.language
				})
			)
			.catch(error => console.log(error));
}

export function getEventStats(eventId) {
	const request = axios.get(`/stats/getStats/${eventId}`);

	return dispatch =>
		request
			.then(response =>
				dispatch({
					type: GET_EVENT_STATS,
					payload: response.data?.event
				})
			)
			.catch(error => console.log(error));
}

export function getAgenda(eventId) {
	const request = axios.get(`/event/getAgenda/${eventId}`);

	return dispatch =>
		request
			.then(response =>
				dispatch({
					type: GET_AGENDA,
					payload: response.data.agenda
				})
			)
			.catch(error => console.log(error));
}

export function getInterpreter(eventId) {
	const request = axios.get(`/event/getInterpretor/${eventId}`);

	return dispatch =>
		request
			.then(response => {
				dispatch({
					type: GET_INTERPRETER_LIST,
					payload: response.data.data
				});
			})
			.catch(err => console.log(err));
}

export function updateStream(data, eventId) {
	console.log('updateStream Lang Data', data);
	const request = axios.post('event/updateStream', data);

	return dispatch =>
		request
			.then(response =>
				dispatch({
					type: UPDATE_STREAM
				})
			)
			.then(() => dispatch(getStream(eventId)))
			.catch(error => console.log(error));
}

export function updateAgenda(data) {
	console.log('updateAgenda Data', data);
	const request = axios.post('event/updateAgenda', data);

	return dispatch =>
		request
			.then(response =>
				dispatch({
					type: UPDATE_AGENDA
				})
			)
			.catch(error => console.log(error));
}

export function updateInterpreter(data) {
	console.log('updateAgenda Data', data);
	const request = axios.post(`/event/updateInterpretor/${data.id}`, data);

	return dispatch =>
		request
			.then(response =>
				dispatch({
					type: UPDATE_INTERPRETER
				})
			)
			.then(() => dispatch(getInterpreter(data.eventId)))
			.then(() => dispatch(FuseActions.closeDialog()))
			.catch(error => console.log(error));
}

export function updateEventDetails(data) {
	console.log('Action For updateEventDetails ', data);
	const request = axios.post('event/updateEvent', data);

	return dispatch =>
		request
			.then(response =>
				dispatch({
					type: UPDATE_EVENT_DETAILS,
					payload: response.data.event
				})
			)
			.then(() => {
				if (data instanceof FormData === true) {
					const eventId = data.get('eventId');
					getEvent(eventId);
				} else {
					getEvent(data.eventId);
				}
			})
			.then(() => dispatch(closeEventDialog()))
			.catch(error => {
				console.log(error);
			});
}

export function addNewRoom(data) {
	const request = axios.post('event/saveRoom', data);

	return dispatch =>
		request.then(response => {
			Promise.all([
				dispatch({
					type: ADD_ROOMS
				})
			])
				.then(() => dispatch(FuseActions.closeDialog()))
				.then(() => dispatch(getRooms(data.eventId)))
				.then(() => dispatch(FuseActions.showMessage({ message: response.data.message, variant: 'success' })));
		});
}
export function addNewSession(data) {
	const request = axios.post('event/saveAgenda', data);

	return dispatch =>
		request.then(response => {
			Promise.all([
				dispatch({
					type: ADD_SESSION
				})
			])
				.then(() => dispatch(FuseActions.closeDialog()))
				.then(() => dispatch(getAgenda(data.eventId)))
				.then(() => dispatch(FuseActions.showMessage({ message: response.data.message, variant: 'success' })));
		});
}

export function deleteAgenda(id, eventId) {
	console.log('deleteAgenda Click', id);
	const request = axios.post(`event/deleteAgenda/${id}`);

	return dispatch =>
		request.then(response => {
			Promise.all([
				dispatch({
					type: DELETE_SESSION
				})
			])
				.then(() => dispatch(FuseActions.closeDialog()))
				.then(() => dispatch(getAgenda(eventId)))
				.then(() => dispatch(FuseActions.showMessage({ message: response.data.message })));
		});
}

export function addNewLanguage(data) {
	console.log('Data Click', data);
	const request = axios.post('event/saveStream', data);

	return dispatch =>
		request.then(response => {
			Promise.all([
				dispatch({
					type: ADD_LANGUAGE
				})
			])
				.then(() => dispatch(closeEventDialog()))
				.then(() => dispatch(getStream(data.eventId)))
				.then(() =>
					dispatch(
						FuseActions.showMessage({
							message: `${data.title} Language Added Successfully`,
							variant: 'success'
						})
					)
				);
		});
}

export function addInterpreter(data) {
	console.log('addInterpreter Click', data);
	const request = axios.post('event/saveInterpretor', data);

	return dispatch =>
		request
			.then(response => {
				Promise.all([
					dispatch({
						type: ADD_INTERPRETER,
						payload: response.data
					})
				])
					.then(() => dispatch(closeEventDialog()))
					.then(() => dispatch(getInterpreter(data.eventId)))
					.then(() =>
						dispatch(FuseActions.showMessage({ message: response.data.message, variant: 'success' }))
					);
			})
			.catch(error =>
				dispatch(FuseActions.showMessage({ message: "Interpreter Doesn't Exist", variant: 'error' }))
			);
}

export function deleteLang(id, eventId) {
	console.log('Data Click', id, eventId);
	const request = axios.post(`event/deleteStream/${id}`);

	return dispatch =>
		request
			.then(response => {
				Promise.all([
					dispatch({
						type: DELETE_ROOM
					})
				])
					.then(() => dispatch(FuseActions.closeDialog()))
					.then(() => dispatch(getStream(eventId)))
					.then(() => dispatch(FuseActions.showMessage({ message: response.data.message })));
			})
			.catch(error => console.log('Yes its Error', error));
}

export function deleteInterpreter(id, eventId) {
	console.log('Data Click', id, eventId);
	const request = axios.post(`event/deleteInterpretor/${id}`);

	return dispatch =>
		request
			.then(response => {
				Promise.all([
					dispatch({
						type: DELETE_INTERPRETER
					})
				])
					.then(() => dispatch(FuseActions.closeDialog()))
					.then(() => dispatch(getInterpreter(eventId)))
					.then(() =>
						dispatch(FuseActions.showMessage({ message: response.data.message, variant: 'success' }))
					);
			})
			.catch(error => console.log('Yes its Error', error));
}

export function deleteRoom(id, eventId) {
	console.log('Data Click', id);
	const request = axios.post(`event/deleteRoom/${id}`);

	return dispatch =>
		request.then(response => {
			Promise.all([
				dispatch({
					type: DELETE_ROOM
				})
			])
				.then(() => dispatch(FuseActions.closeDialog()))
				.then(() => dispatch(getRooms(eventId)))
				.then(() => dispatch(FuseActions.showMessage({ message: response.data.message })));
		});
}

export function deleteEvent(eventId) {
	console.log('Delete Event Click', eventId);
	const request = axios.post(`event/deleteEvent/${eventId}`);

	return dispatch =>
		request.then(response => {
			dispatch({
				type: DELETE_EVENT
			});
		});
}

export function deleteEventMaterial(id, eventId) {
	console.log('Delete Event Click', id);
	const request = axios.post(`event/deleteEventMaterialImage/${id}`);

	return dispatch =>
		request
			.then(() => {
				dispatch({
					type: DELETE_EVENT_MATERIAL
				});
			})
			.then(() => dispatch(getEvent(eventId)));
}
