import * as Actions from '../actions';

const initialState = {
	data: null,
	eventDetails: null,
	roomsTab: [],
	languageTab: [],
	agendaTab: [],
	interpreters: [],
	eventDialog: {
		type: 'new',
		props: {
			open: false
		},
		data: null
	},
	languageList: [],
	stats: null
};

const CreateEvent = (state = initialState, action) => {
	switch (action.type) {
		case Actions.OPEN_EVENT_DIALOG: {
			return {
				...state,
				eventDialog: {
					type: 'new',
					props: {
						open: true
					},
					data: action.payload
				}
			};
		}
		case Actions.OPEN_SHAREABLE_DIALOG: {
			return {
				...state,
				eventDialog: {
					type: 'share',
					props: {
						open: true
					}
				}
			};
		}
		case Actions.CLOSE_EVENT_DIALOG: {
			return {
				...state,
				eventDialog: {
					type: 'new',
					props: {
						open: false
					},
					data: null
				}
			};
		}
		case Actions.CLEAN_UP: {
			return {
				...state,
				data: null,
				eventDetails: null
			};
		}
		case Actions.GET_ALL_EVENT_DATA: {
			return { ...state, data: { ...action.payload } };
		}
		case Actions.GET_EVENT_DETAILS: {
			return { ...state, eventDetails: { ...state.eventDetails, ...action.payload } };
		}
		case Actions.UPDATE_EVENT_DETAILS: {
			return { ...state, eventDetails: { ...state.eventDetails, ...action.payload } };
		}
		case Actions.UPDATE_STREAM: {
			return state;
		}
		case Actions.UPDATE_AGENDA: {
			return state;
		}
		case Actions.UPDATE_INTERPRETER: {
			return state;
		}
		case Actions.GET_ROOMS: {
			return { ...state, roomsTab: [...action.payload] };
		}
		case Actions.GET_STREAM: {
			return { ...state, languageTab: [...action.payload] };
		}
		case Actions.GET_AGENDA: {
			return { ...state, agendaTab: [...action.payload] };
		}
		case Actions.GET_INTERPRETER_LIST: {
			return { ...state, interpreters: [...action.payload] };
		}
		case Actions.GET_LANGUAGE_LIST: {
			return { ...state, languageList: [...action.payload] };
		}
		case Actions.GET_EVENT_STATS: {
			return { ...state, stats: [...action.payload] };
		}
		case Actions.ADD_ROOMS: {
			return state;
		}
		case Actions.ADD_LANGUAGE: {
			return state;
		}
		case Actions.ADD_INTERPRETER: {
			return state;
		}
		case Actions.DELETE_ROOM: {
			return state;
		}
		case Actions.DELETE_INTERPRETER: {
			return state;
		}
		case Actions.DELETE_EVENT: {
			return state;
		}
		case Actions.DELETE_EVENT_MATERIAL: {
			return state;
		}
		default:
			return state;
	}
};

export default CreateEvent;
