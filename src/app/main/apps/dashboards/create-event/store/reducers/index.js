import { combineReducers } from 'redux';
import CreateEvent from './createEvent.reducer';

const reducer = combineReducers({
	CreateEvent
});

export default reducer;
