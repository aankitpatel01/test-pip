import React, { useEffect } from 'react';
import MaterialTable from 'material-table';
import { Button } from '@material-ui/core';

const tableData = {
	columns: [
		{ title: 'Start Time', field: 'startTime' },
		{ title: 'End Time', field: 'endTime' },

		{
			title: 'End Time',
			field: 'endTime',
			type: 'numeric',
			lookup: { 34: 'İstanbul', 63: 'Şanlıurfa', 38: 'Delhi' }
		},
		{ title: 'Session', field: 'session', type: 'numeric' },
		{ title: 'Stream', field: 'title', type: 'numeric' },
		{ title: 'User', field: 'name', type: 'numeric' },
		{ title: 'Application', field: 'phone', type: 'numeric' },
		{ title: 'Protocol', field: 'age', type: 'numeric' },
		{ title: 'Network', field: 'country', type: 'numeric' },
		{ title: 'Direction', field: 'email', type: 'numeric' },
		{ title: 'Duration', field: 'phone', type: 'numeric' },
		{ title: 'Loss', field: 'birthYear', type: 'numeric' },
		{ title: 'Jitter', field: 'birthYear', type: 'numeric' },
		{ title: 'Bandwidth', field: 'birthYear', type: 'numeric' }
	],
	data: []
};

const DataTableForStatnRec = ({ eventStats }) => {
	console.log('eventStats', eventStats);
	const [stats, setStats] = React.useState(tableData);

	useEffect(() => {
		setStats({ ...tableData, data: eventStats });
		// eslint-disable-next-line
	}, [setStats]);

	return (
		<MaterialTable
			// icons={{ Filter: () => <Icon>filter_alt</Icon> }}
			stickyHeader
			title={
				<div>
					<Button>STATS</Button>
					<Button>USERS</Button>
				</div>
			}
			columns={stats.columns}
			data={stats.data}
			options={{
				exportButton: true,
				filtering: true
			}}
		/>
	);
};

export default React.memo(DataTableForStatnRec);
