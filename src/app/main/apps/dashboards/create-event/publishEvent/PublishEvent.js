import FusePageSimple from '@fuse/core/FusePageSimple';
import { makeStyles } from '@material-ui/core/styles';
import React, { useEffect } from 'react';
import {
	Typography,
	Button,
	Paper,
	List,
	ListItemAvatar,
	Avatar,
	Icon,
	ListItemSecondaryAction,
	ListItem,
	ListItemText,
	Link,
	Hidden
} from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import { useSelector, useDispatch } from 'react-redux';
import withReducer from 'app/store/withReducer';
import FuseLoading from '@fuse/core/FuseLoading';
import History from '@history';
import reducer from '../store/reducers';
import PublishHeader from './PublishHeader';
import * as Actions from '../store/actions';
import PublishLang from './PublishLang';
import PublishInterpreter from './PublishInterpreter';
import PublishDialog from '../createEventTabs/TabsElements/PublishDialog';

const useStyles = makeStyles(theme => ({
	layoutRoot: {},
	disableIframe: {
		pointerEvents: 'none'
	}
}));

const PublishEvent = props => {
	const classes = useStyles(props);
	const { eventId } = props.match.params;
	const dispatch = useDispatch();
	const eventAllData = useSelector(({ createEvent }) => createEvent.CreateEvent.data);

	console.log('eventAllData', eventAllData);

	useEffect(() => {
		dispatch(Actions.getAvEvents(eventId));
		dispatch(Actions.getEvent(eventId));
		return () => {
			console.log('Clean UP');
			dispatch(Actions.cleanUp());
		};
	}, [dispatch, eventId]);

	const editEventHandle = () => {
		const data = { eventId, status: 'pending' };
		dispatch(Actions.updateEventDetails(data)).then(() => History.push(`/create-event/${eventId}`));
	};

	if (!eventAllData) {
		return <FuseLoading />;
	}

	const confirmdata = eventAllData.interpretor.filter(interpreter => interpreter.status === 'confirmed');
	const confirmedInterpreter = confirmdata.length;

	return (
		<FusePageSimple
			classes={{
				root: classes.layoutRoot
			}}
			header={
				<div className="flex flex-1 items-center justify-between p-12">
					<PublishHeader eventData={eventAllData} />
				</div>
			}
			content={
				<div className="flex p-12">
					<div className="w-full sm:w-2/3 flex flex-col p-12">
						<Paper className="w-full rounded-8  border-1 p-12 mb-24">
							<div className="flex items-center justify-between px-4 pt-4">
								<Typography variant="h6" className="p-12">
									For the best event quality, please follow these steps:
								</Typography>
							</div>
							<List>
								<ListItem>
									<ListItemAvatar>
										<Avatar className="bg-green">
											<Icon>check</Icon>
										</Avatar>
									</ListItemAvatar>
									<ListItemText
										primary={`Confirmed Interpeters (${confirmedInterpreter}/${eventAllData.interpretor.length})`}
									/>
									<ListItemSecondaryAction>
										<Button variant="contained" size="large" color="primary">
											Resend Invitations
										</Button>
									</ListItemSecondaryAction>
								</ListItem>
								<ListItem>
									<ListItemAvatar>
										<Avatar className="bg-yellow-700">
											<Icon>warning</Icon>
										</Avatar>
									</ListItemAvatar>
									<ListItemText primary="Confirmed Interpeters " />
									<ListItemSecondaryAction>
										<Button variant="contained" size="large" color="primary">
											Translate Text
										</Button>
									</ListItemSecondaryAction>
								</ListItem>
							</List>
							<div className="flex justify-center items-center px-16 h-52 border-t-1">
								<Typography className="text-15" color="textSecondary">
									Need Help Please
									<Link to="/" className="mx-4 font-bold">
										check our manual
									</Link>
									or contact our team.
								</Typography>
							</div>
						</Paper>

						{eventAllData.stream.length > 0 && (
							<Paper className="w-full rounded-8 border-1 p-12 mb-24">
								{eventAllData.stream.map(strm => (
									<PublishLang key={strm.id} stream={strm} />
								))}
							</Paper>
						)}
						{eventAllData.stream.length > 0 && (
							<Paper className="w-full rounded-8 border-1 p-12 mb-24">
								{eventAllData.interpretor.map(inter => (
									<PublishInterpreter key={inter.id} interpreter={inter} />
								))}
							</Paper>
						)}

						<div className="flex flex-1 justify-between">
							<Button variant="contained" size="large" color="primary" onClick={editEventHandle}>
								EDIT EVENT
							</Button>
							<Button
								variant="contained"
								size="large"
								color="primary"
								component={RouterLink}
								to={`/event/stats/${eventId}`}
							>
								STATISTICS
							</Button>
							<Button variant="contained" size="large" color="primary">
								RECORDING
							</Button>
							<Button
								variant="contained"
								size="large"
								color="primary"
								component={RouterLink}
								target="_blank"
								to={`/avconsole/event/${eventId}`}
							>
								AV CONSOLE
							</Button>
						</div>
					</div>
					{eventAllData.code !== '' && (
						<Hidden only="xs">
							<Paper className={clsx(classes.disableIframe, 'w-1/3 rounded-8 border-1')}>
								<iframe
									title="WebAppUserIframe"
									src={`/client/${eventAllData.code}`}
									className="w-full h-full"
								/>
							</Paper>
						</Hidden>
					)}

					<PublishDialog />
				</div>
			}
		/>
	);
};

export default withReducer('createEvent', reducer)(React.memo(PublishEvent));
