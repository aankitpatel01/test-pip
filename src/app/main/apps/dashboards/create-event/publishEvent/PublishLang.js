import React, { useEffect } from 'react';
import { FormControlLabel, Typography, Avatar, Icon, Checkbox, Button } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { useForm } from '@fuse/hooks';
import * as Actions from '../store/actions';

const PublishLang = ({ stream }) => {
	const dispatch = useDispatch();

	const defaultFormState = {
		type: [],
		urls: [],
		lang: '',
		id: null,
		public: false,
		record: false,
		roomId: null,
		eventId: null,
		title: ''
	};

	const { form, handleChange, setForm } = useForm(defaultFormState);

	const handleUpdate = () => {
		console.log('Language Update !');
		const data = {
			id: form.id,
			title: form.title,
			public: form.public,
			record: form.record
		};

		dispatch(Actions.updateStream(data, form.eventId));
	};
	useEffect(() => {
		setForm({ ...defaultFormState, ...stream });
		// eslint-disable-next-line
	}, [setForm]);

	return (
		<div className="w-full flex flex-1 flex-row justify-between items-center p-12">
			<div className="flex flex-row justify-between items-center">
				<Avatar>
					<Icon>translate</Icon>
				</Avatar>

				<Typography className="mx-16" variant="subtitle2">
					{form.title}
				</Typography>
			</div>

			<div>
				<FormControlLabel
					control={
						<Checkbox checked={form.record} onChange={handleChange} onBlur={handleUpdate} name="record" />
					}
					label="Record"
				/>
				<FormControlLabel
					control={
						<Checkbox checked={form.public} onChange={handleChange} onBlur={handleUpdate} name="public" />
					}
					label="Public"
				/>
			</div>
			<Button variant="contained" size="large" color="primary">
				SETUP CALL-IN
			</Button>
		</div>
	);
};

export default React.memo(PublishLang);
