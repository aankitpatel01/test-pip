import React from 'react';
import { Typography, Avatar, Icon } from '@material-ui/core';
import clsx from 'clsx';

const PublishInterpreter = props => {
	const { user, srcTitle, dstTitle, status, bidirectional } = props.interpreter;
	return (
		<div className="w-full flex flex-1 flex-row justify-between items-center p-12">
			<div className="w-1/3 flex flex-row justify-between items-center">
				{status === 'pending' ? (
					<Avatar className="bg-yellow-700">
						<Icon>info</Icon>
					</Avatar>
				) : (
					<Avatar className="bg-green">
						<Icon>check</Icon>
					</Avatar>
				)}

				<Typography className="mx-16" variant="subtitle2">
					{user}
				</Typography>
			</div>

			<div className="w-2/3 flex flex-row justify-center items-center">
				<Typography variant="subtitle2">{srcTitle}</Typography>
				<Icon className="mx-16">{bidirectional ? 'sync_all' : 'arrow_forward'} </Icon>
				<Typography variant="subtitle2">{dstTitle}</Typography>
			</div>
			<div
				className={clsx(
					status === 'pending' ? 'text-blue' : 'text-green',
					'w-1/3 flex items-center justify-end'
				)}
			>
				{status}
			</div>
		</div>
	);
};

export default React.memo(PublishInterpreter);
