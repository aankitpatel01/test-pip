import { makeStyles } from '@material-ui/core/styles';
import withReducer from 'app/store/withReducer';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import FuseLoading from '@fuse/core/FuseLoading';
import reducer from '../store/reducers';
import * as Actions from '../store/actions';
import DataTableForStatnRec from './DataTableForStatnRec';

const useStyles = makeStyles({
	root: {
		padding: 24
	}
});

const EventStats = props => {
	const dispatch = useDispatch();
	const classes = useStyles();
	const { eventId } = props.match.params;

	const eventStats = useSelector(({ createEvent }) => createEvent.CreateEvent.stats);

	useEffect(() => {
		dispatch(Actions.getEventStats(eventId));
	}, [dispatch, eventId]);

	if (!eventStats) {
		return <FuseLoading />;
	}

	return (
		<div className={classes.root}>
			<DataTableForStatnRec eventStats={eventStats} />
		</div>
	);
};

export default withReducer('createEvent', reducer)(React.memo(EventStats));
