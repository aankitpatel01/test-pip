import React from 'react';
import { authRoles } from 'app/auth';

const PublishEventConfig = {
	settings: {
		layout: {
			style: 'layout2',
			config: {
				footer: {
					display: true,
					style: 'static'
				}
			},
			mode: 'fullwidth'
		}
	},
	auth: authRoles.staff,
	routes: [
		{
			path: '/publishevent/:eventId',
			component: React.lazy(() => import('./PublishEvent'))
		},
		{
			path: '/event/stats/:eventId',
			component: React.lazy(() => import('./EventStats'))
		}
	]
};

export default PublishEventConfig;
