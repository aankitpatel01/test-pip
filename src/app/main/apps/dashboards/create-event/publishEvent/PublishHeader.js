import React, { useEffect } from 'react';
import { Typography, Button } from '@material-ui/core';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../store/actions';

const PublishHeader = () => {
	const dispatch = useDispatch();

	const eventDetails = useSelector(({ createEvent }) => createEvent.CreateEvent.eventDetails);

	useEffect(() => {
		console.log('Publsih Header', eventDetails);
	}, [eventDetails]);

	if (!eventDetails) {
		return null;
	}

	const { date, location, eventId, wifiName, code, externalIp } = eventDetails;

	return (
		<>
			<div>
				<Typography className="p-4" variant="h6">
					{location}
				</Typography>
				<Typography className="text-grey-400 p-4" variant="subtitle1">
					Date : {moment(date).format('ddd, D MMMM, h:mm a')}
				</Typography>
			</div>
			<div>
				<Button onClick={() => dispatch(Actions.openEventsDialog({ eventId, wifiName, code, externalIp }))}>
					Event access setting
				</Button>
				{eventDetails && (
					<div className="flex flex-row">
						<Typography className="text-grey-400 p-4">WiFi: {wifiName}</Typography>
						<Typography className="text-grey-400 p-4">IP: {externalIp}</Typography>
						<Typography className="text-grey-400 p-4">Code: {code}</Typography>
					</div>
				)}
			</div>
		</>
	);
};

export default PublishHeader;
