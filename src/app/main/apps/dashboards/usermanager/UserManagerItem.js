import React from 'react';
import {
	ListItem,
	Typography,
	IconButton,
	Icon,
	ListItemText,
	ListItemAvatar,
	Avatar,
	Hidden,
	DialogActions,
	Button,
	DialogContent,
	DialogTitle
} from '@material-ui/core';
import { useDispatch } from 'react-redux';
import * as FuseActions from 'app/store/actions';
import FuseAnimate from '@fuse/core/FuseAnimate';
import moment from 'moment';
import * as Actions from './store/actions';

const UserManagerItem = props => {
	const dispatch = useDispatch();

	const { _id, username, email, lastLogin, roles, createdAt } = props.user;

	return (
		<FuseAnimate animation="transition.slideUpIn" delay={200}>
			<ListItem>
				<Hidden only={['sm', 'xs']}>
					<ListItemAvatar className="pl-8">
						<Avatar>
							<Icon className="text-black">account_circle</Icon>
						</Avatar>
					</ListItemAvatar>
				</Hidden>
				<div className="px-8 w-2/6 sm:w-full sm:p-0 truncate">
					<ListItemText
						primary={
							<Typography variant="h6" className="truncate">
								{username}
							</Typography>
						}
						secondary={<Typography className="truncate">{email}</Typography>}
					/>
				</div>
				<div className="px-8 w-1/6 sm:w-1/2 sm:p-0 truncate">
					<ListItemText
						primary={
							<Typography variant="subtitle1" className="truncate">
								Registered
							</Typography>
						}
						secondary={
							<Typography className="text-red-700">
								{moment(createdAt).format('ddd, D MMMM, h:mm a')}
							</Typography>
						}
					/>
				</div>
				<div className="px-8 w-1/6 sm:w-1/2 sm:p-0 truncate">
					<ListItemText
						primary={
							<Typography variant="subtitle1" className="truncate">
								Last login
							</Typography>
						}
						secondary={<Typography className="text-red-700 truncate">{lastLogin}</Typography>}
					/>
				</div>
				<div className="px-8 w-1/6 sm:w-1/2 sm:p-0 truncate">
					<ListItemText
						primary={<Typography variant="subtitle1">Role</Typography>}
						secondary={<Typography className="text-red-700">{roles}</Typography>}
					/>
				</div>

				<div className="px-8 w-1/6 sm:w-1/2 text-center">
					<IconButton
						onClick={() => {
							dispatch(Actions.openEditUserDialog({ _id, roles }));
						}}
					>
						<Icon>edit</Icon>
					</IconButton>

					<IconButton
						onClick={() => {
							dispatch(
								FuseActions.openDialog({
									children: (
										<>
											<DialogTitle id="delete-user-item">Delete User ?</DialogTitle>
											<DialogContent dividers className="p-24 flex flex-row">
												<Typography className="flex flex-row text-center" variant="subtitle1">
													Are You want to Delete{' '}
													<p className="px-4 font-bold text-red-700">{email}</p> ?
												</Typography>
											</DialogContent>
											<DialogActions>
												<Button
													color="primary"
													onClick={() => {
														dispatch(FuseActions.closeDialog());
													}}
												>
													Cancel
												</Button>
												<Button
													color="primary"
													onClick={() => {
														if (props.loginRoles === 'Admin') {
															dispatch(Actions.removeUser(_id));
														} else {
															dispatch(Actions.removeCompanyUser(_id, props.companyId));
														}
													}}
													autoFocus
												>
													OK
												</Button>
											</DialogActions>
										</>
									)
								})
							);
						}}
					>
						<Icon className="text-red-700">delete</Icon>
					</IconButton>
				</div>
			</ListItem>
		</FuseAnimate>
	);
};

export default UserManagerItem;
