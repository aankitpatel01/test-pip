// import axios from 'axios';

import axios from 'axios';
import * as FuseActions from 'app/store/actions';

export const GET_USERDATA = '[USERS MANAGER DATA] GET ALL USERS';
export const GET_COMPANY_USERS = '[USERS MANAGER DATA] GET COMPANY USERS';
export const UPDATE_USER = '[USER APP] UPDATE USER';
export const ADD_USER = '[USER APP] ADD USER';
export const REMOVE_USER = '[USER APP] REMOVE USER';
export const OPEN_NEW_USER_DIALOG = '[USER APP] OPEN NEW USER DIALOG';
export const CLOSE_NEW_USER_DIALOG = '[USER APP] CLOSE NEW USER DIALOG';
export const OPEN_EDIT_USER_DIALOG = '[USER APP] OPEN EDIT USER DIALOG';
export const CLOSE_EDIT_USER_DIALOG = '[USER APP] CLOSE EDIT USER DIALOG';

export function getAllUsers() {
	const request = axios.get('/list/allusers');

	return dispatch =>
		request
			.then(response =>
				dispatch({
					type: GET_USERDATA,

					payload: response.data.users
				})
			)
			.catch(error => console.log(error));
}

export function getCompaniesUser(companyId) {
	const request = axios.get(`auth/getCompaniesUser/${companyId}`);

	return dispatch =>
		request
			.then(response =>
				dispatch({
					type: GET_COMPANY_USERS,

					payload: response.data.users
				})
			)
			.catch(error => console.log(error));
}

export function updateUsers() {
	return dispatch => {
		const request = axios.get('/list/allusers');

		return request.then(response =>
			dispatch({
				type: UPDATE_USER,
				payload: response.data.users
			})
		);
	};
}

export function openNewUserDialog() {
	return {
		type: OPEN_NEW_USER_DIALOG
	};
}

export function closeNewUserDialog() {
	return {
		type: CLOSE_NEW_USER_DIALOG
	};
}

export function openEditUserDialog(data) {
	return {
		type: OPEN_EDIT_USER_DIALOG,
		data
	};
}

export function closeEditUserDialog() {
	return {
		type: CLOSE_EDIT_USER_DIALOG
	};
}

export function addUser(user) {
	console.log('addUSer', user);

	const request = axios.post('/auth/signup', user);

	return dispatch =>
		request
			.then(response =>
				Promise.all([
					dispatch({
						type: ADD_USER
					})
				])
					.then(() => dispatch(updateUsers()))
					.then(() => dispatch(closeNewUserDialog()))
					.then(() => dispatch(FuseActions.showMessage(response.data)))
			)
			.catch(error => {
				console.log(error.response.data);
				dispatch(FuseActions.showMessage(error.response.data));
			});
}

export function addUserforCompany(user) {
	console.log('addUserforCompany', user);

	const request = axios.post('/auth/signup', user);

	return dispatch =>
		request
			.then(response =>
				Promise.all([
					dispatch({
						type: ADD_USER
					})
				])
					.then(() => dispatch(getCompaniesUser(user.companyId)))
					.then(() => dispatch(closeNewUserDialog()))
					.then(() => dispatch(FuseActions.showMessage(response.data)))
			)
			.catch(error => {
				console.log(error.response.data);
				dispatch(FuseActions.showMessage(error.response.data));
			});
}

export function removeUser(id) {
	console.log(id);
	const userid = { id };
	const request = axios.post('/user/delete', userid);

	return dispatch =>
		request.then(response => {
			console.log(response.data);
			Promise.all([
				dispatch({
					type: REMOVE_USER
				})
			])
				.then(() => dispatch(updateUsers()))
				.then(() => dispatch(FuseActions.showMessage(response.data)))
				.then(() => dispatch(FuseActions.closeDialog()));
		});
}
export function removeCompanyUser(id, companyId) {
	console.log(id);
	const userid = { id };
	const request = axios.post('/user/delete', userid);

	return dispatch =>
		request.then(response => {
			console.log(response.data);
			Promise.all([
				dispatch({
					type: REMOVE_USER
				})
			])
				.then(() => dispatch(getCompaniesUser(companyId)))
				.then(() => dispatch(FuseActions.showMessage(response.data)))
				.then(() => dispatch(FuseActions.closeDialog()));
		});
}

export function updateCompaniesRole(user, companyId) {
	const request = axios.post('/user/updateUserRole', user);
	return dispatch =>
		request
			.then(response => {
				Promise.all([
					dispatch({
						type: UPDATE_USER
					})
				])
					.then(() => dispatch(getCompaniesUser(companyId)))
					.then(() => dispatch(closeNewUserDialog()))
					.then(() =>
						dispatch(FuseActions.showMessage({ message: response.data.message, variant: 'success' }))
					);
			})
			.catch(error => {
				console.log(error.response.data);
				dispatch(FuseActions.showMessage({ message: error.response.data, variant: 'error' }));
			});
}

export function updateRole(user) {
	const request = axios.post('/user/updateUserRole', user);
	return dispatch =>
		request
			.then(response => {
				Promise.all([
					dispatch({
						type: UPDATE_USER
					})
				])
					.then(() => dispatch(updateUsers()))
					.then(() => dispatch(closeNewUserDialog()))
					.then(() =>
						dispatch(FuseActions.showMessage({ message: response.data.message, variant: 'success' }))
					);
			})
			.catch(error => {
				console.log(error.response.data);
				dispatch(FuseActions.showMessage({ message: error.response.data, variant: 'error' }));
			});
}
