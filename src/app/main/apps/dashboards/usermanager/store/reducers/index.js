import { combineReducers } from 'redux';
import userManagerData from './usersmanager.reducer';

const reducer = combineReducers({
	userManagerData
});

export default reducer;
