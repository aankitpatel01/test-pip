// import DemoContent from '@fuse/core/DemoContent';
import FusePageCarded from '@fuse/core/FusePageCarded';
import React from 'react';
import UserManager from 'app/main/apps/dashboards/usermanager/UserManager';

function usersLayout() {
	return (
		<FusePageCarded
			header={null}
			contentToolbar={null}
			content={
				<>
					<div className="w-full p-0 m-0">
						<UserManager />
					</div>
				</>
			}
		/>
	);
}

export default usersLayout;
