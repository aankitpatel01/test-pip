import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { List, Tooltip, Fab, Typography, Icon } from '@material-ui/core';

import FuseAnimate from '@fuse/core/FuseAnimate';
import withReducer from 'app/store/withReducer';
import FuseLoading from '@fuse/core/FuseLoading';
import FuseAnimateGroup from '@fuse/core/FuseAnimateGroup';
import * as Actions from './store/actions';
import reducer from './store/reducers';
import UserManagerItem from './UserManagerItem';
import UserDialog from './UserDialog';

const UserManager = () => {
	// const { allUsers, dialogData, userRole } = UserData;

	const dispatch = useDispatch();
	const loginRoles = useSelector(({ auth }) => auth.user.role);
	const userCompanyID = useSelector(({ auth }) => auth.user.data.companyId);
	const users = useSelector(({ userManager }) => userManager.userManagerData.data);
	const userDialog = useSelector(({ userManager }) => userManager.userManagerData.userDialog);

	useEffect(() => {
		if (loginRoles === 'Admin') {
			dispatch(Actions.getAllUsers());
		} else {
			dispatch(Actions.getCompaniesUser(userCompanyID));
		}
	}, [dispatch, loginRoles, userCompanyID]);

	if (!users) {
		return <FuseLoading />;
	}

	return (
		<>
			<div className="w-full p-16">
				<div className="flex justify-end">
					<Tooltip title="Add New User">
						<Fab
							color="primary"
							className="bg-red-700"
							aria-label="add"
							onClick={() => {
								dispatch(Actions.openNewUserDialog());
							}}
						>
							<Icon>add</Icon>
						</Fab>
					</Tooltip>
				</div>
			</div>

			{users.length === 0 && (
				<FuseAnimate delay={150}>
					<div className="flex flex-1 items-center justify-center h-full">
						<Typography color="textSecondary" variant="h5">
							There are no user!
						</Typography>
					</div>
				</FuseAnimate>
			)}

			<List className="container flex-1 flex-wrap justify-between sm:p-0">
				<FuseAnimateGroup
					enter={{
						animation: 'transition.slideUpBigIn'
					}}
					leave={{
						animation: 'transition.slideUpBigOut'
					}}
				>
					{console.log(users)}
					{console.log(userDialog)}

					{users.map((local, index) => (
						<UserManagerItem key={index} user={local} companyId={userCompanyID} loginRoles={loginRoles} />
					))}
				</FuseAnimateGroup>
			</List>

			<UserDialog />
		</>
	);
};

export default withReducer('userManager', reducer)(UserManager);
