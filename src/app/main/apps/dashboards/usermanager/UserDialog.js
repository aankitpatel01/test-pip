import { useForm } from '@fuse/hooks';
// import FuseUtils from '@fuse/utils';
// import _ from '@lodash';
// import moment from 'moment/moment';
import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SelectFormsy, TextFieldFormsy } from '@fuse/core/formsy';
import Formsy from 'formsy-react';
import { DialogTitle, DialogContent, Button, Dialog, MenuItem, DialogActions } from '@material-ui/core';
import * as Actions from './store/actions';

const defaultFormState = {
	id: '',
	email: '',
	roles: '',
	createdAt: new Date()
};

function UserDialog(props) {
	const dispatch = useDispatch();
	const userDialog = useSelector(({ userManager }) => userManager.userManagerData.userDialog);

	const rolemain = useSelector(({ auth }) => auth.user.role);
	const usersRole = useSelector(({ auth }) => auth.user.data.accessroles);
	const usersId = useSelector(({ auth }) => auth.user.data.id);
	const companyId = useSelector(({ auth }) => auth.user.data.companyId);

	const [roles, setRoles] = useState('');
	const [useremail, setUseremail] = useState('');
	const [isFormValid, setIsFormValid] = useState(false);
	const { form, setForm, setInForm } = useForm({ ...defaultFormState });

	const initDialog = useCallback(() => {
		/**
		 * Dialog type: 'edit'
		 */
		if (userDialog.type === 'edit' && userDialog.data) {
			setForm({ ...defaultFormState, ...userDialog.data });
		}

		/**
		 * Dialog type: 'new'
		 */
		if (userDialog.type === 'new') {
			setForm({
				...defaultFormState,
				...userDialog.data
			});
		}
	}, [userDialog.data, userDialog.type, setForm]);

	useEffect(() => {
		/**
		 * After Dialog Open
		 */
		if (userDialog.props.open) {
			initDialog();
		}
	}, [userDialog.props.open, initDialog]);

	function closeUserDialog() {
		return userDialog.type === 'edit'
			? dispatch(Actions.closeEditUserDialog())
			: dispatch(Actions.closeNewUserDialog());
	}

	// Fornmsy
	const disableButton = () => {
		setIsFormValid(false);
	};

	const enableButton = () => {
		setIsFormValid(true);
	};

	const handleSubmit = () => {
		const data = {
			username: useremail.split('@', 1).toString(),
			email: useremail,
			roles,
			usersId,
			companyId,
			createdAt: new Date()
		};
		console.log('Create USer =>', data);
		if (rolemain !== 'Admin') {
			dispatch(Actions.addUserforCompany(data));
		} else {
			dispatch(Actions.addUser(data));
		}
		setUseremail('');
	};

	const handleUpdate = () => {
		const data = {
			userId: form._id,
			roles: form.roles
		};
		if (rolemain !== 'Admin') {
			console.log('run Sahi');
			dispatch(Actions.updateCompaniesRole(data, companyId));
		} else {
			dispatch(Actions.updateRole(data));
		}

		console.log('date submit', data);
	};

	return (
		<Dialog {...userDialog.props} onClose={closeUserDialog} fullWidth disableBackdropClick maxWidth="sm">
			<DialogTitle>{userDialog.type === 'new' ? 'Create an User' : 'Edit User'}</DialogTitle>

			{userDialog.type === 'new' ? (
				<DialogContent dividers classes={{ root: 'p-0' }}>
					<Formsy
						className="flex flex-col"
						onValidSubmit={handleSubmit}
						onValid={enableButton}
						onInvalid={disableButton}
						// ref={formRef}
					>
						<SelectFormsy
							className="m-8"
							name="roles"
							onChange={e => setRoles(e.target.value)}
							value={roles || ''}
							label="user-role"
							variant="outlined"
							validations={{
								minLength: 0
							}}
							validationErrors={{
								minLength: 'Not empty'
							}}
							required
						>
							{usersRole.map(role => (
								<MenuItem value={role} key={role}>
									{role}
								</MenuItem>
							))}
						</SelectFormsy>
						<TextFieldFormsy
							className="m-8"
							name="Email"
							onChange={e => setUseremail(e.target.value)}
							value={useremail || ''}
							label="email"
							variant="outlined"
							type="email"
							validations={{
								isEmail: true,
								minLength: 0
							}}
							validationErrors={{
								isEmail: 'Enter the Valid E-mail',
								minLength: 'Not empty'
							}}
							required
						/>

						<DialogActions className="p-8 w-full flex justify-end">
							<Button className="mx-8" onClick={() => dispatch(closeUserDialog())} color="primary">
								Cancel
							</Button>
							<Button
								className="mx-8"
								type="submit"
								variant="contained"
								color="secondary"
								disabled={!isFormValid}
								onClick={handleSubmit}
							>
								ADD
							</Button>
						</DialogActions>
					</Formsy>
				</DialogContent>
			) : (
				<DialogContent dividers classes={{ root: 'p-0' }}>
					<Formsy
						className="flex flex-col"
						onValidSubmit={handleUpdate}
						onValid={enableButton}
						onInvalid={disableButton}
						// ref={formRef}
					>
						<SelectFormsy
							className="m-8"
							name="role"
							onChange={e => setInForm('roles', e.target.value)}
							value={form.roles || ''}
							label="user-role"
							variant="outlined"
							validations={{
								minLength: 0
							}}
							validationErrors={{
								minLength: 'Not empty'
							}}
							required
						>
							{usersRole.map(role => (
								<MenuItem value={role} key={role}>
									{role}
								</MenuItem>
							))}
						</SelectFormsy>

						<DialogActions className="p-8 w-full flex justify-end">
							<Button className="mx-8" onClick={() => dispatch(closeUserDialog())} color="primary">
								CANCEL
							</Button>
							<Button
								className="mx-8"
								type="submit"
								variant="contained"
								color="secondary"
								disabled={!isFormValid}
							>
								UPDATE
							</Button>
						</DialogActions>
					</Formsy>
				</DialogContent>
			)}
		</Dialog>
	);
}

export default UserDialog;
