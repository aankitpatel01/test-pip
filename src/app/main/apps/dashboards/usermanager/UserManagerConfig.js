import React from 'react';
import { authRoles } from 'app/auth';

const UserManagerConfig = {
	settings: {
		layout: {
			config: {}
		}
	},
	auth: authRoles.staff,
	routes: [
		{
			path: '/users-manager',
			component: React.lazy(() => import('./usersLayout'))
		},
		{
			path: '/settings',
			component: React.lazy(() => import('../dashboardsettings/UserSettingsLayout'))
		}
	]
};

export default UserManagerConfig;
