import React, { useState, useEffect } from 'react';
import { TextFieldFormsy, SelectFormsy } from '@fuse/core/formsy';
import Formsy from 'formsy-react';
import { Button, MenuItem } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import * as Actions from './store/actions';

const defaultFormState = {
	id: '',
	name: '',
	address: '',
	category: '',
	city: '',
	country: '',
	state: '',
	vat: '',
	zip: ''
};

const CompanyDetailsTab = ({ CompanyDetails, userId }) => {
	const dispatch = useDispatch();

	const CountryList = useSelector(({ userSettingsLayout }) => userSettingsLayout.settings.countrylist);
	const StateList = useSelector(({ userSettingsLayout }) => userSettingsLayout.settings.statelist);
	const CityList = useSelector(({ userSettingsLayout }) => userSettingsLayout.settings.citylist);

	const [isFormValid, setIsFormValid] = useState(false);
	const [companyData, setCompanyData] = useState(defaultFormState);

	const { name, address, category, city, country, state, vat, zip } = companyData;

	useEffect(() => {
		console.log('ComayDetails', CompanyDetails);
		if (userId !== '') {
			setCompanyData({ ...defaultFormState, userId });
		}
		if (CompanyDetails !== '') {
			setCompanyData(CompanyDetails);
		}
	}, [userId, dispatch, CompanyDetails]);

	useEffect(() => {
		Promise.all([state, city])
			.then(() => dispatch(Actions.getStateList(country)))
			.then(() => dispatch(Actions.getCityList(state)));
	}, [dispatch, country, state, city]);

	function disableButton() {
		setIsFormValid(false);
	}

	function enableButton() {
		setIsFormValid(true);
	}

	function handleSubmit(model) {
		const data = { ...model, userId };
		console.log('Company Details ', data);
		dispatch(Actions.updateCompanyDetails(data));
	}

	const handleStateChange = e => {
		console.log(e.target.value);
		// const found = CountryList.find(cfind => cfind.name === e.target.value);
		// console.log(found);
		dispatch(Actions.getStateList(e.target.value));
	};

	const handleCityChange = e => {
		console.log(e.target.value);
		// const foundcity = StateList.find(sfind => sfind.name === e.target.value);
		// console.log(foundcity.id);
		dispatch(Actions.getCityList(e.target.value));
	};

	return (
		<Formsy
			className="w-full flex flex-wrap"
			onValidSubmit={handleSubmit}
			onValid={enableButton}
			onInvalid={disableButton}
			// ref={formRef}
		>
			<div className="w-full flex flex-col sm:flex-row">
				<TextFieldFormsy
					className="m-8 sm:w-1/2 md:w-1/2"
					name="name"
					label="Name"
					value={name}
					variant="outlined"
					required
					type="text"
					validations={{
						minLength: 0
					}}
					validationErrors={{
						minLength: 'Not Empty'
					}}
				/>

				<SelectFormsy
					className="m-8 sm:w-1/2 md:w-1/2"
					name="country"
					label="Country"
					value={country}
					variant="outlined"
					required
					onChange={handleStateChange}
				>
					<MenuItem value={country.name} key={country}>
						{country}
					</MenuItem>
					{CountryList !== ''
						? CountryList.map(cou => (
								<MenuItem value={cou.id} key={cou.id}>
									{cou.name}
								</MenuItem>
						  ))
						: country}
				</SelectFormsy>

				{/* {CountryList !== null ? CountryList.map(cou => console.log(cou.name)) : console.log('Nothign')} */}
			</div>
			<div className="w-full flex flex-col sm:flex-row">
				<SelectFormsy
					className="m-8 sm:w-1/2 md:w-1/2"
					label="State"
					name="state"
					value={state}
					variant="outlined"
					required
					onChange={handleCityChange}
				>
					<MenuItem value={state} key={state}>
						{state}
					</MenuItem>
					{StateList !== ''
						? StateList.map(st => (
								<MenuItem value={st.id} key={st.id}>
									{st.name}
								</MenuItem>
						  ))
						: ''}
				</SelectFormsy>

				<SelectFormsy
					className="m-8 sm:w-1/2 md:w-1/2"
					label="City"
					name="city"
					value={city}
					variant="outlined"
					required
				>
					<MenuItem value={city} key={city}>
						{city}
					</MenuItem>
					{CityList !== '' ? (
						CityList.map(ct => (
							<MenuItem value={ct.id} key={ct.id}>
								{ct.name}
							</MenuItem>
						))
					) : (
						<MenuItem value={city} key={city}>
							{city}
						</MenuItem>
					)}
				</SelectFormsy>
			</div>
			<div className="w-full flex flex-col sm:flex-row">
				<TextFieldFormsy
					className="m-8 sm:w-1/2 md:w-1/2"
					label="Address"
					name="address"
					value={address}
					variant="outlined"
					required
					type="text"
					validations={{
						minLength: 0
					}}
					validationErrors={{
						minLength: 'Not Empty'
					}}
				/>

				<TextFieldFormsy
					className="m-8 sm:w-1/2 md:w-1/2"
					label="Pincode"
					name="zip"
					value={zip}
					variant="outlined"
					required
					type="text"
					validations={{ isNumeric: true, isLength: 6 }}
					validationErrors={{
						isNumeric: 'Should be Digits',
						isLength: 'Should be Valid Pincode'
					}}
				/>
			</div>
			<div className="w-full flex flex-col sm:flex-row">
				<TextFieldFormsy
					className="m-8 sm:w-1/2 md:w-1/2"
					label="VAT"
					name="vat"
					value={vat}
					variant="outlined"
					required
					type="text"
					validations={{
						minLength: 0
					}}
					validationErrors={{
						minLength: 'Not Empty'
					}}
				/>

				<TextFieldFormsy
					className="m-8 sm:w-1/2 md:w-1/2"
					label="Category"
					name="category"
					value={category}
					variant="outlined"
					required
					type="text"
					validations={{
						minLength: 0
					}}
					validationErrors={{
						minLength: 'Not Empty'
					}}
				/>
			</div>
			<div className="m-8 w-full flex justify-end">
				<Button type="submit" variant="contained" color="secondary" disabled={!isFormValid}>
					Update
				</Button>
			</div>
		</Formsy>
	);
};

export default React.memo(CompanyDetailsTab);
