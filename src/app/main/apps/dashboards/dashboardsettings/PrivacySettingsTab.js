import React, { useState, useEffect } from 'react';
import { Button, FormControlLabel, Checkbox } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import * as Actions from './store/actions';

const defaultFormState = {
	allow_email_noty: false,
	allow_recurring_email_noty: false
};

const PrivacySettingsTab = ({ PrivacyData }) => {
	const dispatch = useDispatch();
	const { _id, settings } = PrivacyData;

	const [privacydata, setPrivacySetting] = useState(defaultFormState);

	useEffect(() => {
		console.log('Fetch PrivacyData', PrivacyData);

		if (PrivacyData !== null) {
			setPrivacySetting({ ...defaultFormState, ...settings });
		}
	}, [PrivacyData, settings, setPrivacySetting]);

	const handleChange = event => {
		setPrivacySetting({ ...privacydata, [event.target.name]: event.target.checked });
	};

	const handleUpdate = () => {
		const data = { userId: _id, setting: privacydata };
		console.log(data);
		dispatch(Actions.updatePrivacySetting(data));
	};

	if (!PrivacyData) {
		return null;
	}

	return (
		<>
			<div className="w-full flex flex-col justify-start">
				{console.log('Fetch Settings', settings)}
				<FormControlLabel
					control={
						<Checkbox
							checked={privacydata.allow_email_noty}
							onChange={handleChange}
							name="allow_email_noty"
							color="secondary"
						/>
					}
					label="Allow E-mail notifications"
				/>
				<FormControlLabel
					control={
						<Checkbox
							checked={privacydata.allow_recurring_email_noty}
							onChange={handleChange}
							name="allow_recurring_email_noty"
							color="secondary"
						/>
					}
					label="Allow recurring E-mail notifications"
				/>
			</div>
			<div className="m-8 w-full flex justify-end">
				<Button variant="contained" color="secondary" onClick={handleUpdate}>
					Update
				</Button>
			</div>
		</>
	);
};

export default React.memo(PrivacySettingsTab);
