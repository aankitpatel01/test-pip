import FusePageCarded from '@fuse/core/FusePageCarded';
import { makeStyles } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import React, { useState, useEffect } from 'react';
import withReducer from 'app/store/withReducer';
import { useSelector, useDispatch } from 'react-redux';
import reducer from './store/reducers';
import PersonalDetailsTab from './PersonalDetailsTab';
import CompanyDetailsTab from './CompanyDetailsTab';
import PrivacySettingsTab from './PrivacySettingsTab';
import CompanyNumberTab from './CompanyNumberTab';
import * as Actions from './store/actions';

const useStyles = makeStyles({
	layoutRoot: {}
});

function UserSettingsLayout() {
	const classes = useStyles();
	const dispatch = useDispatch();
	const userId = useSelector(({ auth }) => auth.user.data.id);
	const userRole = useSelector(({ auth }) => auth.user.role);
	const SettingsData = useSelector(({ userSettingsLayout }) => userSettingsLayout.settings.personal);

	useEffect(() => {
		if (userRole === 'Company') {
			dispatch(Actions.getCompanySettings(userId));
		} else {
			dispatch(Actions.getSettings(userId));
		}
		dispatch(Actions.getCountryList());
	}, [dispatch, userId, userRole]);

	const [selectedTab, setSelectedTab] = useState(0);

	const handleTabChange = (event, value) => {
		setSelectedTab(value);
	};

	return (
		<FusePageCarded
			classes={{
				root: classes.layoutRoot,
				toolbar: 'p-0'
			}}
			header={null}
			contentToolbar={
				<Tabs
					value={selectedTab}
					onChange={handleTabChange}
					indicatorColor="primary"
					textColor="primary"
					variant="fullWidth"
					scrollButtons="off"
					className="w-full h-64"
				>
					<Tab className="h-64" label="Personal Details" />
					{userRole !== 'Admin' && <Tab className="h-64" label="Company Details" />}
					{userRole !== 'Admin' && <Tab className="h-64" label="Company Number" />}
					<Tab className="h-64" label="Privacy Settings" />
				</Tabs>
			}
			content={
				<>
					{userRole === 'Admin' ? (
						<div className="p-24">
							{console.log('Setting =>', SettingsData)}
							{selectedTab === 0 && (
								<>
									<h2 className="mb-16">Personal Details</h2>
									<PersonalDetailsTab personalDetails={SettingsData} />
								</>
							)}

							{selectedTab === 1 && (
								<div>
									<h2 className="mb-16">Privacy Settings</h2>
									<PrivacySettingsTab PrivacyData={SettingsData} />
								</div>
							)}
						</div>
					) : (
						<div className="p-24">
							{console.log('Setting =>', SettingsData)}
							{selectedTab === 0 && (
								<>
									<h2 className="mb-16">Personal Details</h2>
									<PersonalDetailsTab personalDetails={SettingsData} />
								</>
							)}
							{selectedTab === 1 && (
								<div>
									<h2 className="mb-16">Company Details</h2>
									<CompanyDetailsTab CompanyDetails={SettingsData.company} userId={userId} />
								</div>
							)}
							{selectedTab === 2 && (
								<div>
									<h2 className="mb-16">Company Numbers</h2>
									<CompanyNumberTab id={userId} />
								</div>
							)}
							{selectedTab === 3 && (
								<div>
									<h2 className="mb-16">Privacy Settings</h2>
									<PrivacySettingsTab PrivacyData={SettingsData} />
								</div>
							)}
						</div>
					)}
				</>
			}
		/>
	);
}

export default withReducer('userSettingsLayout', reducer)(UserSettingsLayout);
