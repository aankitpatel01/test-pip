import axios from 'axios';
import * as Actions from 'app/store/actions';

export const GET_PERSONAL_INFO = '[USER SETTING] GET PERSONAL INFO';
export const GET_COMPANY_INFO = '[USER SETTING] GET_COMPANY_INFO';
export const GET_COUNTRY_LIST = '[USER SETTING] GET COUNTRY LIST';
export const GET_STATE_LIST = '[USER SETTING] GET STATE LIST';
export const GET_CITY_LIST = '[USER SETTING] GET CITY LIST';
export const UPDATE_PERSONAL_INFO = '[USER SETTING] UPDATE PERSONAL INFO';
export const UPDATE_COMPANY_DETAILS = '[USER SETTING] UPDATE COMPANY DETAILS';
export const UPDATE_PRIVACY_SETTING = '[USER SETTING] UPDATE PRIVACY SETTING';
export const UPDATE_PASSWORD = '[USER SETTING] UPDATE PASSWORD';

export function getSettings(id) {
	const data = { id };
	const request = axios.post('/list/getUserById', data);

	return dispatch =>
		request.then(response =>
			dispatch({
				type: GET_PERSONAL_INFO,
				payload: response.data.users
			})
		);
}
export function getCompanySettings(id) {
	const request = axios.get(`company/getCompany/${id}`);

	return dispatch =>
		request.then(response =>
			dispatch({
				type: GET_COMPANY_INFO,
				payload: response.data.users
			})
		);
}

export function getCountryList() {
	const request = axios.get('/company/getCountryList');

	return dispatch =>
		request.then(response =>
			dispatch({
				type: GET_COUNTRY_LIST,
				payload: response.data.country
			})
		);
}
export function getStateList(countryId) {
	const data = { country_id: countryId };
	const request = axios.post('/company/getStateListByCountryId', data);

	return dispatch =>
		request.then(response =>
			dispatch({
				type: GET_STATE_LIST,
				payload: response.data.states
			})
		);
}

export function getCityList(stateId) {
	const data = { state_id: stateId };
	const request = axios.post('/company/getCitiesListByStateId', data);

	return dispatch =>
		request.then(response =>
			dispatch({
				type: GET_CITY_LIST,
				payload: response.data.cities
			})
		);
}

export function updatePersonalInfo(data) {
	const request = axios.post('/user/updateProfile', data);

	return dispatch =>
		request.then(response => {
			Promise.all([
				dispatch({
					type: UPDATE_PERSONAL_INFO,
					payload: response.data
				})
			]).then(() => dispatch(Actions.showMessage({ message: response.data.message, variant: 'success' })));
		});
}

export function updateCompanyDetails(data) {
	console.log(data);
	const request = axios.post('/company/updateCompany', data);

	return dispatch =>
		request.then(response => {
			Promise.all([
				dispatch({
					type: UPDATE_COMPANY_DETAILS,
					payload: response.data
				})
			]).then(() => dispatch(Actions.showMessage({ message: response.data.message, variant: 'success' })));
		});
}

export function updatePassword(data) {
	const request = axios.post('/user/updatePassword', data);

	return dispatch =>
		request.then(response => {
			Promise.all([
				dispatch({
					type: UPDATE_PASSWORD
				})
			]).then(() => dispatch(Actions.showMessage({ message: response.data.message, variant: 'success' })));
		});
}

export function updatePrivacySetting(data) {
	const request = axios.post('company/savePrivacySetting', data);

	return dispatch =>
		request.then(response => {
			Promise.all([
				dispatch({
					type: UPDATE_PRIVACY_SETTING
				})
			]).then(() => dispatch(Actions.showMessage({ message: response.data.message, variant: 'success' })));
		});
}
