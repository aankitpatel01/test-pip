import * as Actions from '../actions';

const initialState = {
	data: null,
	countrylist: [],
	statelist: [],
	citylist: [],
	personal: {
		firstname: '',
		lastname: '',
		email: '',
		phone: '',
		roles: '',
		company: {
			address: '',
			category: '',
			city: '',
			country: '',
			name: '',
			state: '',
			userId: '',
			vat: '',
			zip: ''
		},
		settings: null
	}
};

const settingsReducer = (state = initialState, action) => {
	switch (action.type) {
		case Actions.GET_PERSONAL_INFO:
			return {
				...state,
				personal: { ...action.payload }
			};
		case Actions.GET_COMPANY_INFO:
			return {
				...state,
				personal: { ...action.payload }
			};
		case Actions.GET_COUNTRY_LIST:
			return {
				...state,
				countrylist: [...action.payload]
			};
		case Actions.GET_STATE_LIST:
			return {
				...state,
				statelist: [...action.payload]
			};
		case Actions.GET_CITY_LIST:
			return {
				...state,
				citylist: [...action.payload]
			};
		case Actions.UPDATE_PERSONAL_INFO:
			return {
				...state,
				data: { ...action.payload }
			};
		case Actions.UPDATE_COMPANY_DETAILS:
			return {
				...state,
				data: { ...action.payload }
			};
		case Actions.UPDATE_PRIVACY_SETTING:
			return {
				...state
			};
		case Actions.UPDATE_PASSWORD:
			return {
				...state
			};
		default:
			return state;
	}
};

export default settingsReducer;
