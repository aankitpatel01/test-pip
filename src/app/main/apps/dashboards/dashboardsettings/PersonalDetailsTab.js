import React, { useState, useEffect } from 'react';
import { SelectFormsy, TextFieldFormsy } from '@fuse/core/formsy';
import { MenuItem, Button } from '@material-ui/core';
import Formsy from 'formsy-react';
import { useDispatch } from 'react-redux';
import FuseLoading from '@fuse/core/FuseLoading';
import * as Actions from './store/actions';

const PersonalDetailsTab = ({ personalDetails }) => {
	const dispatch = useDispatch();

	const { id } = personalDetails;

	const [isFormValid, setIsFormValid] = useState(false);
	const [isPasswordValid, setIsPasswordValid] = useState(false);
	const [personalData, setPersonalData] = useState({ firstname: '', lastname: '', email: '', phone: '', roles: '' });
	const { firstname, lastname, email, phone, roles } = personalData;

	useEffect(() => {
		if (personalDetails !== '') {
			setPersonalData(personalDetails);
		}
	}, [personalDetails]);

	const disableButton = () => {
		setIsFormValid(false);
	};

	const enableButton = () => {
		setIsFormValid(true);
	};

	const handleSubmit = model => {
		console.log('Update personal Data', model);
		const data = {
			...model,
			id
		};
		console.log(data);
		dispatch(Actions.updatePersonalInfo(data));
	};
	// Change password Methods
	const disablePasswordButton = () => {
		setIsPasswordValid(false);
	};

	const enablePasswordButton = () => {
		setIsPasswordValid(true);
	};

	const handlePasswordSubmit = model => {
		console.log('Changed Password', model);
		const data = {
			...model,
			id
		};
		console.log(data);
		dispatch(Actions.updatePassword(data));
	};

	if (personalData === '') {
		return <FuseLoading />;
	}

	return (
		<>
			<Formsy
				className="w-full flex flex-wrap"
				onValidSubmit={handleSubmit}
				onValid={enableButton}
				onInvalid={disableButton}
				// ref={formRef}
			>
				<SelectFormsy
					name="role"
					value={roles}
					label="user-role"
					variant="outlined"
					className="m-8 w-full"
					disabled
				>
					<MenuItem disabled value={roles} key={roles}>
						{roles}
					</MenuItem>
				</SelectFormsy>

				<div className="w-full flex flex-col sm:flex-row">
					<TextFieldFormsy
						className="m-8 sm:w-1/2 md:w-1/2"
						name="firstname"
						value={firstname}
						label="First Name"
						variant="outlined"
						type="text"
						validations={{
							minLength: 0
						}}
						validationErrors={{
							minLength: 'Not empty'
						}}
						required
					/>

					<TextFieldFormsy
						className="m-8 sm:w-1/2 md:w-1/2"
						name="lastname"
						value={lastname}
						label="Last Name"
						variant="outlined"
						type="text"
						validations={{
							minLength: 0
						}}
						validationErrors={{
							minLength: 'Not empty'
						}}
						required
					/>
				</div>
				<div className="w-full flex flex-col sm:flex-row">
					<TextFieldFormsy
						className="m-8 sm:w-1/2 md:w-1/2"
						name="email"
						value={email}
						label="E-mail"
						variant="outlined"
						type="email"
						validations="isEmail"
						validationErrors={{ isEmail: 'Email not valid' }}
						required
					/>

					<TextFieldFormsy
						className="m-8 sm:w-1/2 md:w-1/2"
						name="phone"
						value={phone}
						label="Phone"
						variant="outlined"
						required
						type="text"
						validations={{ isNumeric: true, isLength: 10 }}
						validationErrors={{
							isNumeric: 'Should be Number',
							isLength: 'Must be 10 digit number'
						}}
					/>
				</div>
				<div className="m-8 w-full flex justify-end">
					<Button type="submit" variant="contained" color="secondary" disabled={!isFormValid}>
						Update
					</Button>
				</div>
			</Formsy>
			<Formsy
				className="w-full flex flex-wrap"
				onValidSubmit={handlePasswordSubmit}
				onValid={enablePasswordButton}
				onInvalid={disablePasswordButton}
			>
				<div className="w-full flex flex-col sm:flex-row">
					<TextFieldFormsy
						className="m-8 sm:w-1/2 md:w-1/2"
						value=""
						label="New Password"
						name="password"
						variant="outlined"
						type="password"
						required
					/>

					<TextFieldFormsy
						className="m-8 sm:w-1/2 md:w-1/2"
						value=""
						label="Repeat Password"
						variant="outlined"
						name="confirm_password"
						type="password"
						validations="equalsField:password"
						validationErrors={{
							equalsField: 'not match'
						}}
						required
					/>
				</div>
				<div className="m-8 w-full flex justify-end">
					<Button type="submit" variant="contained" color="secondary" disabled={!isPasswordValid}>
						Change password
					</Button>
				</div>
			</Formsy>
		</>
	);
};

export default React.memo(PersonalDetailsTab);
