import React from 'react';
import {
	IconButton,
	Icon,
	DialogTitle,
	DialogContent,
	ListItem,
	ListItemText,
	Typography,
	Button,
	DialogActions
} from '@material-ui/core';
import { useDispatch } from 'react-redux';
import FuseAnimate from '@fuse/core/FuseAnimate';
import * as FuseActions from 'app/store/actions';
// import { ListItem, ListItemText, Typography } from '@material-ui/core';

const CompanyNumberItem = props => {
	const dispatch = useDispatch();
	// const { id, name, email, registered, lastLogin, role } = props.local;
	return (
		<FuseAnimate animation="transition.slideUpIn" delay={200}>
			<ListItem>
				<div className="px-8 w-1/5 sm:w-full sm:p-0 truncate">
					<ListItemText
						primary={
							<Typography variant="subtitle1" className="truncate">
								+555529204923
							</Typography>
						}
					/>
				</div>
				<div className="px-8 w-1/5 sm:w-1/2 sm:p-0 truncate">
					<ListItemText
						primary={
							<Typography variant="subtitle1" className="truncate">
								UAE
							</Typography>
						}
					/>
				</div>
				<div className="px-8 w-1/5 sm:w-1/2 sm:p-0 truncate">
					<ListItemText
						primary={
							<Typography variant="subtitle1" className="truncate">
								Local
							</Typography>
						}
					/>
				</div>
				<div className="px-8 w-1/5 sm:w-1/2 sm:p-0 truncate">
					<ListItemText primary={<Typography variant="subtitle1">ENG</Typography>} />
				</div>

				<div className="px-8 w-1/5 sm:w-1/2 text-center">
					<IconButton
						onClick={() => {
							dispatch(
								FuseActions.openDialog({
									children: (
										<>
											<DialogTitle id="delete-user-item">Delete User ?</DialogTitle>
											<DialogContent dividers className="p-24 flex flex-row">
												<Typography className="flex flex-row text-center" variant="subtitle1">
													Are You want to Delete{' '}
													<p className="px-4 font-bold text-red-700">nono</p> ?
												</Typography>
											</DialogContent>
											<DialogActions>
												<Button
													color="primary"
													onClick={() => {
														dispatch(FuseActions.closeDialog());
													}}
												>
													Cancel
												</Button>
												<Button color="primary" autoFocus>
													OK
												</Button>
											</DialogActions>
										</>
									)
								})
							);
						}}
					>
						<Icon className="text-red-700">delete</Icon>
					</IconButton>
				</div>
			</ListItem>
		</FuseAnimate>

		// <div className="flex flex-row">
		// 	<div className="px-8 w-1/5  sm:p-0 truncate">+555529204923</div>
		// 	<div className="px-8 w-1/5  sm:p-0 truncate">UAE</div>
		// 	<div className="px-8 w-1/5  sm:p-0 truncate">Local</div>
		// 	<div className="px-8 w-1/5  sm:p-0 truncate">ENG</div>
		// 	<div className="px-8 w-1/5  sm:p-0 truncate">
		// 		<IconButton>
		// 			<Icon className="text-red-700">delete</Icon>
		// 		</IconButton>
		// 	</div>
		// </div>
	);
};

export default React.memo(CompanyNumberItem);
