import React from 'react';
import { Typography, ListItemText, ListItem } from '@material-ui/core';
import CompanyNumberItem from './CompanyNumberItem';

const CompanyNumberTab = () => {
	return (
		<>
			<ListItem>
				<div className="px-8 w-1/5 sm:w-full sm:p-0 truncate">
					<ListItemText
						primary={
							<Typography variant="subtitle1" className="truncate font-bold">
								Number
							</Typography>
						}
					/>
				</div>
				<div className="px-8 w-1/5 sm:w-1/2 sm:p-0 truncate">
					<ListItemText
						primary={
							<Typography variant="subtitle1" className="truncate font-bold">
								Country
							</Typography>
						}
					/>
				</div>
				<div className="px-8 w-1/5 sm:w-1/2 sm:p-0 truncate">
					<ListItemText
						primary={
							<Typography variant="subtitle1" className="truncate font-bold">
								Type
							</Typography>
						}
					/>
				</div>
				<div className="px-8 w-1/5 sm:w-1/2 sm:p-0 truncate">
					<ListItemText
						primary={
							<Typography variant="subtitle1" className="font-bold">
								Stream
							</Typography>
						}
					/>
				</div>

				<div className="px-8 w-1/5 sm:w-1/2 text-center font-bold">Action </div>
			</ListItem>

			{/* <div className="p-24 w-full flex">
				<div className="flex w-1/5 my-8 font-bold">Number</div>
				<div className="flex w-1/5 my-8 font-bold">Country</div>
				<div className="flex w-1/5 my-8 font-bold">Type</div>
				<div className="flex w-1/5 my-8 font-bold">Stream</div>
				<div className="flex w-1/5 my-8 font-bold">Action</div>
			</div> */}
			<div className="w-full flex flex-wrap flex-col sm:p-0">
				<CompanyNumberItem />
			</div>
		</>
	);
};

export default React.memo(CompanyNumberTab);
