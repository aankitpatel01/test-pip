import axios from 'axios';
import History from '@history';
import * as FuseActions from 'app/store/actions';

export const GET_WIDGETS = '[ANALYTICS DASHBOARD APP] GET WIDGETS';
export const GET_WIDGETS_DATA = '[ANALYTICS DASHBOARD APP] GET WIDGETS DATA';
export const GET_ALL_EVENTS = '[ANALYTICS DASHBOARD APP] GET  ALL EVENTS';
export const GET_ALL_EVENTS_HISTORY = '[ANALYTICS DASHBOARD APP] GET ALL EVENTS HISTORY';
export const GET_COMPANY_LIST = '[ANALYTICS DASHBOARD APP] GET COMPANY LIST';
export const GET_LANGUAGE_LIST = '[ANALYTICS DASHBOARD APP] GET LANGUAGE LIST';
export const DELETE_COMPANY = '[ANALYTICS DASHBOARD APP] DELETE COMPANY';
export const CREATE_NEW_EVENT = '[ANALYTICS DASHBOARD APP] CREATE NEW EVENT';
export const CREATE_NEW_COMPANY = '[ANALYTICS DASHBOARD APP] CREATE NEW COMPANY';
export const CLOSE_COMPANY_DIALOG = '[ANALYTICS DASHBOARD APP] CLOSE COMPANY DIALOG';
export const OPEN_COMPANY_DIALOG = '[ANALYTICS DASHBOARD APP] OPEN COMPANY DIALOG';

export function openCompanyDialog() {
	return {
		type: OPEN_COMPANY_DIALOG
	};
}

export function closeCompanyDialog() {
	return {
		type: CLOSE_COMPANY_DIALOG
	};
}

export function getCompanyList() {
	const request = axios.get('list/allCompany');

	return dispatch =>
		request
			.then(response =>
				dispatch({
					type: GET_COMPANY_LIST,
					payload: response.data.users
				})
			)
			.catch(err => console.log(err));
}

export function deleteCompany(companyid) {
	const request = axios.get(`list/deleteCompany/${companyid}`);

	return dispatch =>
		request
			.then(response =>
				dispatch({
					type: DELETE_COMPANY,
					payload: response.data.users
				})
			)
			.then(() => dispatch(FuseActions.closeDialog()))
			.then(() => dispatch(getCompanyList()))
			.then(() =>
				dispatch(
					FuseActions.showMessage({ message: 'Company Details delete successfully!', variant: 'success' })
				)
			)
			.catch(err => console.log(err));
}

export function createNewEvent(data) {
	console.log('createNewEvent Action=>', data);
	const request = axios.post('/event/saveEvent', data);

	return dispatch =>
		request
			.then(response =>
				dispatch({
					type: CREATE_NEW_EVENT,
					payload: response.data
				})
			)
			.then(res => {
				console.log(res.payload.eventId);
				History.push(`/create-event/${res.payload.eventId}`);
			})
			.catch(err => console.log(err));
}

export function createNewCompany(data) {
	console.log('createNewCompany Action => ', data);
	const request = axios.post('/auth/signup', data);

	return dispatch =>
		request
			.then(response =>
				Promise.all([
					dispatch({
						type: CREATE_NEW_COMPANY,
						payload: response.data
					})
				])
					.then(() => dispatch(getCompanyList()))
					.then(() => dispatch(closeCompanyDialog()))
					.then(() =>
						dispatch(FuseActions.showMessage({ message: response.data.message, variant: 'success' }))
					)
			)
			.catch(error => {
				console.log(error.response.data);
				dispatch(FuseActions.showMessage(error.response.data));
			});
}

export function getWidgets() {
	const request = axios.get('/api/analytics-dashboard-app/widgets');

	return dispatch =>
		request.then(response =>
			dispatch({
				type: GET_WIDGETS,
				payload: response.data
			})
		);
}

// Getting Widget Data from Server
export function getWidgetsData(data) {
	console.log('GetWidgetsData Action =>', data);
	const request = axios.post('/stats/getStatsForLanguage', data);

	return dispatch =>
		request
			.then(response => {
				dispatch({
					type: GET_WIDGETS_DATA,
					payload: response.data
				});
			})
			.catch(err => console.log('error', err));
}

export function getAllEvent(companyId, page, status) {
	const request = axios.get(`/event/getAllEvent/?companyId=${companyId}&page=${page}&status=${status}`);

	return dispatch =>
		request
			.then(response =>
				dispatch({
					type: GET_ALL_EVENTS,
					payload: response.data
				})
			)
			.catch(error => console.log(error));
}
export function getAllEventHistory(companyId, page, status) {
	const request = axios.get(`/event/getAllEvent/?companyId=${companyId}&page=${page}&status=${status}`);

	return dispatch =>
		request
			.then(response =>
				dispatch({
					type: GET_ALL_EVENTS_HISTORY,
					payload: response.data
				})
			)
			.catch(error => console.log(error));
}
