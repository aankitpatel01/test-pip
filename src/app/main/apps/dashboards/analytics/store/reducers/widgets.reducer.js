import * as Actions from '../actions';

const initialState = {
	data: null,
	newData: null,
	page: 0,
	pages: null,
	companylist: [],
	events: [],
	history: [],
	eventId: null,
	companyDialog: {
		type: 'new',
		props: {
			open: false
		},
		data: null
	}
};

const widgetsReducer = (state = initialState, action) => {
	switch (action.type) {
		case Actions.OPEN_COMPANY_DIALOG: {
			return {
				...state,
				companyDialog: {
					type: 'new',
					props: {
						open: true
					},
					data: null
				}
			};
		}
		case Actions.CLOSE_COMPANY_DIALOG: {
			return {
				...state,
				companyDialog: {
					type: 'new',
					props: {
						open: false
					},
					data: null
				}
			};
		}
		case Actions.GET_WIDGETS:
			return {
				...state,
				data: { ...action.payload }
			};
		case Actions.GET_WIDGETS_DATA:
			return {
				...state,
				newData: { ...action.payload }
			};
		case Actions.GET_ALL_EVENTS:
			return {
				...state,
				events: [...action.payload.events],
				page: action.page,
				pages: action.payload.pages
			};
		case Actions.GET_ALL_EVENTS_HISTORY:
			return {
				...state,
				history: [...action.payload.events],
				page: action.page,
				pages: action.payload.pages
			};
		case Actions.GET_COMPANY_LIST:
			return {
				...state,
				companylist: [...action.payload]
			};
		case Actions.CREATE_NEW_EVENT:
			return {
				...state,
				eventId: action.payload.id
			};
		default:
			return state;
	}
};

export default widgetsReducer;
