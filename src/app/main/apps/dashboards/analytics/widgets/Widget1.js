import FuseAnimate from '@fuse/core/FuseAnimate';
import _ from '@lodash';
import Button from '@material-ui/core/Button';
import { makeStyles, ThemeProvider, useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import React, { useState, useEffect } from 'react';
import { Line } from 'react-chartjs-2';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../store/actions';

const useStyles = makeStyles(theme => ({
	root: {
		background: `linear-gradient(to right, ${theme.palette.primary.dark} 0%, ${theme.palette.primary.main} 100%)`
	}
}));
const options = {
	spanGaps: false,
	legend: {
		display: false
	},
	maintainAspectRatio: false,
	layout: {
		padding: {
			top: 32,
			left: 32,
			right: 32
		}
	},
	elements: {
		point: {
			radius: 4,
			borderWidth: 2,
			hoverRadius: 4,
			hoverBorderWidth: 2
		},
		line: {
			tension: 0
		}
	},
	scales: {
		xAxes: [
			{
				gridLines: {
					display: false,
					drawBorder: false,
					tickMarkLength: 18
				},
				ticks: {
					fontColor: '#ffffff'
				}
			}
		],
		yAxes: [
			{
				display: false,
				ticks: {
					min: 1.5,
					max: 5,
					stepSize: 0.5
				}
			}
		]
	},
	plugins: {
		filler: {
			propagate: false
		},
		xLabelsOnTop: {
			active: true
		}
	}
};

function Widget1(props) {
	const dispatch = useDispatch();
	const mainThemeDark = useSelector(({ fuse }) => fuse.settings.mainThemeDark);

	const classes = useStyles(props);
	const theme = useTheme();
	const [dataset, setDataset] = useState('JAN');

	// API Request Dispatch
	const requestDispatch = key => {
		setDataset(key);
		// let crnt_date = ;
		const currentYear = new Date().getFullYear();
		const month = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
		const monthIndex = month.indexOf(key);
		const data = {
			month: monthIndex,
			companyId: props.compID,
			year: currentYear.toString()
		};
		if (props.compID !== null || props.compID !== undefined) {
			dispatch(Actions.getWidgetsData(data));
		}
	};

	useEffect(() => {
		requestDispatch(dataset);
	}, []);

	const data = _.merge({}, props.data);

	let days = [];
	let chartData = [];

	if (props.newData !== null) {
		if (props.newData.events.length !== 0) {
			const customData = {
				fill: 'start',
				label: 'Views',
				data: [...props.newData.events.countes]
			};
			chartData = [customData];
			days = props.newData.events.days;
		}
	}
	console.log('here', props);
	return (
		<ThemeProvider theme={mainThemeDark}>
			<div className={classes.root}>
				<div className="container relative p-16 sm:p-24 flex flex-row justify-between items-center">
					<FuseAnimate delay={100}>
						<div className="flex-col">
							<Typography className="h2" color="textPrimary">
								Event Data
							</Typography>
							<Typography className="h5" color="textSecondary">
								By Month
							</Typography>
						</div>
					</FuseAnimate>

					<div className="flex flex-row flex-wrap justify-end items-center">
						{Object.keys(data.datasets).map(key => (
							<Button
								key={key}
								className="py-8 px-12"
								size="small"
								onClick={() => requestDispatch(key)}
								disabled={key === dataset}
							>
								{key}
							</Button>
						))}
					</div>
				</div>
				<div className="container relative h-200 sm:h-256 pb-16">
					{days.length === 0 ? (
						<Line
							data={{
								labels: data.labels,
								datasets: data.datasets[dataset].map(obj => ({
									...obj,
									borderColor: theme.palette.secondary.main,
									backgroundColor: theme.palette.secondary.main,
									pointBackgroundColor: theme.palette.secondary.dark,
									pointHoverBackgroundColor: theme.palette.secondary.main,
									pointBorderColor: theme.palette.secondary.contrastText,
									pointHoverBorderColor: theme.palette.secondary.contrastText
								}))
							}}
							options={options}
						/>
					) : (
						<Line
							data={{
								labels: days,
								datasets: chartData.map(obj => ({
									...obj,
									borderColor: theme.palette.secondary.main,
									backgroundColor: theme.palette.secondary.main,
									pointBackgroundColor: theme.palette.secondary.dark,
									pointHoverBackgroundColor: theme.palette.secondary.main,
									pointBorderColor: theme.palette.secondary.contrastText,
									pointHoverBorderColor: theme.palette.secondary.contrastText
								}))
							}}
							options={options}
						/>
					)}
				</div>
			</div>
		</ThemeProvider>
	);
}

export default React.memo(Widget1);
