import React from 'react';
import Icon from '@material-ui/core/Icon';
import { ListItem, ListItemText, Typography, IconButton, Tooltip, Box } from '@material-ui/core';
import moment from 'moment';

const CreateHistoryItem = ({ event }) => {
	const { eventName, status, location, startTime, endTime } = event;

	return (
		<>
			<ListItem divider>
				{status === 'publish' ? (
					<div className="p-16 py-20 rounded bg-green-500">
						<Icon className="text-white">event</Icon>
					</div>
				) : (
					<div className="p-16 py-20 rounded bg-yellow-700">
						<Icon className="text-white">bookmark</Icon>
					</div>
				)}
				<ListItemText
					className=" px-16 w-1/3"
					primary={
						<Typography
							className={status === 'publish' ? 'text-green-600' : 'text-yellow-700'}
							variant="caption"
						>
							{status}
						</Typography>
					}
					secondary={<Typography variant="h6">{eventName}</Typography>}
				/>
				<ListItemText
					className="px-16 w-1/3"
					primary={
						<>
							<Typography variant="subtitle2">One time event</Typography>
							<Typography className="text-red">{location}</Typography>
						</>
					}
					secondary={
						<Typography variant="subtitle2">
							{`${moment(startTime).format('ddd, D MMMM, h:mm a')} -
								${moment(endTime).format('ddd, D MMMM, h:mm a')}`}
						</Typography>
					}
				/>

				<Box className="w-1/3 flex flex-col sm:flex-col md:flex-row flex-1 justify-center ">
					<Tooltip title="Analytics" enterDelay={500} leaveDelay={200}>
						<IconButton aria-label="Analytics">
							<Icon className="text-black">bar_chart</Icon>
						</IconButton>
					</Tooltip>
					<Tooltip title="Copy Event" enterDelay={500} leaveDelay={200}>
						<IconButton aria-label="Copy Event">
							<Icon className="text-black">file_copy</Icon>
						</IconButton>
					</Tooltip>
					<Tooltip title="Volume Up" enterDelay={500} leaveDelay={200}>
						<IconButton aria-label="Copy to Clipboard">
							<Icon className="text-black">volume_up</Icon>
						</IconButton>
					</Tooltip>
				</Box>
			</ListItem>
		</>
	);
};
export default CreateHistoryItem;
