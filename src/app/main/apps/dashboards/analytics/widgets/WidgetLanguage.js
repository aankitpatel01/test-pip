import _ from '@lodash';
import Card from '@material-ui/core/Card';
import Icon from '@material-ui/core/Icon';
// import { useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import React, { useState } from 'react';

function WidgetLanguage(props) {
	// const theme = useTheme();
	const [dataset, setDataset] = useState('Today');
	const data = _.merge({}, props.data);

	let value = [];
	if (props.newData !== null) {
		if (props.newData.events.length !== 0) {
			value = props.newData.events.StatsCount;
		}
	}

	// console.log('Widget Language >', props);
	return (
		<Card className="w-full rounded-8 shadow-none border-1">
			<div className="p-16 flex flex-row items-center justify-between">
				<div>
					<Typography className="h1 font-300">Language</Typography>
				</div>
				{/* <div>
					<Typography className="text-blue px-8 font-700">See all</Typography>
				</div> */}
			</div>

			<div className="p-16 flex flex-row items-center justify-start table-responsive">
				{value.length === 0
					? data.labels.map((label, index) => (
							<div key={label} className="px-20 flex flex-col items-center">
								<Typography className="h4" color="textSecondary">
									{label}
								</Typography>
								<Typography className="h2 font-300 py-8">
									{data.datasets[dataset][0].data[index]}%
								</Typography>

								<div className="flex flex-row items-center justify-center">
									{data.datasets[dataset][0].change[index] < 0 && (
										<Icon className="text-18 text-red">arrow_downward</Icon>
									)}

									{data.datasets[dataset][0].change[index] > 0 && (
										<Icon className="text-18 text-green">arrow_upward</Icon>
									)}
									<div className="h5 px-4">{data.datasets[dataset][0].change[index]}%</div>
								</div>
							</div>
					  ))
					: value.map((item,index) => (
							<div key={index} className="px-20 flex flex-col items-center">
								<Typography className="h4" color="textSecondary">
									{item._id}
								</Typography>
								<Typography className="h2 font-300 py-8">
									{item.count}%
								</Typography>

								<div className="flex flex-row items-center justify-center">
									{data.datasets[dataset][0].change[index] < 0 && (
										<Icon className="text-18 text-red">arrow_downward</Icon>
									)}

									{data.datasets[dataset][0].change[index] > 0 && (
										<Icon className="text-18 text-green">arrow_upward</Icon>
									)}
									<div className="h5 px-4">{item.count}%</div>
								</div>
							</div>
					  ))}
			</div>
		</Card>
	);
}

export default React.memo(WidgetLanguage);
