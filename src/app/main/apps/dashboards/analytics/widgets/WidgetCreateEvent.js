// import _ from '@lodash';
// import { useTheme } from '@material-ui/core/styles';
import React, { useEffect, useState } from 'react';
import { Fab, Button, Tooltip, List, Typography, Card, Icon } from '@material-ui/core';
import clsx from 'clsx';
import { useDispatch, useSelector } from 'react-redux';
import { Pagination } from '@material-ui/lab';
import CreateEventItem from './CreateEventItem';
import CreateHistoryItem from './CreateHistoryItem';
import * as Actions from '../store/actions';

function WidgetCreateEvent(props) {
	const dispatch = useDispatch();
	const user = useSelector(({ auth }) => auth.user.data);
	const loginRoles = useSelector(({ auth }) => auth.user.role);
	const companyId = useSelector(({ auth }) => auth.user.data.companyId);
	const allEvents = useSelector(({ analyticsDashboardApp }) => analyticsDashboardApp.widgets.events);
	const allHistory = useSelector(({ analyticsDashboardApp }) => analyticsDashboardApp.widgets.history);
	const pageData = useSelector(({ analyticsDashboardApp }) => analyticsDashboardApp.widgets.pages);

	console.log('Props', allEvents);
	// const theme = useTheme();
	const [selectedTab, setselectedTab] = useState(0);
	const [pagecount, setpagecount] = useState(1);
	// const data = _.merge({}, props.data);

	useEffect(() => {
		dispatch(Actions.getWidgets());
		if (loginRoles !== 'Admin') {
			dispatch(Actions.getAllEvent(companyId, 0, 'unarchived'));
			dispatch(Actions.getAllEventHistory(companyId, 0, 'archived'));
		}
	}, [dispatch, loginRoles, companyId]);

	const newEventCreate = () => {
		dispatch(Actions.createNewEvent({ status: 'new', userId: user.id, companyId: user.companyId }));
	};

	const handleChange = (e, page) => {
		console.log(e, page);
		setpagecount(page);
		const changepage = page - 1;
		dispatch(Actions.getAllEvent(user.companyId, changepage));
	};

	return (
		<>
			<Card className="w-full rounded-8 shadow-none border-1">
				<div className="p-16 mx-4 w-full flex flex-row justify-between">
					<div className="flex w-full sm:w-1/2">
						<Button
							size="large"
							variant="contained"
							disableElevation
							className={clsx(selectedTab === 0 ? 'bg-blue-500  m-8' : 'm-8', 'hover:bg-blue-A400')}
							onClick={() => setselectedTab(0)}
						>
							Current Event
						</Button>
						<Button
							size="large"
							variant="contained"
							disableElevation
							className={clsx(selectedTab === 1 ? 'bg-blue-500  m-8' : 'm-8', 'hover:bg-blue-A400')}
							onClick={() => setselectedTab(1)}
						>
							History
						</Button>
					</div>
					<div className="flex justify-end items-center p-8 sm:w-1/2">
						<Tooltip title="Create New Event">
							<Fab
								color="primary"
								className="bg-red-700"
								aria-label="add"
								onClick={newEventCreate}
								// component={Link}
								// to="/create-event"
							>
								<Icon>add</Icon>
							</Fab>
						</Tooltip>
					</div>
				</div>
				{selectedTab === 0 && (
					<div className="flex flex-col items-center justify-start">
						{allEvents.length === 0 && (
							<Typography variant="h5" className="p-24">
								No Event Available
							</Typography>
						)}
						<List className="w-full px-16">
							{allEvents.map((event, index) => (
								<CreateEventItem key={index} event={event} />
							))}
						</List>
					</div>
				)}
				{selectedTab === 1 && (
					<div className="flex flex-col items-center justify-start">
						{allHistory.length === 0 && (
							<Typography variant="h5" className="p-24">
								No Event Available
							</Typography>
						)}
						<List className="w-full px-16">
							{allHistory.map((event, index) => (
								<CreateHistoryItem key={index} event={event} />
							))}
						</List>
					</div>
				)}
			</Card>
			<div className="w-full flex justify-end py-12">
				<Pagination count={pageData} color="primary" page={pagecount} onChange={handleChange} />
			</div>
		</>
	);
}

export default React.memo(WidgetCreateEvent);
