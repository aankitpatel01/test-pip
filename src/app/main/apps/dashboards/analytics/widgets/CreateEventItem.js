import React from 'react';
import Icon from '@material-ui/core/Icon';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { ListItem, ListItemText, Typography, ListItemSecondaryAction, IconButton, Tooltip } from '@material-ui/core';

const CreateEventItem = ({ event }) => {
	const { eventId, eventName, status, location, startTime, endTime } = event;

	return (
		<>
			<ListItem divider button component={Link} to={`/create-event/${eventId}`}>
				{status === 'publish' ? (
					<div className="p-16 py-20 rounded bg-green-500">
						<Icon className="text-white">event</Icon>
					</div>
				) : (
					<div className="p-16 py-20 rounded bg-yellow-700">
						<Icon className="text-white">bookmark</Icon>
					</div>
				)}
				<ListItemText
					className=" px-16 w-1/3"
					primary={
						<Typography
							className={status === 'publish' ? 'text-green-600' : 'text-yellow-700'}
							variant="caption"
						>
							{status}
						</Typography>
					}
					secondary={<Typography variant="h6">{eventName}</Typography>}
				/>
				<ListItemText
					className="px-16 w-1/3"
					primary={
						<>
							<Typography variant="subtitle2">One time event</Typography>
							<Typography className="text-red">{location}</Typography>
						</>
					}
					secondary={
						<Typography variant="subtitle2">
							{`${moment(startTime).format('ddd, D MMMM, h:mm a')} -
								${moment(endTime).format('ddd, D MMMM, h:mm a')}`}
						</Typography>
					}
				/>
				<ListItemSecondaryAction>
					<Tooltip title="Copy Event">
						<IconButton aria-label="Copy Event">
							<Icon className="text-black">file_copy</Icon>
						</IconButton>
					</Tooltip>
				</ListItemSecondaryAction>
			</ListItem>
		</>
	);
};
export default React.memo(CreateEventItem);
