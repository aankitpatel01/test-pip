import React from 'react';
import { authRoles } from 'app/auth';

const AnalyticsDashboardAppConfig = {
	settings: {
		layout: {
			config: {}
		}
	},
	auth: authRoles.staff,
	routes: [
		{
			path: '/dashboard',
			component: React.lazy(() => import('./AnalyticsDashboardApp'))
		},
		{
			path: '/company-list',
			component: React.lazy(() => import('./CompanyList'))
		}
	]
};

export default AnalyticsDashboardAppConfig;
