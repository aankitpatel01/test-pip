// import { useForm } from '@fuse/hooks';
// import FuseUtils from '@fuse/utils';
// import _ from '@lodash';
// import moment from 'moment/moment';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { TextFieldFormsy } from '@fuse/core/formsy';
import Formsy from 'formsy-react';
import { DialogTitle, DialogContent, Button, Dialog } from '@material-ui/core';
import * as Actions from './store/actions';

const CreateCompanyDialog = () => {
	const dispatch = useDispatch();
	const companyDialog = useSelector(({ analyticsDashboardApp }) => analyticsDashboardApp.widgets.companyDialog);

	const [isFormValid, setIsFormValid] = useState(false);
	// const { form, setForm, setInForm } = useForm({ ...defaultFormState });

	const closeCompanyDialog = () => {
		dispatch(Actions.closeCompanyDialog());
	};

	// Fornmsy
	const disableButton = () => {
		setIsFormValid(false);
	};

	const enableButton = () => {
		setIsFormValid(true);
	};

	const handleSubmit = modal => {
		const data = {
			...modal,
			username: modal.email.split('@', 1).toString(),
			roles: 'Company'
		};

		console.log('model', data);
		dispatch(Actions.createNewCompany(data));
	};

	return (
		<Dialog {...companyDialog.props} onClose={closeCompanyDialog} fullWidth disableBackdropClick maxWidth="sm">
			<DialogTitle>{companyDialog.type === 'new' ? 'Create an User' : 'Edit User'}</DialogTitle>

			<DialogContent dividers classes={{ root: 'p-0' }}>
				<Formsy
					className="w-full flex flex-wrap"
					onValidSubmit={handleSubmit}
					onValid={enableButton}
					onInvalid={disableButton}
					// ref={formRef}
				>
					<div className="w-full flex flex-col sm:flex-row">
						<TextFieldFormsy
							className="m-8 sm:w-1/2 md:w-1/2"
							label="name"
							name="name"
							value=""
							variant="outlined"
							required
							type="text"
							validations={{
								minLength: 0
							}}
							validationErrors={{
								minLength: 'Not Empty'
							}}
						/>
						<TextFieldFormsy
							className="m-8 sm:w-1/2 md:w-1/2"
							name="email"
							value=""
							label="E-mail"
							variant="outlined"
							type="email"
							validations={{
								isEmail: true,
								minLength: 0
							}}
							validationErrors={{
								isEmail: 'Enter the Valid E-mail',
								minLength: 'Not empty'
							}}
							required
						/>
					</div>

					<div className="w-full flex flex-col sm:flex-row">
						<TextFieldFormsy
							className="m-8 sm:w-1/2 md:w-1/2"
							label="Address"
							name="address"
							value=""
							variant="outlined"
							required
							type="text"
							validations={{
								minLength: 0
							}}
							validationErrors={{
								minLength: 'Not Empty'
							}}
						/>

						<TextFieldFormsy
							className="m-8 sm:w-1/2 md:w-1/2"
							label="Pincode"
							name="zip"
							value=""
							variant="outlined"
							required
							type="text"
							validations={{ isNumeric: true, isLength: 6 }}
							validationErrors={{
								isNumeric: 'Should be Digits',
								isLength: 'Should be Valid Pincode'
							}}
						/>
					</div>
					<div className="w-full flex flex-col sm:flex-row">
						<TextFieldFormsy
							className="m-8 sm:w-1/2 md:w-1/2"
							label="VAT"
							name="vat"
							value=""
							variant="outlined"
							required
							type="text"
							validations={{
								minLength: 0
							}}
							validationErrors={{
								minLength: 'Not Empty'
							}}
						/>

						<TextFieldFormsy
							className="m-8 sm:w-1/2 md:w-1/2"
							label="Category"
							name="category"
							value=""
							variant="outlined"
							required
							type="text"
							validations={{
								minLength: 0
							}}
							validationErrors={{
								minLength: 'Not Empty'
							}}
						/>
					</div>
					<div className="m-8 w-full flex justify-end">
						<Button className="mx-8" onClick={() => dispatch(Actions.closeCompanyDialog())}>
							Cancel
						</Button>
						<Button type="submit" variant="contained" color="secondary" disabled={!isFormValid}>
							Create
						</Button>
					</div>
				</Formsy>
			</DialogContent>
		</Dialog>
	);
};

export default CreateCompanyDialog;
