import React, { useEffect } from 'react';
import FusePageCarded from '@fuse/core/FusePageCarded';
import {
	ListItem,
	ListItemText,
	Typography,
	Icon,
	Button,
	List,
	IconButton,
	DialogActions,
	DialogTitle,
	DialogContent
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import FuseLoading from '@fuse/core/FuseLoading';
import FuseAnimateGroup from '@fuse/core/FuseAnimateGroup';
import FuseAnimate from '@fuse/core/FuseAnimate';
import moment from 'moment';
import * as FuseActions from 'app/store/actions';
import * as Actions from './store/actions';
import reducer from './store/reducers';
import CreateCompanyDialog from './CreateCompanyDialog';

const CompanyList = () => {
	const dispatch = useDispatch();
	const companylist = useSelector(({ analyticsDashboardApp }) => analyticsDashboardApp.widgets.companylist);

	useEffect(() => {
		dispatch(Actions.getCompanyList());
	}, [dispatch]);

	if (!companylist) {
		return <FuseLoading />;
	}

	return (
		<FusePageCarded
			header={
				<div className="w-full flex items-center justify-between">
					<FuseAnimate animation="transition.slideRightIn" delay={300}>
						<div className="flex flex-row ">
							<Icon className="mx-8">business</Icon>
							<h2>Company List</h2>
						</div>
					</FuseAnimate>
					<FuseAnimate animation="transition.slideLeftIn" delay={300}>
						<Button
							variant="contained"
							color="secondary"
							onClick={() => {
								dispatch(Actions.openCompanyDialog());
							}}
						>
							ADD COMPANY
						</Button>
					</FuseAnimate>

					{console.log('Get All Company LIst', companylist)}
				</div>
			}
			contentToolbar={
				<div className="w-full flex flex-row px-24">
					<div className="px-8 w-1/5 sm:w-full sm:p-0 truncate">
						<Typography variant="subtitle1" className="truncate font-bold">
							Organization
						</Typography>
					</div>
					<div className="px-8 w-2/5 sm:w-1/2 sm:p-0 truncate">
						<Typography variant="subtitle1" className="truncate font-bold">
							Owner
						</Typography>
					</div>

					<div className="px-8 w-1/5 sm:w-1/2 text-center sm:p-0 truncate">
						<Typography variant="subtitle1" className="font-bold">
							Type
						</Typography>
					</div>

					<div className="px-8 w-1/5 sm:w-1/2 text-center font-bold">
						<Typography variant="subtitle1" className="truncate font-bold">
							Registered
						</Typography>
					</div>
					<div className="px-8 w-1/5 sm:w-1/2 text-center font-bold">
						<Typography variant="subtitle1" className="truncate font-bold">
							Action
						</Typography>
					</div>
				</div>
			}
			content={
				<>
					<FuseAnimateGroup
						enter={{
							animation: 'transition.slideUpBigIn'
						}}
						leave={{
							animation: 'transition.slideUpBigOut'
						}}
					>
						<List>
							{companylist.map(t => (
								<ListItem key={t.id}>
									<div className="px-8 w-1/5 flex flex-row items-center sm:w-full sm:p-0 truncate">
										<Icon className="mx-8">business</Icon>
										<ListItemText
											primary={
												<Typography variant="subtitle1" className="truncate">
													{t.company.name || ''}
												</Typography>
											}
										/>
									</div>
									<div className="px-8 w-2/5 sm:w-1/2 sm:p-0 truncate">
										<ListItemText
											primary={
												<Typography variant="subtitle1" className="truncate">
													{t.email || ''}
												</Typography>
											}
										/>
									</div>

									<div className="px-8 w-1/5 sm:w-1/2 text-center sm:p-0 truncate">
										<ListItemText
											primary={<Typography variant="subtitle1">{t.roles || ''} </Typography>}
										/>
									</div>
									<div className="px-8 w-1/5 sm:w-1/2 sm:p-0 truncate">
										<ListItemText
											primary={
												<Typography variant="subtitle1">
													{moment(t.createdAt).format('ddd, D MMMM, h:mm a') || ''}
												</Typography>
											}
										/>
									</div>

									<div className="px-8 w-1/5 sm:w-1/2 text-center">
										<IconButton
											edge="end"
											aria-label="delete"
											className="px-24"
											onClick={() => {
												dispatch(
													FuseActions.openDialog({
														children: (
															<>
																<DialogTitle id="delete-company-item">
																	Delete Company ?
																</DialogTitle>
																<DialogContent dividers className="p-24 flex flex-row">
																	<Typography
																		className="flex flex-row text-center"
																		variant="subtitle1"
																	>
																		Are You want to Delete{' '}
																		<p className="px-4 font-bold text-red-700">
																			{t.company.name}
																		</p>{' '}
																		?
																	</Typography>
																</DialogContent>
																<DialogActions>
																	<Button
																		color="primary"
																		onClick={() => {
																			dispatch(FuseActions.closeDialog());
																		}}
																	>
																		Cancel
																	</Button>
																	<Button
																		color="primary"
																		onClick={() => {
																			dispatch(Actions.deleteCompany(t.id));
																		}}
																		autoFocus
																	>
																		OK
																	</Button>
																</DialogActions>
															</>
														)
													})
												);
											}}
										>
											<Icon className="font-bold text-red-700">delete</Icon>
										</IconButton>
									</div>
								</ListItem>
							))}
						</List>
					</FuseAnimateGroup>
					<CreateCompanyDialog />
				</>
			}
		/>
	);
};
export default withReducer('analyticsDashboardApp', reducer)(CompanyList);
