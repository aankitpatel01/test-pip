import React from 'react';

const WebAppConfig = {
	settings: {
		layout: {
			config: {
				navbar: {
					display: false
				},
				toolbar: {
					display: false
				},
				footer: {
					display: false
				},
				leftSidePanel: {
					display: false
				},
				rightSidePanel: {
					display: false
				}
			}
		},
		theme: {
			main: 'mainThemeDark'
		}
	},
	auth: null,
	routes: [
		{
			path: '/stream',
			component: React.lazy(() => import('./WebAppUser'))
		},
		{
			path: '/error',
			component: React.lazy(() => import('./WebAppUserError'))
		},
		{
			path: '/client/:code',
			component: React.lazy(() => import('./WebAppStream'))
		}
	]
};

export default WebAppConfig;
