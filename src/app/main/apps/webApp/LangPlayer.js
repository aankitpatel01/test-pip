import { Button, DialogActions, DialogContent, Icon, IconButton, makeStyles, Typography } from '@material-ui/core';
import React, { useEffect, useRef, useState } from 'react';
import io from 'socket.io-client';
import * as FuseActions from 'app/store/actions';
import { useDispatch, useSelector } from 'react-redux';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
	inputButtons: {
		left: '-90px'
	},
	outputButton: {
		right: '-90px'
	},
	myPlayer: {
		width: '230px'
	},
	playIcon: {
		// fontSize: '12rem'
		fontSize: '5rem'
	}
}));

const LangPlayer = props => {
	const { streamData } = props;
	const { sessionID } = props;

	// console.log('Lang Player Component', sessionID);
	const dispatch = useDispatch();
	const classes = useStyles();
	const userAudio = useRef();
	const { eventId, roomId, streamId, langtitle } = streamData;
	const playButtonRedux = useSelector(({ webStreamAppData }) => webStreamAppData.webAppReducer.playButton);

	const [playButton, setPlayButton] = useState(true);

	useEffect(() => {
		if (playButtonRedux) {
			console.log('playButtonRedux stoop Done  !!!!');
		} else {
			console.log('playButtonRedux run Done !!!!');
			viewerJoined();
		}

		return () => {
			console.log('Clean UP Audio Player !!!');
			userAudio.current.srcObject = null;
			if (!playButton) {
				setPlayButton(true);
			}
		};
	}, [playButtonRedux, viewerJoined]);

	// variables
	let user;
	const rtcPeerConnections = {};
	const playfun = document.getElementById('audioplayer');

	// constants
	const iceServers = {
		iceServers: [{ urls: 'stun:stun.services.mozilla.com' }, { urls: 'stun:stun.l.google.com:19302' }]
	};
	// const streamConstraints = { audio: true };

	// Let's do this 💪
	const socket = io.connect('https://daastapanel.hashweb.in/');

	// const handleInterpreter = () => {
	// 	user = {
	// 		room: 'event/room1',
	// 		name: 'farukh'
	// 	};

	// 	navigator.mediaDevices
	// 		.getUserMedia(streamConstraints)
	// 		.then(stream => {
	// 			userAudio.current.srcObject = stream;
	// 			socket.emit('register as broadcaster', user.room);
	// 		})
	// 		.catch(err => {
	// 			console.log('An error ocurred when accessing media devices', err);
	// 		});
	// };

	socket.on('reconnect', data => {
		console.log('Reconnecting...', data);
		socket.emit('register as viewer', user);
	});

	const viewerJoined = () => {
		user = {
			room: `eventRTC/${eventId}/${roomId}/${streamId}/${sessionID}`,
			JoinedData: { ...props.JoinedData, langtitle }
		};
		console.log('Viewer', user.room);
		// divSelectRoom.style = 'display: none;';
		// divConsultingRoom.style = 'display: block;';

		if (eventId !== null && roomId !== null && streamId !== null) {
			console.log('Viewer', user.room);
			if (playButton) {
				socket.emit('register as viewer', user);
				setPlayButton(false);
				// playfun.play();
			} else {
				socket.emit('listener_disconnect', { ...props.leaveData, endTime: new Date().toISOString() });
				setPlayButton(true);
				playfun.pause();
				// userAudio.current.srcObject.getAudioTracks()[0].enabled = false;
			}
		} else {
			dispatch(
				FuseActions.openDialog({
					children: (
						<>
							<DialogContent className="p-12 mx-24">
								<Typography variant="h6">Stream Not Selected</Typography>
							</DialogContent>
							<DialogActions>
								<Button
									color="primary"
									onClick={() => {
										dispatch(FuseActions.closeDialog());
									}}
								>
									Close
								</Button>
							</DialogActions>
						</>
					)
				})
			);
		}
	};

	// message handlers
	socket.on('new viewer', viewer => {
		rtcPeerConnections[viewer.id] = new RTCPeerConnection(iceServers);

		const stream = userAudio.current.srcObject;
		stream.getTracks().forEach(track => rtcPeerConnections[viewer.id].addTrack(track, stream));

		rtcPeerConnections[viewer.id].onicecandidate = event => {
			if (event.candidate) {
				console.log('sending ice candidate', event);
				socket.emit('candidate', viewer.id, {
					type: 'candidate',
					label: event.candidate.sdpMLineIndex,
					id: event.candidate.sdpMid,
					candidate: event.candidate.candidate
				});
			}
		};

		rtcPeerConnections[viewer.id]
			.createOffer()
			.then(sessionDescription => {
				rtcPeerConnections[viewer.id].setLocalDescription(sessionDescription);
				socket.emit('offer', viewer.id, {
					type: 'offer',
					sdp: sessionDescription,
					broadcaster: user
				});
			})
			.catch(error => {
				console.log(error);
			});

		// let li = document.createElement('li');
		// li.innerText = viewer.name + ' has joined';
		// viewers.appendChild(li);
	});

	socket.on('candidate', (id, event) => {
		const candidate = new RTCIceCandidate({
			sdpMLineIndex: event.label,
			candidate: event.candidate
		});
		rtcPeerConnections[id].addIceCandidate(candidate);
	});

	socket.on('offer', (broadcaster, sdp) => {
		// broadcasterName.innerText = `${broadcaster.name}is broadcasting...`;

		rtcPeerConnections[broadcaster.id] = new RTCPeerConnection(iceServers);

		rtcPeerConnections[broadcaster.id].setRemoteDescription(sdp);

		rtcPeerConnections[broadcaster.id].createAnswer().then(sessionDescription => {
			rtcPeerConnections[broadcaster.id].setLocalDescription(sessionDescription);
			socket.emit('answer', {
				type: 'answer',
				sdp: sessionDescription,
				room: user.room
			});
		});

		rtcPeerConnections[broadcaster.id].ontrack = event => {
			userAudio.current.srcObject = event.streams[0];
		};

		rtcPeerConnections[broadcaster.id].onicecandidate = event => {
			if (event.candidate) {
				console.log('sending ice candidate');
				socket.emit('candidate', broadcaster.id, {
					type: 'candidate',
					label: event.candidate.sdpMLineIndex,
					id: event.candidate.sdpMid,
					candidate: event.candidate.candidate
				});
			}
		};
	});

	socket.on('answer', (viewerId, event) => {
		rtcPeerConnections[viewerId].setRemoteDescription(new RTCSessionDescription(event));
	});

	// console.log ('Laguage Player Component', streamData);

	return (
		<div>
			<Typography variant="h5" className="text-center mb-16 font-bold hidden">
				{langtitle === '' ? 'No Stream Avaliable' : langtitle}
			</Typography>
			<audio
				playsInline
				id="audioplayer"
				ref={userAudio}
				muted={playButton}
				autoPlay
				controls
				className={clsx('hidden', classes.myPlayer)}
			/>
			<IconButton className="text-6xl hidden" onClick={viewerJoined}>
				{playButton ? (
					<Icon className={classes.playIcon}>play_circle_filled</Icon>
				) : (
					<Icon className={classes.playIcon}>pause_circle_filled</Icon>
				)}
			</IconButton>
		</div>
	);
};

export default LangPlayer;
