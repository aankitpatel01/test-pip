import React, { useEffect, useRef } from 'react';
// Libraries
import clsx from 'clsx';
import moment from 'moment';
import Slider from 'react-slick';
import { Card } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// Redux
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from './store/actions';
// Styles
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
// Card
import LangCard from './LangCard';

// Custom Styles
const useStyles = makeStyles(theme => ({
	myCard: {
		outline: 'none',
		maxWidth: '258px',
		borderRadius: '5%'
	}
}));
// Slick Slider Settings
const settings = {
	dots: false,
	arrows: false,
	infinite: false,
	slidesToShow: 1,
	centerMode: true,
	slidesToScroll: 1,
	swipeToSlide: true,
	className: 'center'
};
// Component
const CardSlider = props => {
	// Ref Hook
	const sliderRef = useRef();

	const dispatch = useDispatch();

	const selLangID = useSelector(({ webStreamAppData }) => webStreamAppData.webAppReducer.languageID);
	// const langtitle = useSelector(({ webStreamAppData }) => webStreamAppData.webAppReducer.langtitle);

	const { session } = props; // Destructuring props

	const classes = useStyles();

	const pointerArray = []; // Array to store True and False

	const gotoIndex = (item, index, arr) => {
		if (moment().isBetween(item.recstartTime, item.recEndTime)) {
			pointerArray.push({ status: true });
		} else {
			pointerArray.push({ status: false });
		}
		cardPointer(arr);
	};

	// const cardPointer = arr => {
	// 	let data = {
	// 		roomId: null,
	// 		eventId: null,
	// 		streamId: null,
	// 		sessionID: null,
	// 		langtitle: null
	// 	};
	// 	const pointedObj = pointerArray.find((pointeritem, index) => pointeritem.status === true);

	// };
	const cardPointer = arr => {
		const data = {
			sessionID: null
		};
		pointerArray.filter(async (pointeritem, index) => {
			if (pointeritem.status === true) {
				data.sessionID = arr[index].id;

				const pointerData = await checkLangID(arr, data, index);
				// console.log('asdhfjkasdhfjkasdhfjhasdf', pointerData);

				sliderRef.current.slickGoTo(index);
				if (pointerData) {
					dispatch(Actions.getPlayerURL(data, pointerData));
				}
			}
			return true;
		});
	};

	const checkLangID = (arr, data, index) => {
		// debugger
		const sortedData = arr[index].streams.find(item => item.id === selLangID);
		return sortedData;
	};

	useEffect(() => {
		session.forEach(gotoIndex);
		// react-hooks/exhaustive-deps
	});

	return (
		<>
			{/* <Typography variant="h5" className="text-center mb-16 font-bold">
				{langtitle !== null ? langtitle : 'No Stream'}
			</Typography> */}
			<Slider ref={sliderRef} {...settings}>
				{session.map((item, index) => (
					<Card key={index} className={clsx(classes.myCard, 'bg-blue-600 shadow-2xl')}>
						<LangCard item={item} />
					</Card>
				))}
			</Slider>
		</>
	);
};

export default React.memo(CardSlider);
