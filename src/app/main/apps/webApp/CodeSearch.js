import React from 'react';
import { useForm } from '@fuse/hooks';
import { Typography, TextField, Button } from '@material-ui/core';
import History from '@history';

const CodeSearch = () => {
	const { form, handleChange, resetForm } = useForm({
		code: ''
	});

	function isFormValid() {
		return form.code.length > 0;
	}

	function handleSubmit(ev) {
		ev.preventDefault();
		History.push(`/client/${form.code}`);
		// dispatch(Actions.getEventByCode(form.code));
		resetForm();
	}

	return (
		<div>
			<Typography variant="h5" className="mb-16 font-bold">
				Find your Event
			</Typography>

			{/* <FuseCountdown endDate="2020-07-28" className="mssy-48" /> */}

			{/* 
							<Typography className=" my-32 w-full">
								If you would like to be notified when the app is ready, you can subscribe to our e-mail
								list.
							</Typography> */}

			<form
				name="subscribeForm"
				noValidate
				className="flex flex-1 flex-col justify-center w-full"
				onSubmit={handleSubmit}
			>
				<TextField
					className="w-224 mx-auto mb-32"
					label="Enter Code"
					autoFocus
					type="email"
					name="code"
					value={form.code}
					onChange={handleChange}
					required
					fullWidth
				/>

				<Button
					variant="contained"
					color="primary"
					className="w-224 mx-auto my-16 p-16 rounded-full"
					aria-label="Subscribe"
					disabled={!isFormValid()}
					type="submit"
				>
					SUBMIT
				</Button>
			</form>
		</div>
	);
};

export default CodeSearch;
