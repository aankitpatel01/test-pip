import React from 'react';
import FuseAnimate from '@fuse/core/FuseAnimate';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { grey } from '@material-ui/core/colors';
import { Card, Divider, CardContent } from '@material-ui/core';
import ErrorEvent from './ErrorEvent';

const useStyles = makeStyles(theme => ({
	root: {
		background: grey[400],
		color: theme.palette.primary.contrastText
	},
	bannerImage: {
		height: '40%'
	},
	contentContainer: {
		height: '50%'
	}
}));

const WebAppUserError = () => {
	const classes = useStyles();

	return (
		<div className={clsx(classes.root, 'flex flex-col flex-1 flex-shrink-0 items-center justify-center h-screen')}>
			<div className="flex flex-col items-center justify-center w-full h-full sm:py-24">
				<FuseAnimate animation="transition.expandIn">
					<Card className="w-full sm:max-w-400 h-screen :sm:h-full">
						<img
							className={clsx(classes.bannerImage, 'w-full flex flex-1 p-24')}
							src="assets/images/logos/undraw_Questions_re_1fy7.svg"
							alt="WebApp Error"
						/>
						<Divider className="w-384" />

						<CardContent
							className={clsx(
								classes.contentContainer,
								' flex flex-1 flex-col items-center justify-center p-32 text-center'
							)}
						>
							{/* <CodeSearch /> */}
							<ErrorEvent />
						</CardContent>
					</Card>
				</FuseAnimate>
			</div>
		</div>
	);
};

export default React.memo(WebAppUserError);
