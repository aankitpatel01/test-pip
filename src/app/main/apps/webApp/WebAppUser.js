import React from 'react';
import FuseAnimate from '@fuse/core/FuseAnimate';
// import * as FuseActions from 'app/store/actions';
// import FuseCountdown from '@fuse/core/FuseCountdown';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { grey } from '@material-ui/core/colors';
import { CardHeader, IconButton, Icon, Card, Divider, CardContent } from '@material-ui/core';
import CodeSearch from './CodeSearch';

const useStyles = makeStyles(theme => ({
	root: {
		background: grey[400],
		color: theme.palette.primary.contrastText
	},
	bannerImage: {
		height: '40%'
	},
	contentContainer: {
		height: '50%'
	}
}));

const WebAppUser = () => {
	const classes = useStyles();

	return (
		<div className={clsx(classes.root, 'flex flex-col flex-1 flex-shrink-0 items-center justify-center h-screen')}>
			<div className="flex flex-col items-center justify-center w-full h-screen sm:py-24">
				<FuseAnimate animation="transition.expandIn">
					<Card className="w-full sm:max-w-400 h-screen :sm:h-full">
						<CardHeader
							action={
								<>
									<IconButton aria-label="settings">
										<Icon>contact_support</Icon>
									</IconButton>
								</>
							}
						/>

						<img
							className={clsx(classes.bannerImage, 'w-full flex-1 p-24')}
							src="assets/images/logos/undraw_Login_re_4vu2.svg"
							alt="Daasta Web App"
						/>
						<Divider className="w-384" />

						<CardContent
							className={clsx(
								classes.contentContainer,
								' flex flex-1 flex-col items-center justify-center p-32 text-center'
							)}
						>
							<CodeSearch />
							{/* <ErrorEvent /> */}
						</CardContent>
					</Card>
				</FuseAnimate>
			</div>
		</div>
	);
};

export default React.memo(WebAppUser);
