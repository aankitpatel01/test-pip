import React from 'react';
import { Typography, Button } from '@material-ui/core';
import History from '@history';

const ErrorEvent = () => {
	const routeHome = () => {
		History.push('/stream');
	};
	return (
		<div>
			<Typography variant="h5" className="mb-16 font-bold">
				No Event Found
			</Typography>

			<Typography className=" my-32 w-full">Did not find any events with the code </Typography>

			<Button variant="contained" color="primary" className="w-224 mx-auto my-16 p-16 rounded-full">
				SEARCH NEAR BY
			</Button>
			<Button
				variant="contained"
				color="primary"
				className="w-224 mx-auto my-16 p-16 rounded-full"
				onClick={routeHome}
			>
				ENTER CODE
			</Button>
		</div>
	);
};

export default ErrorEvent;
