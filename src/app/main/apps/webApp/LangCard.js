import React, { useState } from 'react';
// Libraries
import moment from 'moment';
import { Fab, CardContent, Typography, DialogContent, DialogActions, Button } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import * as FuseActions from 'app/store/actions';
import PauseIcon from '@material-ui/icons/Pause';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import * as Actions from './store/actions';

const LangCard = ({ item }) => {
	const dispatch = useDispatch();
	const [play, setPlay] = useState(false);
	// const playStream = useSelector(({ webStreamAppData }) => webStreamAppData.webAppReducer.playButton);
	const sessionId = useSelector(({ webStreamAppData }) => webStreamAppData.webAppReducer.sessionID);

	const playHandler = itemId => {
		console.log('palyhandle ', play, itemId, sessionId);
		if (itemId === sessionId) {
			setPlay(!play);
			dispatch(Actions.playStream(play));
		} else {
			dispatch(
				FuseActions.openDialog({
					children: (
						<>
							<DialogContent className="p-12 mx-24">
								<Typography variant="h6">Stream Not Selected</Typography>
							</DialogContent>
							<DialogActions>
								<Button
									color="primary"
									onClick={() => {
										dispatch(FuseActions.closeDialog());
									}}
								>
									Close
								</Button>
							</DialogActions>
						</>
					)
				})
			);
		}
	};

	// console.log('Language Card Component', props.item.streams[0]);
	return (
		<CardContent className="rounded-2xl">
			<Typography className="text-left" color="textSecondary" gutterBottom>
				{moment(item.recstartTime).format('MMM DD, YYYY, hh:mm a')}
			</Typography>
			<Typography className="text-left" color="textSecondary">
				{item.title}
			</Typography>
			<Typography variant="h5" component="h2" className="text-xl">
				{moment(item.startTime).format('DD MMMM')}
			</Typography>
			<div className="flex justify-end">
				{play ? (
					<Fab className="mt-28" color="primary" aria-label="pause" onClick={() => playHandler(item.id)}>
						<PauseIcon />
					</Fab>
				) : (
					<Fab className="mt-28" color="primary" aria-label="add" onClick={() => playHandler(item.id)}>
						<PlayArrowIcon onClick={() => playHandler(item.id)} />
					</Fab>
				)}
			</div>
		</CardContent>
	);
};

export default LangCard;
