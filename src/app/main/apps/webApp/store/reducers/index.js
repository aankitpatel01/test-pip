import { combineReducers } from 'redux';
import webAppReducer from './webapp.reducer';

const reducer = combineReducers({
	webAppReducer
});

export default reducer;
