import * as Actions from '../actions';

const initialState = {
	data: null,
	playButton: true,
	roomId: null,
	eventId: null,
	streamId: null,
	langtitle: null,
	sessionID: null,
	languageID: null
};

const webAppReducer = (state = initialState, action) => {
	switch (action.type) {
		case Actions.CLEAN_UP_WEB_APP:
			return {
				...state,
				data: null
			};
		case Actions.GET_WEBAPP_EVENT_DATA:
			return {
				...state,
				data: { ...action.payload }
			};
		case Actions.GET_PLAYER_URL:
			return {
				...state,
				roomId: action.payload.roomId,
				eventId: action.payload.eventId,
				streamId: action.payload.streamId,
				langtitle: action.payload.langtitle,
				sessionID: action.payload.sessionID
			};
		case Actions.GET_LANGUAGE_ID:
			return {
				...state,
				languageID: action.payload
			};
		case Actions.PLAY_STREAM:
			return {
				...state,
				playButton: action.payload
			};
		default:
			return state;
	}
};

export default webAppReducer;
