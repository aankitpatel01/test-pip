import History from '@history';
import axios from 'axios';

export const CLEAN_UP_WEB_APP = '[WEBAPP APP] CLEAN_UP_WEB_APP DATA';
export const GET_WEBAPP_EVENT_DATA = '[WEBAPP APP] GET WEBAPP EVENT DATA';
export const GET_LANGUAGE_ID = '[WEBAPP APP] GET_LANGUAGE_ID';
export const GET_PLAYER_URL = '[WEBAPP APP] GET_PLAYER_URL';
export const PLAY_STREAM = '[WEBAPP APP] PLAY_STREAM';

export function cleanUpWebApp() {
	return dispatch => dispatch({ type: CLEAN_UP_WEB_APP });
}

export function getEventByCode(code) {
	console.log({ code });
	const request = axios.get(`/event/getEventWithCode/${code}`);

	return dispatch =>
		request
			.then(res => {
				Promise.all([
					dispatch({
						type: GET_WEBAPP_EVENT_DATA,
						payload: res.data
					})
				]);
			})
			.catch(error => {
				console.log(error);
				History.push('/error');
			});
}

export function getPlayerURL(data, pointerObj) {
	const newObj = {
		sessionID: data.sessionID,
		roomId: pointerObj.roomId,
		eventId: pointerObj.eventId,
		streamId: pointerObj.id,
		langtitle: pointerObj.title
	};

	return dispatch =>
		dispatch({
			type: GET_PLAYER_URL,
			payload: newObj
		});
}

export function getLanguageID(streamId) {
	return dispatch =>
		dispatch({
			type: GET_LANGUAGE_ID,
			payload: streamId
		});
}

export function playStream(val) {
	return dispatch =>
		dispatch({
			type: PLAY_STREAM,
			payload: val
		});
}
