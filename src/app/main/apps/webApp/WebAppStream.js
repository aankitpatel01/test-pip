import React, { useEffect } from 'react';
import FuseAnimate from '@fuse/core/FuseAnimate';
// import * as FuseActions from 'app/store/actions';

import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { grey } from '@material-ui/core/colors';
import {
	CardHeader,
	IconButton,
	Avatar,
	Icon,
	Card,
	Divider,
	CardContent,
	AppBar,
	Toolbar,
	Typography,
	MenuItem,
	Select
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import FuseLoading from '@fuse/core/FuseLoading';
import { useForm } from '@fuse/hooks';
import moment from 'moment';
import History from '@history';
import socket from 'socket.io-client';
import reducer from './store/reducers';
import LangPlayer from './LangPlayer';
import * as Actions from './store/actions';
import CardSlider from './cardSlider';

const useStyles = makeStyles(theme => ({
	root: {
		background: grey[400],
		color: theme.palette.primary.contrastText
	},
	bannerImage: {
		height: '40%'
	},
	contentContainer: {
		height: '90%',
		paddingTop: '35%'
	}
}));

const defaultFormState = { eventId: null, roomId: null, streamId: null, langtitle: '' };

const WebAppStream = ({ match }) => {
	const dispatch = useDispatch();
	const classes = useStyles();

	const { form, setInForm } = useForm(defaultFormState);

	const webStream = useSelector(({ webStreamAppData }) => webStreamAppData.webAppReducer.data);
	const sessionId = useSelector(({ webStreamAppData }) => webStreamAppData.webAppReducer.sessionID);
	const playButton = useSelector(({ webStreamAppData }) => webStreamAppData.webAppReducer.playButton);

	console.log('WebStream', webStream);
	console.log('url', form);

	useEffect(() => {
		dispatch(Actions.getEventByCode(match.params.code));
		// react-hooks/exhaustive-deps
	}, [dispatch, match.params.code]);

	const handleChangelang = (e, child) => {
		setInForm('eventId', child.props.stmevent);
		setInForm('roomId', child.props.stmroom);
		setInForm('langtitle', e.target.value);
		setInForm('streamId', child.props.stmid);
		dispatch(Actions.getLanguageID(child.props.stmid));
	};

	const handleLogoutClick = () => {
		History.push('/stream');
		dispatch(Actions.cleanUpWebApp());
	};

	const JoinedData = {
		eventId: webStream?.eventId,
		companyId: webStream?.companyId,
		socketId: socket.id,
		name: 'Mobile APP',
		// interpreterId: webStream?.id,
		role: 'Listner',
		protocol: 'webRTC',
		userAgent: 'WebApp',
		// data: avConsoleData,
		startTime: new Date().toISOString()
	};
	const leaveData = {
		eventId: webStream?.eventId,
		companyId: webStream?.companyId,
		socketId: socket.id,
		name: 'Mobile APP',
		// interpreterId: webStream?.id,
		role: 'Listner',
		protocol: 'webRTC',
		userAgent: 'WebApp',
		// data: avConsoleData,
		endTime: new Date().toISOString()
	};

	if (!webStream) {
		return <FuseLoading />;
	}

	// console.log('WebAppStream Component >', webStream);

	return (
		<div
			className={clsx(
				classes.root,
				'flex flex-col flex-1 flex-shrink-0 items-center justify-center p-0 sm:p-32 h-screen'
			)}
		>
			<div className="flex flex-col items-center justify-center w-full h-screen sm:py-24">
				<FuseAnimate animation="transition.expandIn">
					<Card className="w-full sm:max-w-400 h-screen :sm:h-full">
						<CardHeader
							avatar={<Avatar aria-label="logo">F</Avatar>}
							action={
								<>
									<IconButton aria-label="settings">
										<Icon>contact_support</Icon>
									</IconButton>

									<IconButton aria-label="delete" onClick={handleLogoutClick}>
										<Icon>exit_to_app</Icon>
									</IconButton>
								</>
							}
							title={webStream.eventName}
							subheader={moment(webStream.startTime).format('ddd, D MMMM, h:mm a')}
						/>
						<Divider className="w-384" />
						{/* <CardContent
							className={clsx(
								classes.contentContainer,
								'flex flex-1 flex-col items-center justify-center p-32 text-center -translate-x-full'
							)}
						>
							{webStream.rooms[0].streams.length > 0 &&
								webStream.rooms[0].streams.map(stream => (
									<LangPlayer key={stream.id} streamData={stream} />
								))}
							<LangPlayer streamData={form} />
							<Draggable axis="x" onDrag={handleDrag} position={deltaPos}>
								<Card className={clsx(classes.root, classes.myCard, 'bg-blue-600 shadow-2xl')}>
									<CardContent className={clsx('rounded-2xl')}>
										<Typography
											className={clsx(classes.title, 'text-left')}
											color="textSecondary"
											gutterBottom
										>
											12/22/2020
										</Typography>
										<Typography className={clsx(classes.pos, 'text-left')} color="textSecondary">
											Current Session {deltaPos.x.toFixed(0)}, {deltaPos.y.toFixed(0)}
										</Typography>
										<Typography variant="h5" component="h2">
											Room 1 - 22 December
										</Typography>
										<Fab className={clsx('mt-28')} color="primary" aria-label="add">
											<PlayArrowIcon />
										</Fab>
									</CardContent>
								</Card>
							</Draggable>
						</CardContent> */}
						<LangPlayer
							streamData={form}
							sessionID={sessionId}
							JoinedData={JoinedData}
							leaveData={leaveData}
						/>
						<CardContent className={clsx(classes.contentContainer)}>
							{webStream && <CardSlider session={webStream.session} />}
						</CardContent>

						<AppBar position="sticky" className="bottom-0 top-auto">
							<Toolbar className="flex justify-between">
								<div className="flex items-center w-1/2">
									<IconButton edge="start" color="inherit" aria-label="menu">
										{playButton ? (
											<Icon>play_circle_outline</Icon>
										) : (
											<Icon>pause_circle_outline</Icon>
										)}
									</IconButton>
									<Typography variant="h6" className="truncate">
										{webStream.eventName}
									</Typography>
								</div>
								<Select
									name="langtitle"
									variant="outlined"
									className="flex w-1/2"
									onChange={handleChangelang}
									value={form.langtitle}
									displayEmpty
									label="Select Stream"
								>
									{webStream.stream.length > 0 ? (
										webStream.stream.map(stream => (
											<MenuItem
												key={stream.id}
												stmevent={stream.eventId}
												stmroom={stream.roomId}
												stmid={stream.id}
												value={stream.title}
											>
												{stream.title}
											</MenuItem>
										))
									) : (
										<MenuItem value="0">Stream Not Available</MenuItem>
									)}
								</Select>
							</Toolbar>
						</AppBar>
					</Card>
				</FuseAnimate>
			</div>
		</div>
	);
};

export default withReducer('webStreamAppData', reducer)(React.memo(WebAppStream));
