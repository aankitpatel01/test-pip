import { Icon, Typography } from '@material-ui/core';
import React from 'react';
// import RoomInfo from '../RoomInfo';
import SummaryLang from './SummaryLang';

function SummaryRoomLang({ room, sessioncurrent }) {
	const { rooms, streams } = room;
	const roomdata = rooms[0];
	// console.log('roomname', roomdata);
	return (
		<div className="flex flex-col mx-24">
			<div className="flex flex-row mx-24 my-16">
				<Icon>event_note</Icon>
				<Typography className="mx-16 uppercase">{roomdata.title}</Typography>/
				<Typography className="mx-16 uppercase">{sessioncurrent?.title}</Typography>/
				<Typography className="mx-16 uppercase">time</Typography>
			</div>
			{streams.length > 0 &&
				streams.map(stream => (
					<SummaryLang key={stream.id} streamdata={stream} sessionId={sessioncurrent?.id} />
				))}
		</div>
	);
}

export default SummaryRoomLang;
