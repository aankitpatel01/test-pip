import React, { useRef, useState } from 'react';
import { Typography, Icon, IconButton, Tooltip, Badge, makeStyles } from '@material-ui/core';
import clsx from 'clsx';
import io from 'socket.io-client';

const useStyles = makeStyles(theme => ({
	textXs: {
		fontSize: '0.9rem',
		textTransform: 'uppercase'
	},
	audioLevel: {
		verticalAlign: ' middle',
		display: ' inline-block',
		height: '11px',
		lineHeight: '11px',
		width: '100%',
		position: 'relative',
		background: 'repeating-linear-gradient(90deg,#233d4b,#233d4b 50%,#564a43 0,#564a43 80%,#562d45 0,#562d45)'
	},
	badgeStatus: {
		left: '-16px',
		backgroundColor: '#44b700',
		color: '#44b700',
		boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
		'&::after': {
			borderRadius: '50%',
			animation: '$ripple 1.2s infinite ease-in-out',
			border: '1px solid currentColor',
			content: '""'
		}
	},
	'@keyframes ripple': {
		'0%': {
			transform: 'scale(.8)',
			opacity: 1
		},
		'100%': {
			transform: 'scale(2.4)',
			opacity: 0
		}
	},
	actionButton: {
		right: '-30px'
	},
	bgBlack: {
		backgroundColor: '#121212'
	}
}));

const SummaryLang = ({ streamdata, sessionId }) => {
	const classes = useStyles();
	const test = useRef('');
	const userAudio = useRef();
	const { eventId, roomId, id } = streamdata;

	const [playButton, setPlayButton] = useState(true);
	const [interpreterName, setInterpreterName] = useState('');

	// useEffect(() => {
	// 	return () => {
	// 		console.log('Clean UP Audio Player !!!');
	// 		userAudio.current.srcObject = null;
	// 		// setPlayButton(!playButton);
	// 		setInterpreterName('');
	// 	};
	// });
	// variables
	let user;
	const rtcPeerConnections = {};
	// const playfun = document.getElementById('audioplayer');

	// constants
	const iceServers = {
		iceServers: [{ urls: 'stun:stun.services.mozilla.com' }, { urls: 'stun:stun.l.google.com:19302' }]
	};
	// const streamConstraints = { audio: true };

	// Let's do this 💪
	const socket = io.connect('https://daastapanel.hashweb.in/');

	const viewerJoined = () => {
		user = {
			room: `eventRTC/${eventId}/${roomId}/${id}/${sessionId}`,
			name: 'Viewer'
		};
		// console.log('Viewer', user.room);
		// divSelectRoom.style = 'display: none;';
		// divConsultingRoom.style = 'display: block;';

		if (eventId !== null && roomId !== null && id !== null) {
			console.log('Viewer', user.room);
			if (playButton === true) {
				socket.emit('register as viewer', user);
				setPlayButton(false);
				// playfun.play();
			} else {
				setPlayButton(true);
				// playfun.pause();
			}
		}
	};

	// message handlers
	socket.on('new viewer', viewer => {
		console.log('viewer', viewer);
		rtcPeerConnections[viewer.id] = new RTCPeerConnection(iceServers);

		const stream = userAudio.current.srcObject;
		stream.getTracks().forEach(track => rtcPeerConnections[viewer.id].addTrack(track, stream));

		rtcPeerConnections[viewer.id].onicecandidate = event => {
			if (event.candidate) {
				console.log('sending ice candidate', event);
				socket.emit('candidate', viewer.id, {
					type: 'candidate',
					label: event.candidate.sdpMLineIndex,
					id: event.candidate.sdpMid,
					candidate: event.candidate.candidate
				});
			}
		};

		rtcPeerConnections[viewer.id]
			.createOffer()
			.then(sessionDescription => {
				rtcPeerConnections[viewer.id].setLocalDescription(sessionDescription);
				socket.emit('offer', viewer.id, {
					type: 'offer',
					sdp: sessionDescription,
					broadcaster: user
				});
			})
			.catch(error => {
				console.log(error);
			});

		// let li = document.createElement('li');
		// li.innerText = viewer.name + ' has joined';
		// viewers.appendChild(li);
	});

	socket.on('candidate', (cid, event) => {
		const candidate = new RTCIceCandidate({
			sdpMLineIndex: event.label,
			candidate: event.candidate
		});
		rtcPeerConnections[cid].addIceCandidate(candidate);
	});

	socket.on('offer', (broadcaster, sdp) => {
		console.log('broadcaster with sdp: ', broadcaster);
		setInterpreterName(broadcaster.JoinedData.name);

		// broadcasterName.innerText = `${broadcaster.name}is broadcasting...`;

		rtcPeerConnections[broadcaster.id] = new RTCPeerConnection(iceServers);

		rtcPeerConnections[broadcaster.id].setRemoteDescription(sdp);

		rtcPeerConnections[broadcaster.id].createAnswer().then(sessionDescription => {
			rtcPeerConnections[broadcaster.id].setLocalDescription(sessionDescription);
			socket.emit('answer', {
				type: 'answer',
				sdp: sessionDescription,
				room: user.room
			});
		});

		rtcPeerConnections[broadcaster.id].ontrack = event => {
			userAudio.current.srcObject = event.streams[0];
		};

		rtcPeerConnections[broadcaster.id].onicecandidate = event => {
			if (event.candidate) {
				console.log('sending ice candidate');
				socket.emit('candidate', broadcaster.id, {
					type: 'candidate',
					label: event.candidate.sdpMLineIndex,
					id: event.candidate.sdpMid,
					candidate: event.candidate.candidate
				});
			}
		};
	});

	socket.on('answer', (viewerId, event) => {
		rtcPeerConnections[viewerId].setRemoteDescription(new RTCSessionDescription(event));
	});

	return (
		<div className={clsx(classes.bgBlack, 'relative flex flex-row px-24 items-center')}>
			{playButton ? (
				<Badge className={clsx(classes.badgeStatus, 'absolute')} color="primary" variant="dot" />
			) : (
				<Badge className={clsx(classes.badgeStatus, 'absolute')} color="secondary" variant="dot" />
			)}
			<Typography className="w-1/5  justify-center uppercase">{streamdata.title}</Typography>
			<audio
				id="audioplayerOrg"
				playsInline
				ref={userAudio}
				muted={playButton}
				autoPlay
				controls
				className="hidden"
			/>
			<Typography ref={test} className="w-1/5  justify-center ">
				{interpreterName}
			</Typography>

			<Typography className="w-1/5  justify-center uppercase">BAR</Typography>

			<div className="w-1/5  flex items-center flex-row">
				<Tooltip
					placement="top"
					title={
						<Typography className="m-8 uppercase text-yellow-700">Not Present Source for Stream</Typography>
					}
				>
					{playButton ? (
						<Icon className="mx-16  text-yellow-700">warning</Icon>
					) : (
						<Icon className="mx-16 text-green-700">warning</Icon>
					)}
				</Tooltip>

				<Typography variant="h6" className="mx-16">
					34
				</Typography>
				<div className="flex flex-1 flex-col">
					<Typography variant="caption" component="span" className={classes.textXs}>
						wifi
					</Typography>
					<Typography variant="caption" component="span" className={classes.textXs}>
						remote
					</Typography>
					<Typography variant="caption" component="span" className={classes.textXs}>
						Call-in
					</Typography>
				</div>
			</div>
			<div className="flex justify-evenly w-1/5">
				<IconButton onClick={viewerJoined}>
					<Icon className="text-blue-200 border-blue">hearing</Icon>
				</IconButton>
				<IconButton>
					<Icon>swap_horizontal_circle</Icon>
				</IconButton>
			</div>
		</div>
	);
};

export default React.memo(SummaryLang);
