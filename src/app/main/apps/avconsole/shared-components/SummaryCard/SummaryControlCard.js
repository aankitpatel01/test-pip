import React, { useEffect } from 'react';
import { Paper, Typography, Divider } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import SummaryRoomLang from './SummaryRoomLang';
import * as Actions from '../../store/actions';

const SummaryControlCard = ({ rooms }) => {
	const dispatch = useDispatch();
	const topics = useSelector(({ avConsoleOrganiser }) => avConsoleOrganiser.avConsole.data.topics);

	console.log('Topics  => ', topics);
	const sessioncurrent = topics.find(tx => moment().isBetween(tx.recstartTime, tx.recEndTime));

	useEffect(() => {
		dispatch(Actions.setCurrentSession(sessioncurrent));
	});
	// console.log('Session Start Time => ', moment(sessioncurrent.startTime).format('ddd, D MMMM, h:mm a'));
	// console.log('Session EndTime => ', moment(sessioncurrent.endTime).format('ddd, D MMMM, h:mm a'));
	// console.log('Session recStartTime => ', moment(sessioncurrent.recStartTime).format('ddd, D MMMM, h:mm a'));
	// console.log('Session recEndTime => ', moment(sessioncurrent.recEndTime).format('ddd, D MMMMM, h:mm a'));

	return (
		<Paper>
			<Divider />
			<Typography component="h6" variant="h6" className="m-16">
				Summary
			</Typography>
			<Divider />
			<div className="p-16">
				<div className=" flex flex-row justify-center mx-24">
					<Typography className="w-1/5 uppercase">Stream</Typography>
					<Typography className="w-1/5 uppercase">Source</Typography>
					<Typography className="w-1/5 uppercase">Level</Typography>
					<Typography className="w-1/5 uppercase">Listener</Typography>
				</div>
				{rooms.length > 0 &&
					rooms.map(room => <SummaryRoomLang key={room.id} room={room} sessioncurrent={sessioncurrent} />)}
			</div>
		</Paper>
	);
};
export default SummaryControlCard;
