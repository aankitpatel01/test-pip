import React from 'react';
import io from 'socket.io-client';
import { IconButton, Icon, Typography } from '@material-ui/core';
// import RoomInfo from '../RoomInfo';
import AvManagerAudio from './AvManagerAudio';

// const defaultState = {
// 	endTime: '',
// 	eventId: '',
// 	interpreterId: '',
// 	mute: true,
// 	name: '',
// 	protocol: '',
// 	role: '',
// 	socketId: '',
// 	status: '',
// 	userAgent: ''
// };

// Let's do this 💪
const socket = io.connect('https://daastapanel.hashweb.in/');

const AvManagerInfo = ({ socketData }) => {
	console.log('Socket Mic => ', socketData.mute);
	// const [cardData, setCardData] = useState(defaultState);

	// useEffect(() => {
	// 	setCardData(socketData);
	// }, [setCardData]);
	const handleMicOff = () => {
		const data = {
			...socketData,
			mute: true
		};
		console.log('ORGANISER MIC OFF', socketData.socketId, socketData.mute, data.mute);
		socket.emit('org-disconnect-interpreter', data);
	};

	return (
		<div className="flex flex-col mx-24">
			<div className="flex flex-row justify-between">
				<div className="flex flex-row mx-24 my-16">
					<Icon>event_note</Icon>
					<Typography className="mx-16 uppercase">Room 1</Typography>/
					<Typography className="mx-16 uppercase">Session 2</Typography>/
					<Typography className="mx-16 uppercase">time</Typography>
				</div>
				<div className="flex justify-evenly">
					<IconButton onClick={handleMicOff}>
						{socketData.mute === true ? (
							<Icon className="text-blue-700 border-blue">power_settings_new</Icon>
						) : (
							<Icon className="text-red-700 border-blue">power_settings_new</Icon>
						)}
					</IconButton>
					<IconButton>
						<Icon>add</Icon>
					</IconButton>
				</div>
			</div>

			<AvManagerAudio />
		</div>
	);
};

export default React.memo(AvManagerInfo);
