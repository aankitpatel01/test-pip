import React, { useState } from 'react';
import {
	IconButton,
	Icon,
	Typography,
	makeStyles,
	DialogActions,
	DialogTitle,
	DialogContent,
	Button,
	ListItemIcon,
	ListItem,
	ListItemText,
	List
} from '@material-ui/core';
import clsx from 'clsx';
import * as FuseActions from 'app/store/actions';
import { useDispatch } from 'react-redux';

const useStyles = makeStyles(theme => ({
	bgBlack: {
		backgroundColor: '#000'
	},

	badgeButton: {
		left: '-40px'
	}
}));

// const ListDevice = props => {
// 	return (
// 		<ListItem button>
// 			{console.log('ListDevice', props)}
// 			<ListItemText primary={props.device} />
// 		</ListItem>
// 	);
// };

const AvManagerAudio = () => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const [selected, setSelected] = useState('');
	// Audio Devices
	let arrayDevice = [];

	// const audioFocus = useRef();

	async function getConnectedDevices(type) {
		const devices = await navigator.mediaDevices.enumerateDevices();
		arrayDevice = await devices.filter(device => device.kind === type);
		return arrayDevice;
	}

	getConnectedDevices('audioinput').then(device => {
		arrayDevice = JSON.parse(JSON.stringify(device));
		return arrayDevice;
	});

	console.log('arrayDevice =>', selected);
	// console.log('audioDevices => ', videoCameras);
	// videoCameras.then(devices => {
	// 	// console.log('arrayDevice', dataone);
	// 	const dataSetall = devices.map(device => device.label);
	// 	console.log('object', dataSetall);
	// 	arrayDevice = dataSetall;
	// });
	// const handleClick = data => {
	// 	console.log(data);
	// };

	return (
		<div className="relative flex flex-row items-center">
			<IconButton
				className={clsx(classes.badgeButton, 'absolute text-yellow-700')}
				onClick={() => {
					dispatch(
						FuseActions.openDialog({
							children: (
								<>
									<DialogTitle id="audio-devices-item">Audio Setting</DialogTitle>
									<DialogContent dividers className="p-12">
										<List>
											{arrayDevice.map(device => (
												<ListItem
													button
													key={device.deviceId}
													onClick={() => {
														setSelected(device.deviceId);
														// dispatch(FuseActions.closeDialog());
													}}
													className="focus:text-red"
												>
													<ListItemIcon>
														<Icon>mic</Icon>
													</ListItemIcon>
													<ListItemText primary={device.label} />
												</ListItem>
											))}
										</List>
									</DialogContent>
									<DialogActions>
										<Button
											color="primary"
											onClick={() => {
												dispatch(FuseActions.closeDialog());
											}}
										>
											Close
										</Button>
									</DialogActions>
								</>
							)
						})
					);
				}}
			>
				<Icon>blur_circular</Icon>
			</IconButton>
			<div className="flex flex-1 flex-row items-center">
				<div className={clsx(classes.bgBlack, 'p-12 flex w-2/5')}>
					<Typography className="justify-center uppercase">English</Typography>
				</div>
				<div className="flex flex-1 justify-center items-center">
					<Icon>highlight_off</Icon>
				</div>
				<div className={clsx(classes.bgBlack, 'p-12 flex w-2/5')}>
					<Typography className="justify-center uppercase">English</Typography>
				</div>

				<div className="flex justify-evenly">
					<IconButton>
						<Icon>swap_horizontal_circle</Icon>
					</IconButton>
					<IconButton>
						<Icon>highlight_off</Icon>
					</IconButton>
				</div>
			</div>
		</div>
	);
};

export default React.memo(AvManagerAudio);
