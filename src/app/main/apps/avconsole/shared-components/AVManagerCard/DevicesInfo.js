import React from 'react';
import { Typography, Icon, Tooltip } from '@material-ui/core';

const DevicesInfo = ({ socketData }) => {
	return (
		<div className="flex">
			<Typography variant="subtitle1" className="mx-16 text-grey-500">
				({socketData?.userAgent})
			</Typography>
			<Icon className="mx-8 text-grey-500">network_check</Icon>
			<Icon className="mx-8 text-grey-500">power</Icon>
			<Tooltip
				placement="top"
				title={
					<>
						<Typography className="uppercase text-light-green-500">Jitter: 0ms</Typography>
						<Typography className="uppercase text-light-green-500">Loss: 0%</Typography>
						<Typography className="uppercase text-yellow-700">UnKnown Network interface type</Typography>
					</>
				}
			>
				<Icon className="mx-8 text-yellow-700">settings_input_hdmi</Icon>
			</Tooltip>
		</div>
	);
};

export default React.memo(DevicesInfo);
