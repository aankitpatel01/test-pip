import React from 'react';
import { Paper, Typography, Divider, IconButton, makeStyles, Icon } from '@material-ui/core';
import clsx from 'clsx';
import AvManagerInfo from './AvManagerInfo';
import DevicesInfo from './DevicesInfo';

const useStyles = makeStyles(theme => ({
	bgBlack: {
		backgroundColor: '#121212'
	}
}));

const AvManagerControlCard = ({ interpreter, socket }) => {
	const classes = useStyles();

	return (
		<Paper>
			<Divider />
			<Typography component="h6" variant="h6" className="m-16">
				{interpreter?.role} Desk
			</Typography>
			<Divider />
			<div className="p-16 flex flex-col">
				<div className="flex items-center justify-between">
					<div className={clsx(classes.bgBlack, 'ml-24 py-8 flex flex-1 justify-between')}>
						<div className="flex mx-8">
							<Icon>settings_input_antenna</Icon>
							<Typography variant="subtitle1" className="mx-16 uppercase">
								{interpreter?.name}
							</Typography>
						</div>
						<DevicesInfo socketData={interpreter} />
					</div>

					<div className="flex mr-24 justify-evenly">
						<IconButton>
							<Icon className="transform rotate-180 borders-blue">open_in_browser</Icon>
						</IconButton>
						<IconButton>
							<Icon>open_in_browser</Icon>
						</IconButton>
					</div>
				</div>

				<AvManagerInfo socketData={interpreter} />
			</div>
		</Paper>
	);
};
export default React.memo(AvManagerControlCard);
