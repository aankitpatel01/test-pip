import Avatar from '@material-ui/core/Avatar';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import withReducer from 'app/store/withReducer';
import clsx from 'clsx';
// import keycode from 'keycode';
import React, { useRef } from 'react';
import {
	ListItem,
	List,
	ListItemAvatar,
	ListItemText,
	ListItemSecondaryAction,
	Divider,
	DialogTitle,
	DialogContent,
	DialogActions,
	Button
} from '@material-ui/core';
import { useDispatch } from 'react-redux';
import * as FuseActions from 'app/store/actions';
import * as authActions from 'app/auth/store/actions';
import Chat from './Chat';
// import ContactList from './ContactList';
// import * as Actions from './store/actions';
import reducer from './store/reducers';

const useStyles = makeStyles(theme => ({
	root: {
		width: 70,
		maxWidth: 70,
		minWidth: 70,
		[theme.breakpoints.down('md')]: {
			width: 0,
			maxWidth: 0,
			minWidth: 0
		}
	},
	panel: {
		position: 'absolute',
		width: 360,
		backgroundColor: theme.palette.background.paper,
		boxShadow: theme.shadows[3],
		top: 0,
		height: '100%',
		minHeight: '100%',
		bottom: 0,
		right: 0,
		margin: 0,
		zIndex: 1000,
		transform: 'translate3d(290px,0,0)',
		overflow: 'hidden',
		[theme.breakpoints.down('md')]: {
			transform: 'translate3d(360px,0,0)',
			boxShadow: 'none',
			'&.opened': {
				boxShadow: theme.shadows[5]
			}
		},
		transition: theme.transitions.create(['transform'], {
			easing: theme.transitions.easing.easeInOut,
			duration: theme.transitions.duration.standard
		}),
		'&.opened': {
			transform: 'translateX(0)'
		}
	}
}));

function ChatPanel(props) {
	const dispatch = useDispatch();
	const { eventId, EventData, userName } = props;
	// const contacts = useSelector(({ chatPanel }) => chatPanel.contacts.entities);
	// const selectedContactId = useSelector(({ chatPanel }) => chatPanel.contacts.selectedContactId);
	// const state = useSelector(({ chatPanel }) => chatPanel.state);
	const ref = useRef();

	const classes = useStyles(props);
	// const selectedContact = contacts.find(_contact => _contact.id === selectedContactId);

	// const handleDocumentKeyDown = useCallback(
	// 	event => {
	// 		if (keycode(event) === 'esc') {
	// 			dispatch(Actions.closeChatPanel());
	// 		}
	// 	},
	// 	[dispatch]
	// );

	// useEffect(() => {
	// 	// dispatch(Actions.getUserData());
	// 	// dispatch(Actions.getContacts());
	// 	// return () => {
	// 	// 	document.removeEventListener('keydown', handleDocumentKeyDown);
	// 	// };
	// }, [dispatch]);

	// useEffect(() => {
	// 	if (state) {
	// 		document.addEventListener('keydown', handleDocumentKeyDown);
	// 	} else {
	// 		document.removeEventListener('keydown', handleDocumentKeyDown);
	// 	}
	// }, [handleDocumentKeyDown, state]);

	/**
	 * Click Away Listener
	 */
	// useEffect(() => {
	// 	console.log(window.navigator.appVersion);
	// 	function handleDocumentClick(ev) {
	// 		if (ref.current && !ref.current.contains(ev.target)) {
	// 			dispatch(Actions.closeChatPanel());
	// 		}
	// 	}

	// 	if (state) {
	// 		document.addEventListener('click', handleDocumentClick, true);
	// 	} else {
	// 		document.removeEventListener('click', handleDocumentClick, true);
	// 	}

	// 	return () => {
	// 		document.removeEventListener('click', handleDocumentClick);
	// 	};
	// }, [state, dispatch]);

	return (
		<div className={classes.root}>
			<div className={clsx(classes.panel, { opened: true }, 'flex flex-col')} ref={ref}>
				<Typography component="h6" variant="h6" className="mx-16">
					Audio Stream Preview
				</Typography>
				<Divider />
				<List>
					<ListItem>
						<ListItemAvatar>
							<Avatar>
								<Icon className="text-red">volume_up</Icon>
							</Avatar>
						</ListItemAvatar>
						<ListItemText className="text-red" primary="Language Stream" />
					</ListItem>
				</List>
				<Divider />

				<List>
					<ListItem>
						<ListItemAvatar>
							<Avatar>
								<Icon className="text-white">account_circle</Icon>
							</Avatar>
						</ListItemAvatar>
						<ListItemText primary={userName || ''} />
						<ListItemSecondaryAction>
							<IconButton
								edge="end"
								aria-label="Logout"
								onClick={() => {
									dispatch(
										FuseActions.openDialog({
											children: (
												<>
													<DialogTitle id="audio-devices-item">Logout</DialogTitle>
													<DialogContent dividers className="p-12">
														<Typography variant="h6" className="p-24">
															Are you sure you want to logout?
														</Typography>
													</DialogContent>
													<DialogActions>
														<Button
															color="primary"
															onClick={() => {
																dispatch(FuseActions.closeDialog());
															}}
														>
															Cancel
														</Button>
														<Button
															color="primary"
															onClick={() => {
																dispatch(authActions.logoutUser());
																dispatch(FuseActions.closeDialog());
															}}
														>
															Logout
														</Button>
													</DialogActions>
												</>
											)
										})
									);
								}}
							>
								<Icon>eject</Icon>
							</IconButton>
						</ListItemSecondaryAction>
					</ListItem>

					<ListItem>
						<ListItemAvatar>
							<Avatar>
								<Icon className="text-white">event</Icon>
							</Avatar>
						</ListItemAvatar>
						<ListItemText primary={EventData.eventName || ''} />
						<ListItemSecondaryAction>
							<IconButton edge="end" aria-label="Switch">
								<Icon>swap_horizontal</Icon>
							</IconButton>
						</ListItemSecondaryAction>
					</ListItem>
				</List>
				<Divider />

				<Paper className="flex flex-1 flex-row min-h-px">
					{/* <ContactList className="flex flex-shrink-0" /> */}
					<Chat className="flex flex-1 z-10" eventId={eventId} />
				</Paper>
			</div>
		</div>
	);
}

export default withReducer('chatPanel', reducer)(React.memo(ChatPanel));
