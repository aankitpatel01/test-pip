/**
 * @author Amir Sanni <amirsanni@gmail.com>
 * @date 6th January, 2020
 */
import io from 'socket.io-client';
import h from './helpers';

window.addEventListener('load', () => {
	// const room = h.getQString(location.href, 'room');
	const username = sessionStorage.getItem('username');

	if (!username) {
		// 	document.querySelector('#room-create').attributes.removeNamedItem('hidden');
		// } else if (!username) {
		// 	document.querySelector('#username-set').attributes.removeNamedItem('hidden');
		// } else {
		// 	const commElem = document.getElementsByClassName('room-comm');

		// 	for (let i = 0; i < commElem.length; i++) {
		// 		commElem[i].attributes.removeNamedItem('hidden');
		// 	}

		const pc = [];

		const socket = io('http://69.16.254.127:4006');

		let socketId = '';
		let myStream = '';
		const screen = '';
		const recordedStream = [];
		// let mediaRecorder = '';

		// Get user video by default
		// getAndSetUserStream();

		socket.on('connect', () => {
			// set socketId
			socketId = socket.io.engine.id;

			console.log('pc', pc);
			console.log('socket', socket);
			console.log('socket ID', socketId);
			console.log('myStream', myStream);
			console.log('recordedStream', recordedStream);

			// socket.emit('subscribe', {
			// 	room,
			// 	socketId
			// });

			socket.on('ice candidates', async data => {
				// data.candidate ? await pc[data.sender].addIceCandidate(new RTCIceCandidate(data.candidate)) : '';
			});

			socket.on('sdp', async data => {
				if (data.description.type === 'offer') {
					// data.description
					// 	? await pc[data.sender].setRemoteDescription(new RTCSessionDescription(data.description))
					// 	: '';

					h.getUserFullMedia()
						.then(async stream => {
							if (!document.getElementById('local').srcObject) {
								h.setLocalStream(stream);
							}

							// save my stream
							myStream = stream;
							console.log('myStream', myStream);

							stream.getTracks().forEach(track => {
								pc[data.sender].addTrack(track, stream);
							});

							const answer = await pc[data.sender].createAnswer();

							await pc[data.sender].setLocalDescription(answer);

							socket.emit('sdp', {
								description: pc[data.sender].localDescription,
								to: data.sender,
								sender: socketId
							});
						})
						.catch(e => {
							console.error(e);
						});
				} else if (data.description.type === 'answer') {
					await pc[data.sender].setRemoteDescription(new RTCSessionDescription(data.description));
				}
			});

			socket.on('chat', data => {
				console.log('Chat', data);
				h.addChat(data, 'remote');
			});
		});

		// function getAndSetUserStream() {
		// 	h.getUserFullMedia()
		// 		.then(stream => {
		// 			// save my stream
		// 			myStream = stream;

		// 			h.setLocalStream(stream);
		// 		})
		// 		.catch(e => {
		// 			console.error(`stream error: ${e}`);
		// 		});
		// }

		// function sendMsg(msg) {
		// 	const data = {
		// 		room,
		// 		msg,
		// 		sender: username
		// 	};

		// 	// emit chat message
		// 	socket.emit('chat', data);

		// 	// add localchat
		// 	h.addChat(data, 'local');
		// }

		// function shareScreen() {
		// 	h.shareScreen()
		// 		.then(stream => {
		// 			h.toggleShareIcons(true);

		// 			// disable the video toggle btns while sharing screen. This is to ensure clicking on the btn does not interfere with the screen sharing
		// 			// It will be enabled was user stopped sharing screen
		// 			h.toggleVideoBtnDisabled(true);

		// 			// save my screen stream
		// 			screen = stream;

		// 			// share the new stream with all partners
		// 			broadcastNewTracks(stream, 'video', false);

		// 			// When the stop sharing button shown by the browser is clicked
		// 			screen.getVideoTracks()[0].addEventListener('ended', () => {
		// 				stopSharingScreen();
		// 			});
		// 		})
		// 		.catch(e => {
		// 			console.error(e);
		// 		});
		// }

		// function stopSharingScreen() {
		// 	// enable video toggle btn
		// 	h.toggleVideoBtnDisabled(false);

		// 	return new Promise((res, rej) => {
		// 		screen.getTracks().length ? screen.getTracks().forEach(track => track.stop()) : '';

		// 		res();
		// 	})
		// 		.then(() => {
		// 			h.toggleShareIcons(false);
		// 			broadcastNewTracks(myStream, 'video');
		// 		})
		// 		.catch(e => {
		// 			console.error(e);
		// 		});
		// }

		// function broadcastNewTracks(stream, type, mirrorMode = true) {
		// 	h.setLocalStream(stream, mirrorMode);

		// 	const track = type === 'audio' ? stream.getAudioTracks()[0] : stream.getVideoTracks()[0];

		// 	for (const p in pc) {
		// 		const pName = pc[p];

		// 		if (typeof pc[pName] === 'object') {
		// 			h.replaceTrack(track, pc[pName]);
		// 		}
		// 	}
		// }

		// function toggleRecordingIcons(isRecording) {
		// 	const e = document.getElementById('record');

		// 	if (isRecording) {
		// 		e.setAttribute('title', 'Stop recording');
		// 		e.children[0].classList.add('text-danger');
		// 		e.children[0].classList.remove('text-white');
		// 	} else {
		// 		e.setAttribute('title', 'Record');
		// 		e.children[0].classList.add('text-white');
		// 		e.children[0].classList.remove('text-danger');
		// 	}
		// }

		// function startRecording(stream) {
		// 	mediaRecorder = new MediaRecorder(stream, {
		// 		mimeType: 'video/webm;codecs=vp9'
		// 	});

		// 	mediaRecorder.start(1000);
		// 	toggleRecordingIcons(true);

		// 	mediaRecorder.ondataavailable = function (e) {
		// 		recordedStream.push(e.data);
		// 	};

		// 	mediaRecorder.onstop = function () {
		// 		toggleRecordingIcons(false);

		// 		h.saveRecordedStream(recordedStream, username);

		// 		setTimeout(() => {
		// 			recordedStream = [];
		// 		}, 3000);
		// 	};

		// 	mediaRecorder.onerror = function (e) {
		// 		console.error(e);
		// 	};
		// }

		// Chat textarea
		// document.getElementById('chat-input').addEventListener('keypress', e => {
		// 	if (e.which === 13 && e.target.value.trim()) {
		// 		e.preventDefault();

		// 		sendMsg(e.target.value);

		// 		setTimeout(() => {
		// 			e.target.value = '';
		// 		}, 50);
		// 	}
		// });

		// When the video icon is clicked
		// document.getElementById('toggle-video').addEventListener('click', e => {
		// 	e.preventDefault();

		// 	const elem = document.getElementById('toggle-video');

		// 	if (myStream.getVideoTracks()[0].enabled) {
		// 		e.target.classList.remove('fa-video');
		// 		e.target.classList.add('fa-video-slash');
		// 		elem.setAttribute('title', 'Show Video');

		// 		myStream.getVideoTracks()[0].enabled = false;
		// 	} else {
		// 		e.target.classList.remove('fa-video-slash');
		// 		e.target.classList.add('fa-video');
		// 		elem.setAttribute('title', 'Hide Video');

		// 		myStream.getVideoTracks()[0].enabled = true;
		// 	}

		// 	// broadcastNewTracks(myStream, 'video');
		// });

		// When the mute icon is clicked
		// document.getElementById('toggle-mute').addEventListener('click', e => {
		// 	e.preventDefault();

		// 	const elem = document.getElementById('toggle-mute');

		// 	if (myStream.getAudioTracks()[0].enabled) {
		// 		e.target.classList.remove('fa-microphone-alt');
		// 		e.target.classList.add('fa-microphone-alt-slash');
		// 		elem.setAttribute('title', 'Unmute');

		// 		myStream.getAudioTracks()[0].enabled = false;
		// 	} else {
		// 		e.target.classList.remove('fa-microphone-alt-slash');
		// 		e.target.classList.add('fa-microphone-alt');
		// 		elem.setAttribute('title', 'Mute');

		// 		myStream.getAudioTracks()[0].enabled = true;
		// 	}

		// 	// broadcastNewTracks(myStream, 'audio');
		// });

		// When user clicks the 'Share screen' button
		// document.getElementById('share-screen').addEventListener('click', e => {
		// 	e.preventDefault();

		// 	if (screen && screen.getVideoTracks().length && screen.getVideoTracks()[0].readyState !== 'ended') {
		// 		stopSharingScreen();
		// 	} else {
		// 		shareScreen();
		// 	}
		// });

		// When record button is clicked
		// document.getElementById('record').addEventListener('click', e => {
		// 	/**
		// 	 * Ask user what they want to record.
		// 	 * Get the stream based on selection and start recording
		// 	 */
		// 	if (!mediaRecorder || mediaRecorder.state === 'inactive') {
		// 		h.toggleModal('recording-options-modal', true);
		// 	} else if (mediaRecorder.state === 'paused') {
		// 		mediaRecorder.resume();
		// 	} else if (mediaRecorder.state === 'recording') {
		// 		mediaRecorder.stop();
		// 	}
		// });

		// When user choose to record screen
		// document.getElementById('record-screen').addEventListener('click', () => {
		// 	h.toggleModal('recording-options-modal', false);

		// 	if (screen && screen.getVideoTracks().length) {
		// 		startRecording(screen);
		// 	} else {
		// 		h.shareScreen()
		// 			.then(screenStream => {
		// 				startRecording(screenStream);
		// 			})
		// 			.catch(() => {});
		// 	}
		// });

		// When user choose to record own video
		// document.getElementById('record-video').addEventListener('click', () => {
		// 	h.toggleModal('recording-options-modal', false);

		// 	if (myStream && myStream.getTracks().length) {
		// 		startRecording(myStream);
		// 	} else {
		// 		h.getUserFullMedia()
		// 			.then(videoStream => {
		// 				startRecording(videoStream);
		// 			})
		// 			.catch(() => {});
		// 	}
		// });
	}
});
