import React from 'react';
import { Typography, Icon } from '@material-ui/core';

const RoomInfo = ({ roomdata }) => {
	console.log('Room nfo ', roomdata);
	return (
		<div className="flex flex-row mx-24 my-16">
			<Icon>event_note</Icon>
			<Typography className="mx-16 uppercase">Room 1</Typography>/
			<Typography className="mx-16 uppercase">Session 34</Typography>/
			<Typography className="mx-16 uppercase">time</Typography>
		</div>
	);
};

export default RoomInfo;
