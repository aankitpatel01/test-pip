import React from 'react';
import { authRoles } from 'app/auth';

const AvConsoleInterpreterConfig = {
	settings: {
		layout: {
			config: {
				navbar: {
					display: false
				},
				toolbar: {
					display: false
				},
				footer: {
					display: false
				},
				leftSidePanel: {
					display: false
				},
				rightSidePanel: {
					display: false
				}
			}
		},
		theme: {
			main: 'mainThemeDark'
		}
	},
	auth: authRoles.interpreter,
	routes: [
		{
			isExact: true,
			path: '/avconsole/interpreter/:email/:eventId/:roomId/:srcStreamId',
			component: React.lazy(() => import('./AvConsoleInterpreter'))
		}
	]
};

export default AvConsoleInterpreterConfig;
