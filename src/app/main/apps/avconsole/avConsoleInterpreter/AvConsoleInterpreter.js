/* eslint-disable jsx-a11y/media-has-caption */
import React, { useEffect, useRef } from 'react';
import { Divider, Icon, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import io from 'socket.io-client';
import FuseLoading from '@fuse/core/FuseLoading';
import withReducer from 'app/store/withReducer';
import Chat from '../shared-components/chatPanel/Chat';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import * as ChatActions from '../shared-components/chatPanel/store/actions';
import ControlBox from './ControlBox';

const useStyles = makeStyles(theme => ({
	inputButtons: {
		left: '-90px'
	},
	outputButton: {
		right: '-90px'
	},
	playIcon: {
		fontSize: '7.6rem'
	}
}));

// Let's do this 💪
const socket = io.connect('https://daastapanel.hashweb.in/');

const AvConsoleInterpreter = props => {
	const dispatch = useDispatch();
	const refControlBox = useRef();

	const classes = useStyles();
	const { match } = props;
	const { eventId, roomId, email } = match.params;
	const OSName = navigator.platform;

	// if (navigator.appVersion.indexOf('Win') !== -1) OSName = 'Windows 10';
	// if (navigator.appVersion.indexOf('Mac') !== -1) OSName = 'MacOS';
	// if (navigator.appVersion.indexOf('X11') !== -1) OSName = 'UNIX';
	// if (navigator.appVersion.indexOf('Linux') !== -1) OSName = 'Linux';

	// // Let's do this 💪
	// const socket = io.connect('https://daastapanel.hashweb.in/');

	// const [micHandler, setMicHandler] = useState(true);

	// Prevent Reload Window
	// window.onbeforeunload = event => {
	// 	return 'Do you really want to close ?';
	// };

	const avConsoleData = useSelector(({ avConsoleInterpreter }) => avConsoleInterpreter.avConsole.data);
	const userData = useSelector(({ auth }) => auth.user.data);

	const JoinedData = {
		eventId,
		socketId: socket.id,
		name: userData.displayName,
		interpreterId: userData.id,
		role: 'Interpreter',
		protocol: 'webRTC',
		userAgent: OSName,
		status: 'active',
		mute: false,
		data: avConsoleData,
		startTime: new Date().toISOString()
	};
	const leaveData = {
		eventId,
		socketId: socket.id,
		name: userData.displayName,
		interpreterId: userData.id,
		role: 'Interpreter',
		protocol: 'webRTC',
		userAgent: OSName,
		status: 'deactived',
		mute: true,
		data: avConsoleData
		// endTime: new Date().toISOString()
	};

	useEffect(() => {
		dispatch(Actions.getAvConsoleEventInterpreter({ eventId, email }));
		dispatch(ChatActions.getChat(eventId));
	}, [dispatch, eventId, email]);

	useEffect(() => {
		if (avConsoleData) {
			console.log('Socket Run !!', socket.id);
			socket.emit('newUserJoinRoom', eventId);
			socket.emit('interpreter joined', { ...JoinedData, startTime: new Date().toISOString() });
			console.log('JoinedData', JoinedData);
		}

		return () => {
			console.log('interpreter disconnect Yeah!!! ', socket.id);
			// const data = {
			// 	...leaveData,
			// 	endTime: new Date().toISOString()
			// };
			console.log('Leave Data => ', leaveData);
			socket.emit('interpreter leave', leaveData);
			// dispatch(Actions.cleanUpAvconsole());
		};
		// react-hooks/exhaustive-deps
	}, [avConsoleData, eventId]);

	socket.on('org-disconnect-interpreter-received', data => {
		console.log('refControlBox', data);
		refControlBox.current.handleStartInterpreter();
	});

	if (!avConsoleData) {
		return <FuseLoading />;
	}

	// const {
	// 	// bidirectional,
	// 	// dstLang,
	// 	// dstStreamId,
	// 	// dstTitle,
	// 	// interpretorId,
	// 	// roomLanguage,
	// 	// roomName,
	// 	srcLang,
	// 	srcStreamId
	// 	// srcTitle,
	// 	// status
	// } = avConsoleData;

	return (
		<div className="flex flex-col h-screen">
			<div className="flex flex-1">
				<div className="flex p-16 m-16 justify-center items-center w-3/4 bg-black">
					<Icon className={classes.playIcon}>play_circle_outline</Icon>
				</div>

				<div className="w-1/4 py-16 pr-16">
					<Chat className="flex flex-1 z-10" eventId={eventId} />
				</div>
			</div>
			<Divider />

			<div className="flex flex-col">
				<ControlBox
					avConsoleData={avConsoleData}
					userData={userData}
					eventId={eventId}
					roomId={roomId}
					JoinedData={JoinedData}
					leaveData={leaveData}
					ref={refControlBox}
				/>
			</div>
		</div>
	);
};

export default withReducer('avConsoleInterpreter', reducer)(React.memo(AvConsoleInterpreter));
