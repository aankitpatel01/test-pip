import React, { useState } from 'react';
import {
	Button,
	DialogActions,
	DialogContent,
	DialogTitle,
	Icon,
	IconButton,
	List,
	ListItem,
	ListItemIcon,
	ListItemText,
	makeStyles,
	Typography
} from '@material-ui/core';
import clsx from 'clsx';
import { useDispatch, useSelector } from 'react-redux';
import * as FuseActions from 'app/store/actions';
import * as Actions from '../store/actions';

const useStyles = makeStyles(theme => ({
	inputButtons: {
		left: '-90px'
	},
	outputButton: {
		right: '-90px'
	},
	playIcon: {
		fontSize: '7.6rem'
	},
	highlight: {
		backgroundColor: '#03a9f4',
		color: '#fff'
	}
}));

const AudioContainerInput = ({ avConsoleData }) => {
	const dispatch = useDispatch();
	const selectSrcLang = useSelector(({ avConsoleInterpreter }) => avConsoleInterpreter.avConsole.srcLang);
	const selectDstLang = useSelector(({ avConsoleInterpreter }) => avConsoleInterpreter.avConsole.dstLang);
	const classes = useStyles();
	const [selectedAudio, setSelectedAudio] = useState('');

	// Audio Devices
	let arrayDevice = [];

	const {
		// bidirectional,
		dstLang,
		dstStreamId,
		// dstTitle,
		// interpretorId,
		// roomLanguage,
		// roomName,
		srcLang,
		srcStreamId
		// srcTitle,
		// status
	} = avConsoleData;

	async function getConnectedDevices(type) {
		const devices = await navigator.mediaDevices.enumerateDevices();
		const filterDevices = devices.filter(device => device.kind === type);
		return filterDevices;
	}

	getConnectedDevices('audiooutput').then(device => {
		arrayDevice = JSON.parse(JSON.stringify(device));
		return arrayDevice;
	});

	const handleSecStreamID = () => {
		dispatch(Actions.selectSrcLang(srcStreamId));
		dispatch(Actions.selectDstLang(dstStreamId));
	};

	const handleDstStreamID = () => {
		dispatch(Actions.selectSrcLang(dstStreamId));
		dispatch(Actions.selectDstLang(srcStreamId));
	};

	console.log('Laguage Button ID =>', selectSrcLang, selectDstLang);

	return (
		<div className="w-1/5 flex flex-col relative">
			<div className={clsx(classes.inputButtons, 'flex h-full flex-col justify-evenly absolute')}>
				<IconButton
					className="bg-black hover:bg-grey-A400 text-light-blue font-semibold  border border-gray-400 rounded shadow-md"
					onClick={handleSecStreamID}
				>
					<Typography className="flex justify-center uppercase">1</Typography>
				</IconButton>
				<IconButton
					className="bg-black hover:bg-grey-A400 text-light-blue font-semibold  border border-gray-400 rounded shadow"
					onClick={handleDstStreamID}
				>
					<Typography className="flex justify-center uppercase">2</Typography>
				</IconButton>
				<IconButton
					className="bg-black hover:bg-grey-A400 text-light-blue font-semibold  border border-gray-400 rounded shadow"
					onClick={() => {
						dispatch(
							FuseActions.openDialog({
								children: (
									<>
										<DialogTitle id="audio-devices-item">Audio Setting</DialogTitle>
										<DialogContent dividers className="p-12">
											<List>
												{arrayDevice.map(device => (
													<ListItem
														button
														key={device.deviceId}
														onClick={() => {
															setSelectedAudio(device.label);
															// dispatch(FuseActions.closeDialog());
														}}
														className="focus:text-red"
													>
														<ListItemIcon>
															<Icon>speaker</Icon>
														</ListItemIcon>
														<ListItemText primary={device.label} />
													</ListItem>
												))}
											</List>
										</DialogContent>
										<DialogActions>
											<Button
												color="primary"
												onClick={() => {
													dispatch(FuseActions.closeDialog());
												}}
											>
												Close
											</Button>
										</DialogActions>
									</>
								)
							})
						);
					}}
				>
					<Icon>volume_up</Icon>
				</IconButton>
			</div>

			<div
				className={
					selectSrcLang === srcStreamId
						? clsx(
								classes.highlight,
								'p-8 m-8 flex flex-row justify-between uppercase border border-grey-600 rounded'
						  )
						: 'p-8 m-8 flex flex-row justify-between uppercase border border-grey-600 rounded bg-gray text-grey-900'
				}
			>
				<Typography variant="subtitle2" className="uppercase">
					{srcLang}
				</Typography>
				<Icon className="focus:shadow-outline">close</Icon>
			</div>
			<div
				className={
					selectDstLang === srcStreamId
						? clsx(
								classes.highlight,
								'p-8 m-8 flex flex-row justify-between uppercase border border-grey-600 rounded'
						  )
						: 'p-8 m-8 flex flex-row justify-between uppercase border border-grey-600 rounded bg-gray text-grey-900'
				}
			>
				<Typography variant="subtitle2" className="uppercase">
					{dstLang}
				</Typography>
				<Icon className="focus:shadow-outline">close</Icon>
			</div>
			<div className="p-8 m-8 flex flex-row justify-between uppercase border border-grey-600 rounded bg-gray text-grey-900">
				{selectedAudio || 'Default Audio Device'}
			</div>
		</div>
	);
};

export default React.memo(AudioContainerInput);
