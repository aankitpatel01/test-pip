/* eslint-disable jsx-a11y/media-has-caption */
import React, { useRef, useState, useEffect, forwardRef, useImperativeHandle } from 'react';
import { Button, Divider, Typography, DialogTitle, DialogContent, DialogActions } from '@material-ui/core';
import clsx from 'clsx';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { useDispatch, useSelector } from 'react-redux';
import io from 'socket.io-client';
import * as FuseActions from 'app/store/actions';
import * as authActions from 'app/auth/store/actions';
import moment from 'moment';
import DevicesInfo from '../shared-components/AVManagerCard/DevicesInfo';
import AudioContainerInput from './AudioContainerInput';
import AudioContainerOutput from './AudioContainerOutput';

let mediaRecorder = [];

const ControlBox = ({ avConsoleData, userData, eventId, roomId, JoinedData, leaveData }, ref) => {
	const dispatch = useDispatch();
	const [sessID, setSessID] = useState(null);

	const { agendaData } = useSelector(({ avConsoleInterpreter }) => avConsoleInterpreter.avConsole.data);
	const langId = useSelector(({ avConsoleInterpreter }) => avConsoleInterpreter.avConsole.dstLang);

	useEffect(() => {
		if (agendaData.length > 0) {
			const test = agendaData.find(item => moment().isBetween(item.recStartTime, item.recEndTime));
			setSessID(test);
		}
	}, [agendaData]);

	console.log('Control Box Session =>', sessID);

	// const {
	// 	// bidirectional,
	// 	// dstLang,
	// 	dstStreamId,
	// 	// dstTitle,
	// 	// interpretorId,
	// 	// roomLanguage,
	// 	// roomName,
	// 	// srcLang,
	// 	srcStreamId
	// 	// srcTitle,
	// 	// status
	// } = avConsoleData;

	// Let's do this 💪
	const socket = io.connect('https://daastapanel.hashweb.in/');

	const [micHandler, setMicHandler] = useState(true);

	// let interpreterAudio = document.getElementById('interpreterAudio');

	// const JoinedData = {
	// 	eventId,
	// 	socketId: socket.id,
	// 	name: userData.displayName,
	// 	interpreterId: userData.id,
	// 	role: 'Interpreter',
	// 	protocol: 'webRTC',
	// 	userAgent: OSName,
	// 	data: avConsoleData,
	// 	startTime: new Date().toISOString()
	// };
	// const leaveData = {
	// 	eventId,
	// 	socketId: socket.id,
	// 	name: userData.displayName,
	// 	interpreterId: userData.id,
	// 	role: 'Interpreter',
	// 	protocol: 'webRTC',
	// 	userAgent: OSName,
	// 	data: avConsoleData,
	// 	startTime: new Date().toISOString()
	// };

	// useEffect(() => {
	// 	if (micHandler) {
	// 		console.log('Clean UP Control Box  1111111');
	// 		socket.disconnect();
	// 	}
	// }, [micHandler]);

	// variables
	let user;
	const rtcPeerConnections = {};
	const userAudio = useRef();

	// constants
	const iceServers = {
		iceServers: [{ urls: 'stun:stun.services.mozilla.com' }, { urls: 'stun:stun.l.google.com:19302' }]
	};
	const streamConstraints = {
		audio: { echoCancellation: true, noiseSuppression: true }
	};

	const handleStartInterpreter = () => {
		user = {
			room: `eventRTC/${eventId}/${roomId}/${langId}/${sessID?.id}`,
			JoinedData
		};

		console.log('RTC Url', user.room);
		if (langId !== null && sessID !== null) {
			navigator.mediaDevices
				.getUserMedia(streamConstraints)
				.then(stream => {
					userAudio.current.srcObject = stream;

					startRecording(stream);
					setMicHandler(!micHandler);
					// userAudio.current.play();
					socket.emit('register as interpreter', user);
					socket.emit('interpreter-mic-on', JoinedData);
				})
				.catch(err => {
					console.log('An error ocurred when accessing media devices', err);
				});
		} else {
			dispatch(
				FuseActions.openDialog({
					children: (
						<>
							<DialogTitle id="audio-lang-item">Stream Not Selected</DialogTitle>
							<DialogContent dividers className="p-12">
								<Typography variant="h6" className="p-24">
									Select a Language to Stream.
								</Typography>
							</DialogContent>
							<DialogActions>
								<Button
									color="primary"
									onClick={() => {
										dispatch(FuseActions.closeDialog());
									}}
								>
									ok
								</Button>
								{/* <Button
									color="primary"
									onClick={() => {
										dispatch(authActions.logoutUser());
										dispatch(FuseActions.closeDialog());
									}}
								>
									Logout
								</Button> */}
							</DialogActions>
						</>
					)
				})
			);
		}
	};

	const handleStopInterpreter = () => {
		const data = {
			room: `eventRTC/${eventId}/${roomId}/`,
			leaveData
		};

		mediaRecorder.stop();
		userAudio.current.pause();
		// interpreterAudio.pause();
		userAudio.current.currentTime = 0;
		console.log('RTC Url Stop', userAudio.current.currentTime, data.room, userAudio.current);
		// Stop recording event
		setMicHandler(!micHandler);
		// mute mic
		userAudio.current.srcObject.getAudioTracks()[0].enabled = false;
		socket.emit('interpreter disconnect', leaveData);
		// socket.emit('interpreter-mic-disconnect', leaveData);
	};

	const saveRecordedStream = (stream, filename) => {
		const blob = new Blob(stream, { type: 'video/webm' });
		const file = new File([blob], `${filename}-${moment().unix()}-record.webm`);
		const fileURL = URL.createObjectURL(file);
		console.log(fileURL);
	};

	function startRecording(stream) {
		let recordedStream = [];
		mediaRecorder = new MediaRecorder(stream, {
			mimeType: 'video/webm;codecs=vp9'
		});

		mediaRecorder.start(1000);

		mediaRecorder.ondataavailable = e => {
			if (e.data && e.data.size > 0) {
				// console.log('eventt what ', e.data);
				// RecordStream Send Chunk
				const recData = { eventId, interpreterId: userData.id, startTime: new Date().toISOString() };
				socket.emit('RecordStream', { chunk: e.data, recData });
				recordedStream.push(e.data);
			}
		};

		mediaRecorder.onstop = () => {
			console.log('streams.onStop');
			saveRecordedStream(recordedStream, eventId);

			setTimeout(() => {
				console.log('Clean Up Chunk');
				recordedStream = [];
			}, 2000);
		};

		mediaRecorder.onerror = e => {
			console.error(e);
		};
	}

	// const viewerJoined = () => {
	// 	user = {
	// 		room: 'event/room1',
	// 		name: 'Viewer'
	// 	};
	// 	// divSelectRoom.style = 'display: none;';
	// 	// divConsultingRoom.style = 'display: block;';
	// 	socket.emit('register as viewer', user);
	// };

	// message handlers
	socket.on('new viewer', viewer => {
		console.log('NewViewer', viewer);
		rtcPeerConnections[viewer.id] = new RTCPeerConnection(iceServers);

		const stream = userAudio.current.srcObject;
		stream.getTracks().forEach(track => rtcPeerConnections[viewer.id].addTrack(track, stream));

		rtcPeerConnections[viewer.id].onicecandidate = event => {
			if (event.candidate) {
				console.log('sending ice candidate Viewer');
				socket.emit('candidate', viewer.id, {
					type: 'candidate',
					label: event.candidate.sdpMLineIndex,
					id: event.candidate.sdpMid,
					candidate: event.candidate.candidate
				});
			}
		};

		rtcPeerConnections[viewer.id]
			.createOffer()
			.then(sessionDescription => {
				rtcPeerConnections[viewer.id].setLocalDescription(sessionDescription);
				socket.emit('offer', viewer.id, {
					type: 'offer',
					sdp: sessionDescription,
					broadcaster: user
				});
			})
			.catch(error => {
				console.log(error);
			});

		// let li = document.createElement('li');
		// li.innerText = viewer.name + ' has joined';
		// viewers.appendChild(li);
	});

	socket.on('candidate', (id, event) => {
		const candidate = new RTCIceCandidate({
			sdpMLineIndex: event.label,
			candidate: event.candidate
		});
		rtcPeerConnections[id].addIceCandidate(candidate);
	});

	socket.on('offer', (broadcaster, sdp) => {
		// broadcasterName.innerText = `${broadcaster.name}is broadcasting...`;

		rtcPeerConnections[broadcaster.id] = new RTCPeerConnection(iceServers);

		rtcPeerConnections[broadcaster.id].setRemoteDescription(sdp);

		rtcPeerConnections[broadcaster.id].createAnswer().then(sessionDescription => {
			rtcPeerConnections[broadcaster.id].setLocalDescription(sessionDescription);
			socket.emit('answer', {
				type: 'answer',
				sdp: sessionDescription,
				room: user.room
			});
		});

		rtcPeerConnections[broadcaster.id].ontrack = event => {
			// eslint-disable-next-line prefer-destructuring
			userAudio.current.srcObject = event.streams[0];
		};

		rtcPeerConnections[broadcaster.id].onicecandidate = event => {
			if (event.candidate) {
				console.log('sending ice candidate Offer !!');
				socket.emit('candidate', broadcaster.id, {
					type: 'candidate',
					label: event.candidate.sdpMLineIndex,
					id: event.candidate.sdpMid,
					candidate: event.candidate.candidate
				});
			}
		};
	});

	socket.on('answer', (viewerId, event) => {
		rtcPeerConnections[viewerId].setRemoteDescription(new RTCSessionDescription(event));
	});

	useImperativeHandle(ref, () => ({
		handleStartInterpreter,
		handleStopInterpreter
	}));

	return (
		<>
			<div className="flex justify-center m-16">
				<div className="flex w-5/6 bg-black">
					<AudioContainerInput avConsoleData={avConsoleData} />

					<div className="m-8 w-3/5 flex flex-col justify-between">
						<div className="p-16 flex flex-col border border-grey-600 rounded">
							<audio
								id="interpreterAudio"
								playsInline
								autoPlay
								ref={userAudio}
								muted
								controls
								className="hidden"
							/>
							<Typography variant="subtitle1" className="flex justify-center uppercase text-grey-500">
								Ready !
							</Typography>
							<Typography variant="subtitle2" className="flex justify-center text-grey-500">
								Press MIC to Start Broadcasting
							</Typography>
						</div>
						<div className="m-8 flex flex-row justify-between items-center">
							<div className="flex flex-row">
								<Typography variant="subtitle1" className="flex justify-center uppercase">
									Ready !
								</Typography>
								<Typography variant="subtitle1" className="flex justify-center uppercase">
									Ready !
								</Typography>
							</div>
							<div className="flex flex-row">
								<DevicesInfo socketData={JoinedData} />
							</div>
						</div>
					</div>

					<AudioContainerOutput avConsoleData={avConsoleData} />
				</div>
			</div>
			<Divider />
			<div className="flex flex-1 m-16 ">
				<Button
					className=" bg-red-700 hover:bg-red-900"
					onClick={() => {
						dispatch(
							FuseActions.openDialog({
								children: (
									<>
										<DialogTitle id="audio-devices-item">Logout</DialogTitle>
										<DialogContent dividers className="p-12">
											<Typography variant="h6" className="p-24">
												Are you sure you want to logout?
											</Typography>
										</DialogContent>
										<DialogActions>
											<Button
												color="primary"
												onClick={() => {
													dispatch(FuseActions.closeDialog());
												}}
											>
												Cancel
											</Button>
											<Button
												color="primary"
												onClick={() => {
													dispatch(authActions.logoutUser());
													dispatch(FuseActions.closeDialog());
												}}
											>
												Logout
											</Button>
										</DialogActions>
									</>
								)
							})
						);
					}}
				>
					<ExitToAppIcon />
				</Button>
				<Button className="w-1/5 p-16 mx-16 bg-black text-grey-500">partner</Button>
				<Button className="w-1/5 p-16 mx-16 bg-black text-grey-500">floor</Button>
				{micHandler === true ? (
					<Button
						className={clsx('bg-black text-grey-500 hover:bg-black', 'w-2/5 p-16 mx-16 ')}
						onClick={handleStartInterpreter}
					>
						mic
					</Button>
				) : (
					<Button
						className={clsx('bg-red text-white hover:bg-red-700', 'w-2/5 p-16 mx-16 ')}
						onClick={handleStopInterpreter}
					>
						mic
					</Button>
				)}
				<Button className="w-1/5 p-16 mx-16 bg-black text-grey-500">mute</Button>
				<Button className="w-1/5 p-16 mx-16 bg-black text-grey-500">handover</Button>
			</div>
		</>
	);
};

const forwardRefBox = forwardRef(ControlBox);

export default React.memo(forwardRefBox);
