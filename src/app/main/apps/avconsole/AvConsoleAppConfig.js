import AvConsoleConfig from './avConsoleDash/AvConsoleConfig';
import AvConsoleInterpreterConfig from './avConsoleInterpreter/AvConsoleInterpreterConfig';

const AvConsoleAppConfig = [AvConsoleConfig, AvConsoleInterpreterConfig];

export default AvConsoleAppConfig;
