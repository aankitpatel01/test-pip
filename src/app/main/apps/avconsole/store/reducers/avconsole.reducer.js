import * as Actions from '../actions';

const initialState = {
	data: null,
	currentSessions: null,
	srcLang: null,
	dstLang: null,
	connectedUsers: [],
	micoff: null
};

const avConsole = (state = initialState, action) => {
	switch (action.type) {
		case Actions.GET_AVCONSOLE_EVENT_DATA:
			return {
				...state,
				data: action.payload
			};
		case Actions.GET_CONNECTED_INTERPRETER:
			return {
				...state,
				connectedUsers: action.payload
			};
		case Actions.CLEAN_UP_AVCONSOLE: {
			return {
				...state,
				data: null,
				connectedUsers: []
			};
		}
		case Actions.MIC_ON: {
			const indexObj = state.connectedUsers.findIndex(user => user?.socketId === action.payload?.socketId);
			console.log('Find UserIndex => ', indexObj);
			const spliceObj = state.connectedUsers.splice(indexObj, 1, action.payload);
			console.log('Final MIC ON => ', spliceObj);
			return {
				...state,
				connectedUsers: [...state.connectedUsers]
			};
		}
		case Actions.MIC_OFF: {
			const indexObj = state.connectedUsers.findIndex(user => user?.socketId === action.payload?.socketId);
			console.log('Find UserIndex => ', indexObj);
			const spliceObj = state.connectedUsers.splice(indexObj, 1, action.payload);
			console.log('Final Update=> ', spliceObj);
			return {
				...state,
				connectedUsers: [...state.connectedUsers]
			};
		}
		case Actions.NEW_USER_JOINED: {
			return {
				...state,
				connectedUsers: [...state.connectedUsers, action.payload]
			};
		}
		case Actions.REMOVE_JOINED_USER: {
			const newList = state.connectedUsers.filter(user => user?.socketId !== action.payload?.socketId);
			console.log('newlist =>', newList);
			return {
				...state,
				connectedUsers: [...newList]
			};
		}
		case Actions.SET_CURRENT_SESSION: {
			return {
				...state,
				currentSessions: action.payload
			};
		}
		case Actions.SELECT_SRC_LANG: {
			return {
				...state,
				srcLang: action.payload
			};
		}
		case Actions.SELECT_DST_LANG: {
			return {
				...state,
				dstLang: action.payload
			};
		}
		default:
			return state;
	}
};

export default avConsole;
