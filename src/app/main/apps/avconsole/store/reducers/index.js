import { combineReducers } from 'redux';
import avConsole from './avconsole.reducer';

const reducer = combineReducers({
	avConsole
});

export default reducer;
