import axios from 'axios';

export const GET_AVCONSOLE_EVENT_DATA = '[AVCONSOLE APP] GET AVCONSOLE EVENT DATA';
export const GET_CONNECTED_INTERPRETER = '[AVCONSOLE APP] GET_CONNECTED_INTERPRETER';
export const NEW_USER_JOINED = '[AVCONSOLE APP] NEW_USER_JOINED';
export const REMOVE_JOINED_USER = '[AVCONSOLE APP] REMOVE_JOINED_USER';
export const CLEAN_UP_AVCONSOLE = '[AVCONSOLE APP] CLEAN_UP_AVCONSOLE';
export const SET_CURRENT_SESSION = '[AVCONSOLE APP] SET_CURRENT_SESSION';
export const SELECT_SRC_LANG = '[AVCONSOLE INTERPRETER APP] SELECT_SRC_LANG';
export const SELECT_DST_LANG = '[AVCONSOLE INTERPRETER APP] SELECT_DST_LANG';
export const MIC_ON = '[AVCONSOLE APP] MIC_ON';
export const MIC_OFF = '[AVCONSOLE APP] MIC_OFF';

export function cleanUpAvconsole() {
	return {
		type: CLEAN_UP_AVCONSOLE
	};
}

export function micON(data) {
	console.log('MIC_ON => ', data);
	return {
		type: MIC_ON,
		payload: data
	};
}

export function micOFF(data) {
	console.log('MIC_OFF => ', data);
	return {
		type: MIC_OFF,
		payload: data
	};
}

export function newUserJoined(data) {
	console.log('NEW_USER_JOINED => ', data);
	return {
		type: NEW_USER_JOINED,
		payload: data
	};
}

export function removeJoinedUser(data) {
	console.log('REMOVE_JOINED_USER => ', data);
	return {
		type: REMOVE_JOINED_USER,
		payload: data
	};
}
export function setCurrentSession(data) {
	console.log('SET_CURRENT_SESSION => ', data);
	return {
		type: SET_CURRENT_SESSION,
		payload: data
	};
}

export function getAvConsoleEvent(eventId) {
	console.log('Event ID for AVConsole', eventId);
	const request = axios.get(`av-console/getAvConsoleEvents/${eventId}`);

	return dispatch =>
		request
			.then(res =>
				dispatch({
					type: GET_AVCONSOLE_EVENT_DATA,
					payload: res.data
				})
			)
			.catch(error => console.log(error));
}

export function getConnectedInterpreter(eventId) {
	console.log('Event ID for AVConsoleInterprettere', eventId);
	const request = axios.get(`event/getInterpreterDetails/${eventId}`);

	return dispatch =>
		request
			.then(res =>
				dispatch({
					type: GET_CONNECTED_INTERPRETER,
					payload: res.data.event
				})
			)
			.catch(error => console.log(error));
}

// INTERPRETER AVCONSOLE

export function getAvConsoleEventInterpreter(data) {
	console.log('Event ID for AVConsole', data.eventId);
	const request = axios.get(`/event/getInterpretorForAvConsole?eventId=${data.eventId}&email=${data.email}`);

	return dispatch =>
		request
			.then(res =>
				dispatch({
					type: GET_AVCONSOLE_EVENT_DATA,
					payload: res.data
				})
			)
			.catch(error => console.log(error));
}

export function selectSrcLang(val) {
	return {
		type: SELECT_SRC_LANG,
		payload: val
	};
}

export function selectDstLang(val) {
	return {
		type: SELECT_DST_LANG,
		payload: val
	};
}
