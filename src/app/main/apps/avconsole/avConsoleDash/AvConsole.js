import React, { useEffect } from 'react';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import FuseLoading from '@fuse/core/FuseLoading';
import io from 'socket.io-client';
import SummaryControlCard from '../shared-components/SummaryCard/SummaryControlCard';
import ChatPanel from '../shared-components/chatPanel/ChatPanel';
import AvManagerControlCard from '../shared-components/AVManagerCard/AvManagerControlCard';
import * as ChatActions from '../shared-components/chatPanel/store/actions';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';

// Let's do this 💪
const socket = io.connect('https://daastapanel.hashweb.in/');

const AvConsole = props => {
	const dispatch = useDispatch();
	const { eventId } = props.match.params;
	const userData = useSelector(({ auth }) => auth.user.data);
	const userName = useSelector(({ auth }) => auth.user.data.displayName);
	const EventData = useSelector(({ avConsoleOrganiser }) => avConsoleOrganiser.avConsole.data);
	const connecttedUsers = useSelector(({ avConsoleOrganiser }) => avConsoleOrganiser.avConsole.connectedUsers);

	const OSName = navigator.platform;

	// if (navigator.appVersion.indexOf('Win') !== -1) OSName = 'Windows 10';
	// if (navigator.appVersion.indexOf('Mac') !== -1) OSName = 'MacOS';
	// if (navigator.appVersion.indexOf('X11') !== -1) OSName = 'UNIX';
	// if (navigator.appVersion.indexOf('Linux') !== -1) OSName = 'Linux';

	const joinedData = {
		eventId,
		socketId: socket.id,
		name: userName,
		id: userData.id,
		role: 'Organiser',
		protocol: 'webRTC',
		userAgent: OSName,
		status: 'active',
		// data: avConsoleData,
		startTime: new Date().toISOString()
	};

	// const leaveData = {
	// 	eventId,
	// 	socketId: socket.id,
	// 	name: userName,
	// 	id: userData.id,
	// 	role: 'Interpreter',
	// 	protocol: 'webRTC',
	// 	userAgent: OSName,
	// 	status: 'deactived',
	// 	// data: avConsoleData,
	// 	endTime: new Date().toISOString()
	// };

	useEffect(() => {
		// Connected User
		socket.on('connect', () => {
			socket.emit('newUserJoinRoom', eventId);
			socket.emit('connected_interpreter', eventId);

			socket.emit('OrganiserJoined', joinedData);
			// console.log('JOINDATA => ', joinedData);
			// console.log(' Leavee Data => ', leaveData);
			socket.on('interpreter-mic-disconnect-received', data => {
				dispatch(Actions.micOFF(data));
			});
			socket.on('interpreter-mic-on-received', data => {
				dispatch(Actions.micON(data));
			});

			socket.on('interpreter received', data => {
				dispatch(Actions.newUserJoined(data));
			});
			// Disconnected User
			socket.on('interpreter removed', data => {
				dispatch(Actions.removeJoinedUser(data));
			});
		});
		// react-hooks/exhaustive-deps
	}, []);

	useEffect(() => {
		dispatch(ChatActions.getChat(eventId));
		dispatch(Actions.getAvConsoleEvent(eventId));
		dispatch(Actions.getConnectedInterpreter(eventId));
	}, [dispatch, eventId]);

	// window.onbeforeunload = event => {
	// 	event.preventDefault();

	// 	console.log('WINDOW RELOAD!!!');

	// 	return window.Prompt('Hey You ');
	// };

	console.log('connecttedUsers Run => ', connecttedUsers);

	// navigator.mediaDevices
	// 	.enumerateDevices()
	// 	.then(devices => {
	// 		devices.forEach(device => {
	// 			console.log(`${device.kind}: ${device.label} id = ${device.deviceId}`);
	// 		});
	// 	})
	// 	.catch(err => {
	// 		console.log(`${err.name}: ${err.message}`);
	// 	});

	if (!EventData) {
		return <FuseLoading />;
	}

	return (
		<>
			<FuseScrollbars>
				<div className="flex flex-1 flex-col w-3/4">
					<SummaryControlCard rooms={EventData.rooms} />
					{connecttedUsers.length > 0 &&
						connecttedUsers.map((user, index) => <AvManagerControlCard key={index} interpreter={user} />)}
				</div>
			</FuseScrollbars>

			<ChatPanel className="w-1/4 flex-1 max-h-sm" eventId={eventId} EventData={EventData} userName={userName} />
		</>
	);
};

export default withReducer('avConsoleOrganiser', reducer)(AvConsole);
