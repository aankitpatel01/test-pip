import React from 'react';
import { authRoles } from 'app/auth';

const AvConsoleConfig = {
	settings: {
		layout: {
			config: {
				navbar: {
					display: false
				},
				toolbar: {
					display: false
				},
				footer: {
					display: false
				},
				leftSidePanel: {
					display: false
				},
				rightSidePanel: {
					display: false
				}
			}
		},
		theme: {
			main: 'mainThemeDark'
		}
	},
	auth: authRoles.staff,
	routes: [
		{
			exact: true,
			path: '/avconsole/event/:eventId',
			component: React.lazy(() => import('./AvConsole'))
		}
	]
};

export default AvConsoleConfig;
