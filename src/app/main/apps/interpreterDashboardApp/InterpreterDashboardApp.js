import React, { useState, useEffect } from 'react';
import FusePageSimple from '@fuse/core/FusePageSimple';
import { useStyles } from '@material-ui/pickers/views/Calendar/SlideTransition';
import { Tabs, Tab, Typography } from '@material-ui/core';
import FuseAnimate from '@fuse/core/FuseAnimate';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import FuseLoading from '@fuse/core/FuseLoading';
import EventInvitedCard from './shared-components/EventInvitedCard';
import * as Actions from './store/actions';
import reducer from './store/reducers';

const InterpreterDashboardApp = () => {
	const dispatch = useDispatch();
	const classes = useStyles();
	const invitedEvent = useSelector(({ interpreterDashboardApp }) => interpreterDashboardApp.interpreter.data);
	const historyEvent = useSelector(({ interpreterDashboardApp }) => interpreterDashboardApp.interpreter.history);
	const [selectedTab, setSelectedTab] = useState(0);
	const user = useSelector(({ auth }) => auth.user.data.email);

	useEffect(() => {
		dispatch(Actions.getInterpreter(user));
		dispatch(Actions.getInterpreterHistroy(user));
	}, [dispatch, user]);

	const handleTabChange = (event, value) => {
		setSelectedTab(value);
	};

	if (!invitedEvent) {
		return <FuseLoading />;
	}

	return (
		<div>
			<FusePageSimple
				classes={{
					root: classes.layoutRoot,
					toolbar: 'px-16 sm:px-24'
				}}
				header={
					<div className="p-24">
						<h1>Interpreter</h1>
					</div>
				}
				contentToolbar={
					<Tabs
						value={selectedTab}
						onChange={handleTabChange}
						indicatorColor="primary"
						textColor="primary"
						variant="fullWidth"
						scrollButtons="off"
						className="w-full h-64 border-b-1"
					>
						<Tab className="h-64" label="Current" />
						<Tab className="h-64" label="History" />
					</Tabs>
				}
				content={
					<div className="p-24">
						{selectedTab === 0 && (
							<div>
								{invitedEvent.length > 0 ? (
									invitedEvent.map((invited, index) => (
										<EventInvitedCard key={index} invited={invited} />
									))
								) : (
									<FuseAnimate delay={100}>
										<div className="flex flex-1 items-center justify-center h-full">
											<Typography color="textSecondary" variant="h5">
												There are no Events!
											</Typography>
										</div>
									</FuseAnimate>
								)}
							</div>
						)}
						{selectedTab === 1 && (
							<div>
								{historyEvent.length > 0 ? (
									historyEvent.map((invited, index) => (
										<EventInvitedCard key={index} invited={invited} />
									))
								) : (
									<FuseAnimate delay={100}>
										<div className="flex flex-1 items-center justify-center h-full">
											<Typography color="textSecondary" variant="h5">
												There are no Events!
											</Typography>
										</div>
									</FuseAnimate>
								)}
							</div>
						)}
					</div>
				}
			/>
		</div>
	);
};

export default withReducer('interpreterDashboardApp', reducer)(React.memo(InterpreterDashboardApp));
