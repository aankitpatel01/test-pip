import React from 'react';
import { authRoles } from 'app/auth';

const InterpreterDashboardAppConfig = {
	settings: {
		layout: {
			config: {
				footer: {
					display: true,
					style: 'fixed'
				}
			}
		}
	},
	auth: authRoles.interpreter,
	routes: [
		{
			path: '/interpreter',
			component: React.lazy(() => import('./InterpreterDashboardApp'))
		},
		{
			path: '/interpreter-settings',
			component: React.lazy(() => import('./shared-components/InterpreterSettingsLayout'))
		}
	]
};

export default InterpreterDashboardAppConfig;
