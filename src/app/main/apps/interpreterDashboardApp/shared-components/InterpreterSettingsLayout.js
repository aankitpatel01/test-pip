import React, { useState, useEffect } from 'react';
import { makeStyles, Tabs, Tab } from '@material-ui/core';
import FusePageCarded from '@fuse/core/FusePageCarded';
import withReducer from 'app/store/withReducer';
import { useDispatch, useSelector } from 'react-redux';
import reducer from '../../dashboards/dashboardsettings/store/reducers';
import PersonalDetailsTab from '../../dashboards/dashboardsettings/PersonalDetailsTab';
import PrivacySettingsTab from '../../dashboards/dashboardsettings/PrivacySettingsTab';
import * as Actions from '../../dashboards/dashboardsettings/store/actions';

const useStyles = makeStyles({
	layoutRoot: {}
});

function InterpreterSettingsLayout() {
	const classes = useStyles();
	const dispatch = useDispatch();
	const user = useSelector(({ auth }) => auth.user.data);
	const SettingsData = useSelector(({ interpreterSettingsLayout }) => interpreterSettingsLayout.settings.personal);
	const { id } = user;

	useEffect(() => {
		dispatch(Actions.getSettings(id));
		dispatch(Actions.getCountryList());
	}, [dispatch, id]);

	const [selectedTab, setSelectedTab] = useState(0);

	const handleTabChange = (event, value) => {
		setSelectedTab(value);
	};

	return (
		<FusePageCarded
			classes={{
				root: classes.layoutRoot,
				toolbar: 'p-0'
			}}
			header={null}
			contentToolbar={
				<Tabs
					value={selectedTab}
					onChange={handleTabChange}
					indicatorColor="primary"
					textColor="primary"
					variant="fullWidth"
					scrollButtons="off"
					className="w-full h-64"
				>
					<Tab className="h-64" label="Personal Details" />
					<Tab className="h-64" label="Privacy Settings" />
				</Tabs>
			}
			content={
				<div className="p-24">
					{console.log('Setting ', SettingsData)}
					{selectedTab === 0 && (
						<>
							<h2 className="mb-16">Personal Details</h2>
							<PersonalDetailsTab personalDetails={SettingsData} />
						</>
					)}

					{selectedTab === 1 && (
						<div>
							<h2 className="mb-16">Privacy Settings</h2>
							<PrivacySettingsTab PrivacyData={SettingsData} />
						</div>
					)}
				</div>
			}
		/>
	);
}

export default withReducer('interpreterSettingsLayout', reducer)(InterpreterSettingsLayout);
