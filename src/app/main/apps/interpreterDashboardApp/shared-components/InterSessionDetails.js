import { Icon, ListItem, ListItemText, Typography } from '@material-ui/core';
import React from 'react';
import moment from 'moment';

const InterSessionDetails = ({ topic }) => {
	// console.log('session Data', topic);

	const { title, recstartTime, assignments } = topic;
	const { srcTitle, dstTitle } = assignments;

	return (
		<ListItem>
			<ListItemText
				primary={
					<Typography variant="body1" className="todo-title truncate" color="primary">
						{title}
					</Typography>
				}
				secondary={
					<Typography variant="subtitle1" className="todo-title truncate" color="primary">
						{moment(recstartTime).format('ddd, D MMMM, h:mm a')}
					</Typography>
				}
			/>
			<div className="flex  flex-col relative overflow-hidden px-8">
				<Typography color="primary" variant="subtitle1" className="truncate">
					{srcTitle}
				</Typography>
			</div>
			<div className="mx-8">
				<Icon>compare_arrows</Icon>
			</div>
			<div className="flex flex-col relative overflow-hidden px-8">
				<Typography color="primary" variant="subtitle1" className="truncate">
					{dstTitle}
				</Typography>
			</div>
		</ListItem>
	);
};

export default React.memo(InterSessionDetails);
