import React from 'react';
import { ListItem, Paper, Typography, Button, ListItemText } from '@material-ui/core';
// import History from '@history';
import { Link as RouterLink } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import EventMaterialDownload from './EventMaterialDownload';
import InterSessionDetails from './InterSessionDetails';
import * as Actions from '../store/actions';

const EventInvitedCard = ({ invited }) => {
	const dispatch = useDispatch();
	const { eventName, bidirectional, eventId, location, topics, assignments, eventMaterial } = invited;
	const { id, user, srcTitle, srcStreamId, dstTitle, status, roomId } = assignments;

	console.log('EventInvitedCard => ', invited);

	const handleAccept = () => {
		const data = { id, user, status: 'confirmed' };
		dispatch(Actions.acceptInvite(data));
	};

	return (
		<Paper className=" w-full p-16 mb-16">
			<Typography variant="h6" className="flex" noWrap>
				<p className="font-normal text-grey-A700"> You are invited to</p>&nbsp;{eventName}
				<p className="font-normal text-grey-A700">&nbsp;at</p>
				&nbsp;{location}
			</Typography>

			<ListItem className="border-solid border-b-1 py-16 px-0 sm:px-8">
				{status === 'confirmed' ? (
					<>
						<ListItemText
							primary={
								<Typography variant="h6" className="todo-title truncate" color="primary">
									{eventName}
								</Typography>
							}
							secondary={
								<Typography variant="subtitle1" className="todo-title truncate" color="primary">
									at&nbsp;{location}
								</Typography>
							}
						/>
						<hr />
					</>
				) : (
					<>
						<div className="flex flex-1 flex-col relative overflow-hidden px-8">
							<Typography variant="caption" className="todo-title truncate" color="textSecondary">
								From
							</Typography>

							<Typography color="primary" variant="subtitle1" className="truncate">
								{srcTitle}
							</Typography>

							<Typography color="error" variant="subtitle2" className="truncate">
								{bidirectional && 'Two-ways allowed'}
							</Typography>
						</div>
						<div className="flex flex-1 flex-col relative overflow-hidden px-8">
							<Typography variant="caption" className="todo-title truncate" color="textSecondary">
								To
							</Typography>

							<Typography color="primary" variant="subtitle1" className="truncate">
								{dstTitle}
							</Typography>
						</div>
						<div className="flex flex-1 flex-col relative overflow-hidden px-8">
							<Typography variant="caption" className="todo-title truncate" color="textSecondary">
								Session
							</Typography>

							<Typography color="error" variant="subtitle1" className="truncate">
								{`Assigned to ${topics.length} sessions`}
							</Typography>
						</div>
					</>
				)}

				{status === 'confirmed' ? (
					<div className="px-8">
						<Button
							size="large"
							color="secondary"
							className="mx-8"
							variant="contained"
							onClick={ev => {
								ev.preventDefault();
								ev.stopPropagation();
								// dispatch(Actions.toggleImportant(props.todo));
							}}
						>
							Translate text
						</Button>
						<Button
							size="large"
							color="secondary"
							className="mx-8"
							variant="contained"
							component={RouterLink}
							target="_blank"
							to={`/avconsole/interpreter/${user}/${eventId}/${roomId}/${srcStreamId}`}
							// onClick={ev => {
							// 	ev.preventDefault();
							// 	ev.stopPropagation();
							// History.push('/avconsole/interpreter');
							// dispatch(Actions.toggleStarred(props.todo));
							// }}
						>
							Start
						</Button>
					</div>
				) : (
					<div className="px-8">
						<Button
							size="large"
							color="secondary"
							className="mx-8"
							variant="contained"
							onClick={ev => {
								ev.preventDefault();
								ev.stopPropagation();
								// dispatch(Actions.toggleImportant(props.todo));
							}}
						>
							Decline
						</Button>
						<Button
							size="large"
							color="secondary"
							className="mx-8"
							variant="contained"
							onClick={handleAccept}
						>
							accept
						</Button>
					</div>
				)}
			</ListItem>

			{status === 'confirmed' && topics.length > 0
				? topics.map((topic, index) => <InterSessionDetails key={index} topic={topic} />)
				: null}

			{eventMaterial.length > 0 && <EventMaterialDownload eventMaterial={eventMaterial} />}
		</Paper>
	);
};

export default React.memo(EventInvitedCard);
