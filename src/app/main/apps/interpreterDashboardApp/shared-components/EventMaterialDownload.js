import React, { useState } from 'react';
import { Typography, ListItem, FormControlLabel, Checkbox } from '@material-ui/core';
import EventMaterial from './EventMaterial';

const EventMaterialDownload = props => {
	const [allowdownload, setAllowdownload] = useState(false);
	return (
		<ListItem
			className="border-solid py-16 px-0 sm:px-8"
			onClick={ev => {
				ev.preventDefault();
				// dispatch(Actions.openEditTodoDialog(props.todo));
			}}
		>
			<div className="flex flex-1 flex-col relative overflow-hidden px-8">
				<Typography variant="h6" className="todo-title truncate" color="primary">
					Event Material
				</Typography>
				<FormControlLabel
					className="p-4"
					control={
						<Checkbox
							checked={allowdownload}
							name="allowdownload"
							onClick={() => setAllowdownload(!allowdownload)}
						/>
					}
					label="I understand
				that this material is confidential and it can only be used for this event. It is not permitted to share
				or distribute the material."
				/>
				{props.eventMaterial.map(material => (
					<EventMaterial
						key={material?._id}
						fileName={material?.fileName}
						size=""
						allowdownload={allowdownload}
					/>
				))}
			</div>
		</ListItem>
	);
};

EventMaterialDownload.propTypes = {};

export default React.memo(EventMaterialDownload);
