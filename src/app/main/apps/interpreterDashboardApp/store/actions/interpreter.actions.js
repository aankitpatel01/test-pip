import axios from 'axios';

export const GET_INTERPRETER_DATA = '[INTERPRETER DASHBOARD APP] GET INTERPRETER DATA';
export const GET_INTERPRETER_HISTROY = '[INTERPRETER DASHBOARD APP] GET INTERPRETER HISTORY';
export const ACCPT_INVITE = '[INTERPRETER DASHBOARD APP] ACCPT INVITE';

// const accessToken = localStorage.getItem('jwt_access_token');

// const axiosDefaults = axios.create({
// 	baseURL: 'http://69.16.254.127:4006/api'
// });

// const config = {
// 	headers: {
// 		'Content-Type': 'application/json',
// 		'x-access-token': accessToken
// 	}
// };

export function getInterpreter(email) {
	const request = axios.get(`/event/getInterpretorEvents/${email}`);

	return dispatch =>
		request
			.then(response => {
				const activeEvent = response.data.filter(tx => tx.status === 'publish');
				dispatch({
					type: GET_INTERPRETER_DATA,
					payload: activeEvent
				});
			})
			.catch(error => console.log(error));
}
export function getInterpreterHistroy(email) {
	const request = axios.get(`/event/getInterpretorEvents/${email}`);

	return dispatch =>
		request
			.then(response => {
				const historylist = response.data.filter(tx => tx.status === 'archived');
				dispatch({
					type: GET_INTERPRETER_HISTROY,
					payload: historylist
				});
			})
			.catch(error => console.log(error));
}

export function acceptInvite(data) {
	const request = axios.post(`/event/updateInterpretor/${data.id}`, data);

	return dispatch =>
		request
			.then(response =>
				dispatch({
					type: ACCPT_INVITE,
					payload: response.data
				})
			)
			.then(() => dispatch(getInterpreter(data.user)))
			.catch(error => console.log(error));
}
