import * as Actions from '../actions';

const initialState = {
	data: [],
	history: []
};

const interpreterReducer = (state = initialState, action) => {
	switch (action.type) {
		case Actions.GET_INTERPRETER_DATA:
			return {
				...state,
				data: [...action.payload]
			};
		case Actions.GET_INTERPRETER_HISTROY:
			return {
				...state,
				history: [...action.payload]
			};
		default:
			return state;
	}
};

export default interpreterReducer;
