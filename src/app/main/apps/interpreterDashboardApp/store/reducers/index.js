import { combineReducers } from 'redux';
import interpreter from './interpreter.reducer';

const reducer = combineReducers({
	interpreter
});

export default reducer;
