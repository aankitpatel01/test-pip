// import AcademyAppConfig from './academy/AcademyAppConfig';
// import CalendarAppConfig from './calendar/CalendarAppConfig';
// import ChatAppConfig from './chat/ChatAppConfig';
// import ContactsAppConfig from './contacts/ContactsAppConfig';
import AnalyticsDashboardAppConfig from './dashboards/analytics/AnalyticsDashboardAppConfig';
import UserManagerConfig from './dashboards/usermanager/UserManagerConfig';
import CreateEventConfig from './dashboards/create-event/CreateEventConfig';
import PublishEventConfig from './dashboards/create-event/publishEvent/PublishEventConfig';
import InterpreterDashboardAppConfig from './interpreterDashboardApp/InterpeterDashboardAppConfig';
import WebAppConfig from './webApp/WebAppConfig';
// import AvConsoleAppConfig from './avconsole/AvConsoleAppConfig';
// import ProjectDashboardAppConfig from './dashboards/project/ProjectDashboardAppConfig';
// import ECommerceAppConfig from './e-commerce/ECommerceAppConfig';
// import FileManagerAppConfig from './file-manager/FileManagerAppConfig';
// import MailAppConfig from './mail/MailAppConfig';
// import NotesAppConfig from './notes/NotesAppConfig';
// import ScrumboardAppConfig from './scrumboard/ScrumboardAppConfig';
// import TodoAppConfig from './todo/TodoAppConfig';

const appsConfigs = [
	AnalyticsDashboardAppConfig,
	InterpreterDashboardAppConfig,
	UserManagerConfig,
	CreateEventConfig,
	PublishEventConfig,
	WebAppConfig
	// ProjectDashboardAppConfig,
	// MailAppConfig,
	// TodoAppConfig,
	// FileManagerAppConfig,
	// ContactsAppConfig,
	// CalendarAppConfig,
	// ChatAppConfig,
	// ECommerceAppConfig,
	// ScrumboardAppConfig,
	// AcademyAppConfig,
	// NotesAppConfig
];

export default appsConfigs;
