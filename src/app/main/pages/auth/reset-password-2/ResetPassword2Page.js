import FuseAnimate from '@fuse/core/FuseAnimate';
// import { useForm } from '@fuse/hooks';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { makeStyles } from '@material-ui/core/styles';
// import { darken } from '@material-ui/core/styles/colorManipulator';
import Typography from '@material-ui/core/Typography';
import clsx from 'clsx';
import * as authActions from 'app/auth/store/actions';
import React, { useState, useEffect, useRef } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Formsy from 'formsy-react';
import { TextFieldFormsy } from '@fuse/core/formsy';
import { useSelector, useDispatch } from 'react-redux';

const useStyles = makeStyles(theme => ({
	root: {
		background: theme.palette.grey[900],
		color: theme.palette.primary.contrastText
	},
	heightCenter: {
		minHeight: '100vh'
	}
}));

function ResetPasswordPage2() {
	const classes = useStyles();
	const dispatch = useDispatch();
	const reset = useSelector(({ auth }) => auth.reset);
	const resetSucess = useSelector(({ auth }) => auth.reset.linksuccess);

	const [isFormValid, setIsFormValid] = useState(false);

	const formRef = useRef(null);

	useEffect(() => {
		if (reset.error && (reset.error.email || reset.error.password)) {
			formRef.current.updateInputsWithError({
				...reset.error
			});
			disableButton();
		}
		return () => {
			if (resetSucess) {
				// History.push('/login');
				console.log('change');
			}
		};
	}, [resetSucess, reset.error]);

	// function isFormValid() {
	// 	return (
	// 		form.email.length > 0
	// 		// form.password.length > 0 &&
	// 		// form.password.length > 3 &&
	// 		// form.password
	// 		//  === form.passwordConfirm
	// 	);
	// }
	function disableButton() {
		setIsFormValid(false);
	}

	function enableButton() {
		setIsFormValid(true);
	}

	function handleSubmit(model) {
		console.log(model);

		dispatch(authActions.resetPasswordUser(model));
		// resetForm();
	}

	// function handleSubmit(ev) {
	// 	ev.preventDefault();
	// 	console.log('stateDAta test');
	// }
	if (resetSucess) {
		// history.push('/login');
		return <Redirect to="/mail-confirm" />;
	}

	return (
		<div className={clsx(classes.root, 'flex flex-col flex-auto flex-shrink-0 p-24 md:flex-row md:p-0')}>
			<div className="flex flex-col flex-grow-0 items-center text-white p-16 text-center md:p-128 md:items-start md:flex-shrink-0 md:flex-1 md:text-left">
				<FuseAnimate animation="transition.expandIn">
					<img className="w-128 mb-32" src="assets/images/logos/logo-square.png" alt="Daasta" />
				</FuseAnimate>

				<FuseAnimate animation="transition.slideUpIn" delay={300}>
					<Typography variant="h3" color="inherit" className="font-light">
						Welcome to the DAASTA!
					</Typography>
				</FuseAnimate>

				<FuseAnimate delay={400}>
					<Typography variant="subtitle1" color="inherit" className="max-w-512 mt-16">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ullamcorper nisl erat, vel
						convallis elit fermentum pellentesque. Sed mollis velit facilisis facilisis.
					</Typography>
				</FuseAnimate>
			</div>

			<FuseAnimate animation={{ translateX: [0, '100%'] }}>
				<Card className="w-full max-w-400 mx-auto m-16 md:m-0" square>
					<CardContent
						className={clsx(
							classes.heightCenter,
							'flex flex-col items-center justify-center p-32 md:p-48 '
						)}
					>
						<Typography variant="h6" className="md:w-full mb-32">
							RECOVER YOUR PASSWORD
						</Typography>
						<Formsy
							onValidSubmit={handleSubmit}
							onValid={enableButton}
							onInvalid={disableButton}
							ref={formRef}
							className="flex flex-col justify-center w-full"
						>
							<TextFieldFormsy
								className="mb-16"
								type="email"
								name="email"
								label="Email"
								autoFocus
								value=""
								validations={{ isEmail: true, minLength: 0 }}
								validationErrors={{
									isEmail: 'Enter the valid Email',
									minLength: 'Not Empty'
								}}
								variant="outlined"
								required
							/>
							<Button
								variant="contained"
								color="primary"
								className="w-224 mx-auto mt-16"
								aria-label="Reset"
								disabled={!isFormValid}
								type="submit"
							>
								SEND RESET LINK
							</Button>
						</Formsy>

						<div className="flex flex-col items-center justify-center pt-32 pb-24">
							<Link className="font-medium" to="/login">
								Go back to login
							</Link>
						</div>
					</CardContent>
				</Card>
			</FuseAnimate>
		</div>
	);
}

export default ResetPasswordPage2;
