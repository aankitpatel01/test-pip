import FuseAnimate from '@fuse/core/FuseAnimate';
import { useForm } from '@fuse/hooks';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { makeStyles } from '@material-ui/core/styles';
// import { darken } from '@material-ui/core/styles/colorManipulator';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import clsx from 'clsx';
import * as authActions from 'app/auth/store/actions';
import React, { useEffect } from 'react';
import { Link, withRouter, Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

const useStyles = makeStyles(theme => ({
	root: {
		background: theme.palette.grey[900],
		color: theme.palette.primary.contrastText
	}
}));

function ResetPasswordPage(props) {
	const classes = useStyles();
	const { match } = props;
	const dispatch = useDispatch();
	const resetSucess = useSelector(({ auth }) => auth.reset.success);

	useEffect(() => {
		return () => {
			if (resetSucess) {
				// history.push('/login');
				console.log('change');
			}
		};
	}, [resetSucess]);

	const { form, handleChange, resetForm } = useForm({
		name: '',
		email: '',
		password: '',
		passwordConfirm: ''
	});

	function isFormValid() {
		return form.password === form.passwordConfirm;
	}

	function handleSubmit(ev) {
		ev.preventDefault();
		const data = {
			userId: match.params.userId,
			token: match.params.token,
			password: form.password
		};
		console.log(form.password);
		console.log(data);
		dispatch(authActions.resetPasswordUpdate(data));
		resetForm();
	}
	if (resetSucess) {
		// history.push('/login');
		return <Redirect to="/login" />;
	}

	return (
		<div className={clsx(classes.root, 'flex flex-col flex-auto flex-shrink-0 items-center justify-center p-32')}>
			<div className="flex flex-col items-center justify-center w-full">
				<FuseAnimate animation="transition.expandIn">
					<Card className="w-full max-w-384">
						<CardContent className="flex flex-col items-center justify-center p-32">
							<img className="w-128 m-32" src="assets/images/logos/logo-square.png" alt="Daasta" />
							<Typography variant="h6" className="mt-16 mb-32">
								Create New Password
							</Typography>
							{console.log(resetSucess)}
							<form
								name="resetForm"
								noValidate
								className="flex flex-col justify-center w-full"
								onSubmit={handleSubmit}
							>
								{/* <TextField
									className="mb-16"
									label="Email"
									autoFocus
									type="email"
									name="email"
									value={form.email}
									onChange={handleChange}
									variant="outlined"
									required
									fullWidth
								/> */}

								<TextField
									className="mb-16"
									label="Password"
									type="password"
									name="password"
									value={form.password}
									onChange={handleChange}
									variant="outlined"
									required
									fullWidth
								/>

								<TextField
									className="mb-16"
									label="Password (Confirm)"
									type="password"
									name="passwordConfirm"
									value={form.passwordConfirm}
									onChange={handleChange}
									variant="outlined"
									required
									fullWidth
								/>

								<Button
									variant="contained"
									color="primary"
									className="w-224 mx-auto mt-16"
									aria-label="Reset"
									disabled={!isFormValid()}
									type="submit"
								>
									Create
								</Button>
							</form>
							{console.log(props)}
							<div className="flex flex-col items-center justify-center pt-32 pb-24">
								<Link className="font-medium" to="/login">
									Go back to login
								</Link>
							</div>
						</CardContent>
					</Card>
				</FuseAnimate>
			</div>
		</div>
	);
}

export default withRouter(ResetPasswordPage);
