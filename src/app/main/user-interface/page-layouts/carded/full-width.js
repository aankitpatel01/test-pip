// import DemoContent from '@fuse/core/DemoContent';
import FusePageCarded from '@fuse/core/FusePageCarded';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import UserManager from 'app/main/apps/dashboards/usermanager/UserManager';

const useStyles = makeStyles({
	layoutRoot: {}
});

function CardedFullWidthSample() {
	const classes = useStyles();

	return (
		<FusePageCarded
			classes={{
				root: classes.layoutRoot
			}}
			header={null}
			contentToolbar={null}
			content={
				<>
					<div className="w-full p-0 m-0">
						<UserManager />
					</div>
				</>
			}
		/>
	);
}

export default CardedFullWidthSample;
