import FuseUtils from '@fuse/utils/FuseUtils';
import axios from 'axios';
import jwtDecode from 'jwt-decode';

/* eslint-disable camelcase */
const accessToken = localStorage.getItem('jwt_access_token');
axios.defaults.baseURL = 'https://daastapanel.hashweb.in/api';
axios.defaults.headers.common.Authorization = accessToken;
// axios.defaults.headers.common['x-access-token'] = accessToken;
axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.headers.common['Cache-Control'] = 'no-cache';
// axios.defaults.headers.get['x-access-token'] = accessToken;

class JwtService extends FuseUtils.EventEmitter {
	init() {
		this.setInterceptors();
		this.handleAuthentication();
	}

	setInterceptors = () => {
		console.log(axios.interceptors);
		axios.interceptors.response.use(
			response => {
				console.log('setInterceptors', response);
				return response;
			},
			err => {
				return new Promise((resolve, reject) => {
					if (err.response.status === 401 && err.config && !err.config.__isRetryRequest) {
						// if you ever get an unauthorized response, logout the user
						console.log(err.response.data.error);
						this.emit('onAutoLogout', 'Unauthorized');
						this.setSession(null);
					}
					throw err;
				});
			}
		);
	};

	handleAuthentication = () => {
		const access_token = this.getAccessToken();

		if (!access_token) {
			this.emit('onNoAccessToken');

			return;
		}

		if (this.isAuthTokenValid(access_token)) {
			this.setSession(access_token);
			this.emit('onAutoLogin', true);
		} else {
			this.setSession(null);
			this.emit('onAutoLogout', 'access_token expired');
		}
	};

	createUser = data => {
		return new Promise((resolve, reject) => {
			axios.post('/api/auth/register', data).then(response => {
				if (response.data.user) {
					this.setSession(response.data.access_token);
					resolve(response.data.user);
				} else {
					reject(response.data.error);
				}
			});
		});
	};

	resetPasswordUser = email => {
		return new Promise((resolve, reject) => {
			axios.post('/auth/reset-password', email).then(response => {
				if (response.data) {
					resolve(response.data);
				} else {
					console.log(response);
					reject(response.data.error);
				}
			});
		});
	};

	resetPasswordUpdate = data => {
		return new Promise((resolve, reject) => {
			axios.post('/auth/store-password', data).then(response => {
				console.log(response);
				if (response.data) {
					resolve(response.data);
				} else {
					reject(response.data.error);
				}
			});
		});
	};

	signInWithEmailAndPassword = (email, password) => {
		return new Promise((resolve, reject) => {
			axios
				.post('/auth/signin', {
					email,
					password
				})
				.then(response => {
					if (response.status === 200) {
						console.log(response.data);
						this.setSession(response.data.access_token);
						window.localStorage.setItem('localUser', JSON.stringify(response.data));
						resolve(response.data);
					} else {
						console.log('login fail', response.status);
						// reject(response.data);
					}
				})
				.catch(error => {
					console.log(error.response?.data.error);
					// eslint-disable-next-line prefer-promise-reject-errors
					reject(error.response?.data.error);
				});
		});
	};

	signInWithToken = () => {
		return new Promise((resolve, reject) => {
			const access_token = this.getAccessToken();

			if (access_token) {
				this.setSession(access_token);
				const localUser = window.localStorage.getItem('localUser');
				const user = JSON.parse(localUser);
				resolve(user);
			} else {
				console.log('LogOut The User');
				this.logout();
				// Promise.reject(new Error('Failed to login with token.'));
			}
		});
	};

	updateUserData = user => {
		return axios.post('/api/auth/user/update', {
			user
		});
	};

	setSession = access_token => {
		if (access_token) {
			localStorage.setItem('jwt_access_token', access_token);
			axios.defaults.headers.common.Authorization = `Bearer ${access_token}`;
		} else {
			localStorage.removeItem('jwt_access_token');
			localStorage.removeItem('localUser');
			delete axios.defaults.headers.common.Authorization;
		}
	};

	logout = () => {
		this.setSession(null);
	};

	isAuthTokenValid = access_token => {
		if (!access_token) {
			return false;
		}
		const decoded = jwtDecode(access_token);
		const currentTime = Date.now() / 1000;
		if (decoded.exp < currentTime) {
			console.warn('access token expired');
			return false;
		}

		return true;
	};

	getAccessToken = () => {
		return window.localStorage.getItem('jwt_access_token');
	};
}

const instance = new JwtService();

export default instance;
