import React from 'react';
import { Redirect } from 'react-router-dom';
import FuseUtils from '@fuse/utils';
import appsConfigs from 'app/main/apps/appsConfigs';
import LoginConfig from 'app/main/login/LoginConfig';
import LogoutConfig from 'app/main/logout/LogoutConfig';
import CallbackConfig from 'app/main/callback/CallbackConfig';
import pagesConfigs from 'app/main/pages/pagesConfigs';
import ExampleConfig from 'app/main/example/ExampleConfig';
import UserInterfaceConfig from 'app/main/user-interface/UserInterfaceConfig';
import authRoleExamplesConfigs from 'app/main/auth/authRoleExamplesConfigs';
import AvConsoleAppConfig from 'app/main/apps/avconsole/AvConsoleAppConfig';

const routeConfigs = [
	...appsConfigs,
	...pagesConfigs,
	...authRoleExamplesConfigs,
	...AvConsoleAppConfig,
	ExampleConfig,
	LoginConfig,
	UserInterfaceConfig,
	LogoutConfig,
	CallbackConfig
];

const routes = [
	...FuseUtils.generateRoutesFromConfigs(routeConfigs, ['Admin', 'Organizer', 'Interpreter', 'Company']),
	{
		path: '/',
		exact: true,
		component: () => <Redirect to="/callback" />
	},
	{
		component: () => <Redirect to="/404" />
	}
];

export default routes;
