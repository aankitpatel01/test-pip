import FuseUtils from '@fuse/utils';
// import _ from '@lodash';
import moment from 'moment';
import mock from '../mock';

const eventDB = {
	eventDetails: {
		eventid: FuseUtils.generateGUID(),
		eventname: '',
		country: '',
		description: 'Description Message !!!',
		startdate: moment(new Date()).toISOString(),
		starttime: moment(new Date()).toISOString(),
		endtime: moment(new Date()).add(1, 'hours').toISOString(),
		coverimage: null,
		images: null,
		logo: null,
		eventTheme: null
	},
	roomsTab: [
		{
			roomid: FuseUtils.generateGUID(),
			roomtitle: 'Room 1x',
			lang: [
				{
					id: FuseUtils.generateGUID(),
					mode: 'audio',
					language: '',
					title: 'Finx',
					record: false,
					public: true
				},
				{
					id: FuseUtils.generateGUID(),
					mode: 'audio',
					language: '',
					title: 'Finix',
					record: false,
					public: true
				}
			]
		},
		{
			roomid: FuseUtils.generateGUID(),
			roomtitle: 'Room 2',
			lang: [
				{
					id: FuseUtils.generateGUID(),
					mode: 'audio',
					language: '',
					title: 'Samz',
					record: false,
					public: true
				},
				{
					id: FuseUtils.generateGUID(),
					mode: 'audio',
					language: '',
					title: 'Samx',
					record: false,
					public: true
				}
			]
		}
	],
	languageTab: [
		{
			id: FuseUtils.generateGUID(),
			mode: 'audio',
			language: '',
			title: '',
			record: false,
			public: false
			// startDate: new Date(),
			// dueDate: new Date()
		}
	]
};
mock.onGet('/api/event').reply(config => {
	return [200, eventDB.eventDetails];
});

mock.onGet('/api/room').reply(config => {
	return [200, eventDB.roomsTab];
});
mock.onGet('/api/language').reply(config => {
	return [200, eventDB.languageTab];
});

// mock.onPost('/api/language').reply(request => {
// 	const data = JSON.parse(request.data); //?
// 	const { selectedTodoIds } = data;
// 	eventDB.languageTab = eventDB.languageTab.filter(_todo => (selectedTodoIds.includes(_todo.id) ? false : _todo));
// 	return [200];
// });
