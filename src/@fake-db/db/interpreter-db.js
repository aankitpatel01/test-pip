// import FuseUtils from '@fuse/utils';
import mock from '../mock';

const interpretersDB = {
	data: [
		{
			id: 16535,
			hasCover: 1,
			hasLogo: 0,
			country: 'NO',
			city: 'Tromsø',
			state: null,
			address: 'Storgata 67, 9008 Tromsø, Norway',
			zip: '9008',
			lat: 69.6497213,
			lon: 18.9566424,
			status: 'archived',
			access: 'public',
			timezone: 'Europe/Oslo',
			wifiName: null,
			externalIp: null,
			code: 'duodji2020',
			startTime: '2020-01-14T08:00:56.000Z',
			endTime: '2020-01-14T12:00:56.000Z',
			recurring: 0,
			palette: 'blue',
			expectedListeners: null,
			type: 'standard',
			created: '2020-01-09T09:51:55.000Z',
			vanilla: 0,
			t: [{ lang: 'en', title: 'Duodji', description: '', eventId: 16535 }],
			topics: [
				{
					id: 18312,
					room: 0,
					startTime: '2020-01-14T08:00:56.000Z',
					endTime: '2020-01-14T12:00:56.000Z',
					recStartTime: '2020-01-14T08:00:56.000Z',
					recEndTime: '2020-01-14T12:00:56.000Z',
					t: [{ lang: 'en', title: 'Duodji', description: '', topicId: 18312 }],
					assignments: [
						{
							id: 24837,
							user: { type: 'user', id: 1879 },
							email: 'interpreter@daasta.com',
							status: 'confirmed',
							srcLang: 'smi',
							dstLang: 'nor',
							bidirectional: 0,
							partners: [],
							topicId: 18312,
							userId: 1879
						}
					],
					eventId: 16535
				}
			],
			companyId: 505,
			userId: 550
		},
		{
			id: 16736,
			hasCover: 1,
			hasLogo: 0,
			country: 'NO',
			city: null,
			state: null,
			address: 'Hannoluohkka 45, 9520 Kautokeino, Norge',
			zip: '9520',
			lat: 69.0136983,
			lon: 23.0386697,
			status: 'archived',
			access: 'public',
			timezone: 'Europe/Oslo',
			wifiName: null,
			externalIp: null,
			code: null,
			startTime: '2020-01-19T12:00:28.000Z',
			endTime: '2020-01-19T15:00:28.000Z',
			recurring: 0,
			palette: 'violet',
			expectedListeners: null,
			type: 'standard',
			created: '2020-01-15T11:52:26.000Z',
			vanilla: 0,
			t: [{ lang: 'en', title: 'Halti', description: ' Halti Transboundary Area ', eventId: 16736 }],
			topics: [
				{
					id: 18529,
					room: 0,
					startTime: '2020-01-19T12:00:28.000Z',
					endTime: '2020-01-19T15:00:28.000Z',
					recStartTime: '2020-01-19T12:00:28.000Z',
					recEndTime: '2020-01-19T15:00:28.000Z',
					t: [{ lang: 'en', title: 'Halti', description: ' Halti Transboundary Area ', topicId: 18529 }],
					assignments: [
						{
							id: 25178,
							user: { type: 'user', id: 1879 },
							email: 'interpreter@daasta.com',
							status: 'pending',
							srcLang: 'eng',
							dstLang: 'smi',
							bidirectional: 0,
							partners: [],
							topicId: 18529,
							userId: 1879
						}
					],
					eventId: 16736
				}
			],
			companyId: 505,
			userId: 1877
		}
	],
	publishEvent: {
		id: 25875,
		hasCover: 1,
		hasLogo: 0,
		country: null,
		city: null,
		state: null,
		address: 'Cross Green, Leeds LS9 0PS, UK',
		zip: 'LS9 0',
		lat: 53.7683941,
		lon: -1.4875805,
		status: 'scheduled',
		access: 'public',
		timezone: 'Europe/London',
		wifiName: null,
		externalIp: null,
		code: null,
		startTime: '2020-07-21T22:00:46.000Z',
		endTime: '2020-07-23T16:30:21.000Z',
		recurring: 0,
		palette: 'blue',
		expectedListeners: null,
		type: 'standard',
		created: '2020-07-21T18:24:44.000Z',
		vanilla: 1,
		t: [{ lang: 'en', title: 'Testee', description: '', eventId: 25875 }],
		companyId: 505,
		userId: 1877
	},
	invitedEvent: [
		{
			id: 25875,
			hasCover: 1,
			hasLogo: 0,
			country: null,
			city: null,
			state: null,
			address: 'Cross Green, Leeds LS9 0PS, UK',
			zip: 'LS9 0',
			lat: 53.7683941,
			lon: -1.4875805,
			status: 'scheduled',
			access: 'public',
			timezone: 'Europe/London',
			wifiName: null,
			externalIp: null,
			code: null,
			startTime: '2020-07-21T22:00:46.000Z',
			endTime: '2020-07-23T16:30:21.000Z',
			recurring: 0,
			palette: 'blue',
			expectedListeners: null,
			type: 'standard',
			created: '2020-07-21T18:24:44.000Z',
			vanilla: 1,
			t: [{ lang: 'en', title: 'Testee', description: '', eventId: 25875 }],
			topics: [
				{
					id: 28884,
					room: 3682,
					startTime: '2020-07-23T14:30:21.000Z',
					endTime: '2020-07-23T15:30:21.000Z',
					recStartTime: '2020-07-23T14:30:21.000Z',
					recEndTime: '2020-07-23T15:30:21.000Z',
					t: [{ lang: 'en', title: 'Session 2', description: '', topicId: 28884 }],
					assignments: [
						{
							id: 41103,
							user: { type: 'user', id: 1879 },
							email: 'interpreter@daasta.com',
							status: 'invited',
							srcStream: { id: 189118 },
							dstStream: { id: 189119 },
							srcLang: 'may',
							dstLang: 'urd',
							bidirectional: 1,
							partners: [],
							roomId: 3682,
							userId: 1879,
							srcStreamId: 189118,
							dstStreamId: 189119
						}
					],
					eventId: 25875,
					roomId: 3682
				},
				{
					id: 28885,
					room: 3681,
					startTime: '2020-07-23T15:30:21.000Z',
					endTime: '2020-07-23T16:30:21.000Z',
					recStartTime: '2020-07-23T15:30:21.000Z',
					recEndTime: '2020-07-23T16:30:21.000Z',
					t: [{ lang: 'en', title: 'Session 3', description: '', topicId: 28885 }],
					assignments: [
						{
							id: 41102,
							user: { type: 'user', id: 1879 },
							email: 'interpreter@daasta.com',
							status: 'invited',
							srcStream: { id: 188649 },
							dstStream: { id: 189116 },
							srcLang: 'ara',
							dstLang: 'bak',
							bidirectional: 1,
							partners: [],
							roomId: 3681,
							userId: 1879,
							srcStreamId: 188649,
							dstStreamId: 189116
						}
					],
					eventId: 25875,
					roomId: 3681
				}
			],
			companyId: 505,
			userId: 1877
		}
	]
};

mock.onGet('/api/interpreter').reply(config => {
	return [200, interpretersDB.data];
});
